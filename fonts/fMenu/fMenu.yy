{
    "id": "b2b46417-2aed-4fa2-a089-6aaa3b9e40d3",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fMenu",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Segoe UI",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "4630e6ec-c4d0-4148-9f7d-74adae60a7cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 43,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 61,
                "y": 137
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "e2153ec8-907c-4139-9ae2-319b668d3979",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 43,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 129,
                "y": 137
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "4d5b0ab2-4afd-4766-9dea-1381f4649737",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 43,
                "offset": 2,
                "shift": 16,
                "w": 11,
                "x": 437,
                "y": 92
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "70413c0b-77eb-47f6-bbe5-d937dc5f2224",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 43,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 2,
                "y": 47
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "f0485572-f29d-402d-a59e-ab1ee99f972a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 43,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 164,
                "y": 92
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "9822fc6c-9817-4a96-8c1b-9662785fb92e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 43,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 150,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "be9f11ff-b88e-47c9-9ee5-6f07c683c56d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 43,
                "offset": 1,
                "shift": 27,
                "w": 26,
                "x": 178,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "ea0b4778-b45a-415e-8f62-0512edbbaf32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 43,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 163,
                "y": 137
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "d372a6a4-d5aa-4a44-894b-b3a3390c41d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 43,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 488,
                "y": 92
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "64e9ba95-b655-4525-bf04-ea40c4cdc036",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 43,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 137
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "44d9a680-7a49-4416-80df-f84065883921",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 43,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 394,
                "y": 92
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "e89eb048-b9d6-47dd-91bf-f8f3fb5b0696",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 43,
                "offset": 3,
                "shift": 23,
                "w": 16,
                "x": 146,
                "y": 92
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "40280daf-5cc0-49c8-9f7d-0652770e7d01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 43,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 138,
                "y": 137
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "2106b664-4820-4d40-a921-bcfc08031789",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 43,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 50,
                "y": 137
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "2ba707e0-f86d-4f57-a80d-55d7371696e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 43,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 111,
                "y": 137
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "2e6c2b82-15fd-43be-9873-0b0c0505e0fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 43,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 56,
                "y": 92
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "88197334-dc00-4a7f-962a-49e32a01a28b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 43,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 453,
                "y": 47
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "ebfaa39d-6a59-456b-a3d1-a80c5531d561",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 43,
                "offset": 3,
                "shift": 18,
                "w": 10,
                "x": 38,
                "y": 137
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "4a26402b-92c6-4f29-9ffd-bdac714ec269",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 43,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 2,
                "y": 92
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "a9bbbbae-ebb2-436c-af5b-33474d345d8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 43,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 20,
                "y": 92
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "4a87d51e-a6bd-49f9-a83e-203edc141cd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 43,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 126,
                "y": 47
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "de4fe05e-dafa-4236-b630-f9ee2e60cea3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 43,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 234,
                "y": 92
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "a2d9369c-b627-4a2c-8126-8e87dee89763",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 43,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 396,
                "y": 47
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "9d7d7f3a-7ae3-4522-a6e0-b0440d9be0a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 43,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 415,
                "y": 47
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "552936bb-2005-4f13-8be0-940960c260f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 43,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 434,
                "y": 47
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "96dc9074-0aaf-458d-aef5-aab58d2f7863",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 43,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 38,
                "y": 92
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "48cf4077-b44a-4e24-a54f-ba4e0aa1d453",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 43,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 120,
                "y": 137
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "6bd9428f-9774-480b-a2b6-35e78c6ad68c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 43,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 72,
                "y": 137
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "3f24d61b-9326-4969-b584-c0cb7547ab77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 43,
                "offset": 4,
                "shift": 23,
                "w": 15,
                "x": 200,
                "y": 92
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "3e7f6581-b96d-4cee-a799-b3ee53ae271b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 43,
                "offset": 3,
                "shift": 23,
                "w": 16,
                "x": 110,
                "y": 92
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "3cf29ed9-6523-4a23-9356-5db4fac2c2bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 43,
                "offset": 4,
                "shift": 23,
                "w": 15,
                "x": 217,
                "y": 92
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "8fcb6668-8fe3-42cd-8645-e9bcdc7acdd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 43,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 316,
                "y": 92
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "e7a5b65e-bfa6-485e-b0c9-80d2bbea33db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 43,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "8555f919-0888-4d10-ad7f-0bfb9821fcfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 43,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 258,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "8cc59cf1-806a-4462-b638-2cd7d7ac6aba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 43,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 166,
                "y": 47
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "bb160179-67e2-4884-987e-813b35dbc580",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 43,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 186,
                "y": 47
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "5b1e020d-d75f-4d21-b084-7d662dcff370",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 43,
                "offset": 2,
                "shift": 24,
                "w": 21,
                "x": 330,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "2cdb91ac-f7a2-4b78-8af3-6174bc04425f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 43,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 284,
                "y": 92
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "a8a3805d-25b2-45cb-8e2e-5c2ec534ce61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 43,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 268,
                "y": 92
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "3cde7247-b2ae-4133-a988-57ab930f8d0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 43,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 464,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "2125aa04-469b-4d02-98bc-b0462cb74e81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 43,
                "offset": 2,
                "shift": 25,
                "w": 20,
                "x": 420,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "f6ad7b58-6887-44a0-8461-9cf7b2207ec0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 43,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 155,
                "y": 137
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "95e081d5-d8f4-4421-ba0c-cbd776946595",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 43,
                "offset": 0,
                "shift": 14,
                "w": 12,
                "x": 409,
                "y": 92
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "84917874-64d9-4484-95ce-e7b1c5d3170d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 43,
                "offset": 2,
                "shift": 21,
                "w": 20,
                "x": 376,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "6ca413c4-b88f-4637-a8e9-96be929ee0f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 43,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 348,
                "y": 92
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "02c47b4e-032f-4067-aa73-abd2fe68a8a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 43,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "dc97fdfd-79e7-4d23-9148-bf84aca573b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 43,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 307,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "8f5a3e5a-d948-45ce-9b8f-201d33fa5e68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 43,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 233,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "229b86b2-f421-448f-a8d8-aa82b92ca987",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 43,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 358,
                "y": 47
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "eb2401e8-0a51-4705-8298-5c68501d28e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 43,
                "offset": 1,
                "shift": 24,
                "w": 25,
                "x": 206,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "4f82ce75-0f72-4821-aeff-fcbcc7281751",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 43,
                "offset": 2,
                "shift": 21,
                "w": 20,
                "x": 398,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "b1734f38-df3a-43fb-994c-7c43c40ec921",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 43,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 472,
                "y": 47
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "7865d839-9a5c-4b8f-90db-76c54bc8d77b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 43,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 23,
                "y": 47
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "b6fc82ac-54aa-473e-b5a3-45cc79762f97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 43,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 44,
                "y": 47
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "b63a3239-6dd1-405e-b2f0-46f109a9390b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 43,
                "offset": 0,
                "shift": 21,
                "w": 22,
                "x": 283,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "25b10681-6417-457b-91e6-664fdb2bc4c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 43,
                "offset": 0,
                "shift": 32,
                "w": 32,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "e9146e1a-c7d1-4bdd-9d72-729dc28255e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 43,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 353,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "f12828f6-3d7f-40b9-bf31-960f11097869",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 43,
                "offset": 0,
                "shift": 19,
                "w": 20,
                "x": 442,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "59561121-a7f1-4417-9247-b192fe933812",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 43,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 65,
                "y": 47
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "561152b1-fc7c-42c4-bc84-8f73dcd9fee2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 43,
                "offset": 3,
                "shift": 12,
                "w": 8,
                "x": 82,
                "y": 137
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "c4bd03a3-df92-4a04-a04e-934ae6dbccef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 43,
                "offset": 0,
                "shift": 14,
                "w": 16,
                "x": 490,
                "y": 47
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "38853384-6555-4c8c-9118-3549aae5c049",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 43,
                "offset": 1,
                "shift": 12,
                "w": 8,
                "x": 92,
                "y": 137
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "f35cc904-4fc3-4032-b773-1fa3311b95d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 43,
                "offset": 3,
                "shift": 23,
                "w": 17,
                "x": 263,
                "y": 47
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "dec43245-7422-40a3-8cdc-bdbf446094b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 43,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 332,
                "y": 92
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "001950fd-5fea-4f60-9462-8d761c792cc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 43,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 14,
                "y": 137
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "a49f717c-88a8-43fc-bafd-e8d6a633d270",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 43,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 74,
                "y": 92
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "156c7212-9ad0-4ef4-9569-7919169b2009",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 43,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 320,
                "y": 47
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "3ed3efe2-066f-4bda-b9d5-bd44e2ec2c5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 43,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 300,
                "y": 92
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "6fe9857b-927b-4008-9b03-62444ffff70c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 43,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 225,
                "y": 47
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "b02c8b09-0269-4c40-9b16-bc1b3108c2e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 43,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 182,
                "y": 92
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "ffb93f36-9fa7-4a5f-9302-975636f3fffe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 43,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 364,
                "y": 92
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "e6be5cfd-8fcd-4868-b662-39180f211cdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 43,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 206,
                "y": 47
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "f24a38f8-cb6d-4974-8b12-19ed196bc2b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 43,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 92,
                "y": 92
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "4d5ff752-f9fc-4e19-9546-b612d1537250",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 43,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 102,
                "y": 137
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "c3f02b44-58fc-4c80-9267-e2a12f469685",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 43,
                "offset": -3,
                "shift": 9,
                "w": 11,
                "x": 463,
                "y": 92
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "1d389e66-73ce-4faa-91b1-625e2b2296c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 43,
                "offset": 2,
                "shift": 18,
                "w": 17,
                "x": 339,
                "y": 47
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "0efe2c94-93d5-4335-bad6-ce93e2f9d83d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 43,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 147,
                "y": 137
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "4b8f7d76-5406-4193-84ac-5107835343bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 43,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a29dde42-2599-40c6-9ee1-7f014e182fac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 43,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 128,
                "y": 92
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "58a0e65c-4032-42e7-8101-6f4a0ae87bd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 43,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 86,
                "y": 47
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "33a69b86-bec1-4140-9531-9aa7d47c0c20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 43,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 244,
                "y": 47
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "88737b42-828f-4a91-a378-96af91d2798d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 43,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 282,
                "y": 47
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "274fa6fc-784e-4609-bf84-e82290a5201d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 43,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 450,
                "y": 92
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "d203da1b-9144-43b9-8e58-f06499716bb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 43,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 379,
                "y": 92
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "59c5f8e2-4a47-4f3d-8dcb-d7d96342b2ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 43,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 423,
                "y": 92
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "41de311e-d7df-435b-a61f-f3f8a8196824",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 43,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 301,
                "y": 47
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "21acbb86-b615-4636-8bf6-8b9b1606c4a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 43,
                "offset": 0,
                "shift": 17,
                "w": 18,
                "x": 106,
                "y": 47
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "08aeca19-9eac-4a14-9eab-b9a3b476e5af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 43,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 122,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "e0400205-7949-4e87-a614-289e6f50a587",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 43,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 146,
                "y": 47
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "e22e3973-6129-42fd-aa79-a507c943ae3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 43,
                "offset": -1,
                "shift": 17,
                "w": 19,
                "x": 486,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "a213d87d-2c61-49f2-b614-5083221a8790",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 43,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 251,
                "y": 92
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "9d8720d0-5685-4703-864f-a3ee8322db70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 43,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 476,
                "y": 92
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "7f7bea2e-f20d-4122-8648-bf1459eb2aef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 43,
                "offset": 3,
                "shift": 10,
                "w": 5,
                "x": 170,
                "y": 137
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "00735f9c-94ab-4b83-9978-10e9b3722174",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 43,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 26,
                "y": 137
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "47ef10a0-9b1f-4a3f-b1e5-8f584fb1a4a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 43,
                "offset": 3,
                "shift": 23,
                "w": 17,
                "x": 377,
                "y": 47
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "310e44d9-ad70-458c-a7bc-9fc027b080a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 114
        },
        {
            "id": "18303dc3-545e-4c8e-a5aa-e6a73b53cf76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 115
        },
        {
            "id": "89ebd29d-7706-4abd-845c-54104ed4b11d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 341
        },
        {
            "id": "fa2d2fd9-1842-4e33-bb2a-4d063b7276f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 343
        },
        {
            "id": "412121d5-db31-41bc-b60f-fff5b42ab5b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 345
        },
        {
            "id": "52574016-93c4-48b1-bcda-0dc170ec8f59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 347
        },
        {
            "id": "cec29bca-2994-4e37-9711-830c12e1ff8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 349
        },
        {
            "id": "b04a549f-c0f0-4bc4-a24f-2b31600c138e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 351
        },
        {
            "id": "a604d0bf-1496-477d-8571-4ab339995212",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 353
        },
        {
            "id": "98b96cf9-5d01-426c-8b37-a322bf9491bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 537
        },
        {
            "id": "fc06ef1e-fa0f-4eac-9136-7764c5081845",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 969
        },
        {
            "id": "5ff0caa5-21b3-401c-ba56-5b237b2b3fbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1109
        },
        {
            "id": "bef193c1-0c58-4d9c-a63b-98f8d56ec8e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 114
        },
        {
            "id": "a5b21c39-daf2-4568-892c-8080904cddd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 115
        },
        {
            "id": "6e08908c-16a0-41cf-a49a-b2e050ebf135",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 341
        },
        {
            "id": "93aa1749-2853-401a-b914-0092cbf8d149",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 343
        },
        {
            "id": "672c311f-e60d-4b38-a496-d5b3e7814b01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 345
        },
        {
            "id": "1e82cfa6-229e-4c96-9084-f216bc108721",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 347
        },
        {
            "id": "af391814-c9ff-4ac7-92f9-9278f277a217",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 349
        },
        {
            "id": "42666de1-865c-46ea-95c3-f86ded122f88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 351
        },
        {
            "id": "9b936777-b4c9-4fe3-8bd3-a2849c2eab31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 353
        },
        {
            "id": "0c5c4579-bb30-4f7f-a913-46c047530111",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 537
        },
        {
            "id": "a0020771-3f20-4c73-b190-3d1cbeed3398",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 969
        },
        {
            "id": "2e5bffcd-265a-47fe-a3a7-9f07f87d5f87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1109
        },
        {
            "id": "04464db3-1d61-41ea-b86b-04f198401ea9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 40,
            "second": 106
        },
        {
            "id": "165c2f1f-eb8c-4940-99c2-98fc99ee2b3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 40,
            "second": 309
        },
        {
            "id": "a8202da2-2b7a-4bf2-b706-28f8813f1ac0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 65
        },
        {
            "id": "79eebaf2-78ae-486c-b204-35cccd11b20c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 74
        },
        {
            "id": "103e2667-ab0b-400b-93de-a0cc76777670",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 99
        },
        {
            "id": "54b5064e-6871-4fc5-b316-46797c9dadd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 100
        },
        {
            "id": "e0018106-0785-4ac2-b171-026873d73e04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 101
        },
        {
            "id": "2dbcbdf8-ff70-4cd9-ae57-afd7192a4061",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 103
        },
        {
            "id": "47150979-03e1-4ee9-86a2-032d0c1c67fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 111
        },
        {
            "id": "0d252af4-ebe2-4759-8a99-aabc7b05a985",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 113
        },
        {
            "id": "0086dadd-baf1-4d31-80c6-29b1ea987025",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 256
        },
        {
            "id": "87a2b873-3679-4b70-b9c1-d78c00c4f108",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 258
        },
        {
            "id": "461bf292-8f96-4f45-8353-49e8d9d47829",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 260
        },
        {
            "id": "f9268047-4172-480a-bc36-4c9ea8830004",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 263
        },
        {
            "id": "49bc6d5a-49c4-46a6-93a7-347f60c6fa02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 265
        },
        {
            "id": "bfc94fca-62d9-433b-86f9-5b4e8f7eb216",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 269
        },
        {
            "id": "ad492952-4960-4ff2-9622-849743fb6cf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 271
        },
        {
            "id": "1e532278-e9af-442f-b735-237f64c8321b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 273
        },
        {
            "id": "b4fd5190-049f-4277-b351-cc6f41a0b753",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 281
        },
        {
            "id": "f82c2ec5-5a7e-4b02-8007-d68fb153fdee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 285
        },
        {
            "id": "10f02a7b-ff74-4683-93a5-048f25cf2042",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 287
        },
        {
            "id": "0d92c772-f573-428f-9c72-1e778b90e187",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 289
        },
        {
            "id": "326beb4c-2b67-43fa-8bc1-e6cc7b18cfbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 291
        },
        {
            "id": "68013bf6-b763-49f7-8745-395ae1ae278f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 308
        },
        {
            "id": "bc4963fc-562e-4962-9e02-656e3321cd62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 506
        },
        {
            "id": "822c02a2-6a94-424b-aa35-9318beb16bbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 508
        },
        {
            "id": "03de8eb8-e0f1-4927-9466-8cc1ff3cced3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 913
        },
        {
            "id": "6e8bc187-9b03-438b-b778-9a19170a6916",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 916
        },
        {
            "id": "5050813b-1b05-4239-90ad-ea361ad3b67c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 923
        },
        {
            "id": "2cd53b99-91c8-417e-91a0-ee82af5bf27f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 1040
        },
        {
            "id": "cc110fcb-4dbd-4de3-a8da-476c7f815a05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 1077
        },
        {
            "id": "46e7c49c-974a-4334-8d82-7f6b1ddff2ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 1086
        },
        {
            "id": "13581f25-e83c-42a7-b7a8-c66300e30413",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 1089
        },
        {
            "id": "2e5f0725-6d43-4783-8a49-6af0bf3e4306",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 1092
        },
        {
            "id": "3be881db-0e4b-49a7-a311-75f79147ffc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 1105
        },
        {
            "id": "d0d780f6-9300-4ca7-a463-6f3b118c3291",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 1108
        },
        {
            "id": "882e6e18-3526-414a-8784-e4d75bba5fd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 8216
        },
        {
            "id": "3e1db5ee-30a9-4273-9db7-92e53e94776d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 8217
        },
        {
            "id": "c23ddc44-94e4-4129-97ae-8671cb2c5877",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 8220
        },
        {
            "id": "fe60a741-95c5-4801-a536-279bd8636a26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 8221
        },
        {
            "id": "19828339-78ef-4e1c-b4e3-6e2e9c490599",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 8216
        },
        {
            "id": "acd0d4df-aff4-488d-a7f5-5460ace267a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 8217
        },
        {
            "id": "6f8dc427-56cc-4e9f-b1dd-6108ad9c1f69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 8220
        },
        {
            "id": "57159708-c193-4a88-b5ec-135009f19e4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 8221
        },
        {
            "id": "9f3bc017-29d5-43b1-bd55-fa590df784c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 42
        },
        {
            "id": "3922aecd-865b-4ec6-b0cd-e8e165fe0ae3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 44
        },
        {
            "id": "ceef67f4-a9f3-402a-a453-107aa3da79f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 59
        },
        {
            "id": "a3e5eae6-f406-4bfd-969e-edda34f79f7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 74
        },
        {
            "id": "20f76f56-c858-49c0-86bb-cb1a51a70174",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 84
        },
        {
            "id": "a2986e7d-ec69-4742-9eec-c76d9869fda6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 85
        },
        {
            "id": "cb61e780-366d-4fd5-828a-90eaed970cd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 86
        },
        {
            "id": "39d96a7e-b06b-496d-ad00-527f58f6f02e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "cc7b0584-f9dc-4eb9-81a1-69e8be5939ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 89
        },
        {
            "id": "5b6705da-3890-4a5e-afb6-0a0b6f8d1756",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 116
        },
        {
            "id": "ecb7a003-bb73-4b88-87a7-46bd56b8e397",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "91161641-878a-4133-9dbd-abd0f2abfa63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "e866ee4e-10b3-42bc-a893-f9122f788012",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 221
        },
        {
            "id": "3d684ea7-522e-466c-8f52-22e4d79d169d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 255
        },
        {
            "id": "dbedffee-aa30-43f3-8a35-f023cd7c5062",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 308
        },
        {
            "id": "7a2fbe3c-a22e-44c4-ae6d-25dc37590319",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 354
        },
        {
            "id": "42e952da-6c29-4dce-9668-f6d9f7ae1767",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 356
        },
        {
            "id": "c532a00e-3a34-423f-a47e-252f7f937c5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 358
        },
        {
            "id": "20ad1571-566c-48cd-baef-c0cd50bf036a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 372
        },
        {
            "id": "b4ffca19-bd30-4a3f-b852-c75a0d4e3895",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 374
        },
        {
            "id": "27e44f98-9350-4ad7-8a30-6623aafc0cda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 376
        },
        {
            "id": "47b62388-9df3-44b8-9d31-1dfc992a6f8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 377
        },
        {
            "id": "1f57071c-3292-401c-a1d5-cd808ada0f6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 379
        },
        {
            "id": "87e9ebe2-e301-4f38-82cf-5de6f94461c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 381
        },
        {
            "id": "71f18250-9caf-4312-8e41-d9753706b786",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7808
        },
        {
            "id": "d7a526b9-dd4a-4856-b759-9ce228b8fc92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7810
        },
        {
            "id": "44b53a31-59ac-4556-85b0-8e185885474d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7812
        },
        {
            "id": "46e4ee4f-d77c-4cce-83d7-b6fefc398854",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7922
        },
        {
            "id": "5af635a9-0ed9-4ec5-8c01-74ad81d359dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8216
        },
        {
            "id": "fb7c9cad-2496-486e-8f70-d9625bbdfa18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 8217
        },
        {
            "id": "83d8fdbf-fa75-47c1-a4ed-27b0d03883e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8220
        },
        {
            "id": "3eeddbd9-6ffe-4664-bc4d-c921df70a70d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 8221
        },
        {
            "id": "a5e35d64-c1fd-434d-9964-3f01f1100a30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8482
        },
        {
            "id": "4aeccb9f-504b-4c42-98b7-37a10412de6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 84
        },
        {
            "id": "83031868-2542-4e6d-a0de-0368fd68a931",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 89
        },
        {
            "id": "c6e48375-bb23-4b2a-8f28-be8ece28eb5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 221
        },
        {
            "id": "bf6f6cba-5763-473f-91d2-d1bebe04d1ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 354
        },
        {
            "id": "3c1d90aa-0830-4809-bb27-cc01bbda913d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 356
        },
        {
            "id": "5feffb69-840e-4555-b1b0-883abffd87a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 358
        },
        {
            "id": "a4af7c76-16c1-4072-af88-53a480f72b60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 374
        },
        {
            "id": "b7b36127-1e94-4bfa-bfa2-0a34ce57004e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 376
        },
        {
            "id": "ed9f8524-3695-40e7-99a0-63633f5ec892",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7922
        },
        {
            "id": "e4844b59-345c-474d-9f4b-d0bff6b5263b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 8218
        },
        {
            "id": "12cdee2d-c24c-4465-803b-44a5b4b4267c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 8222
        },
        {
            "id": "9db0f2c7-36d6-4584-bd7a-32a1fc403ca7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 67
        },
        {
            "id": "2f1e1651-2d9a-448f-89df-3703a4333713",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 71
        },
        {
            "id": "f8e50299-9761-48df-8248-eaa839dba47f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 81
        },
        {
            "id": "416df87d-dc03-434d-b0ce-4ccd047037bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 171
        },
        {
            "id": "09c58d9a-c77b-451e-9d12-89bb8dce46ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 199
        },
        {
            "id": "c0b4d04f-be78-49d5-95a8-d7dac772e1b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 210
        },
        {
            "id": "76b969ca-5cd0-4bb7-ad64-11e2a9925390",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 211
        },
        {
            "id": "abaf6294-02bb-47b3-9c0a-f4528875c69b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 212
        },
        {
            "id": "46372c02-efef-40dc-b3d8-0e6bee416024",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 213
        },
        {
            "id": "85ea9887-da41-4eb0-a8db-91a2dde2f031",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 214
        },
        {
            "id": "8e625a53-f22f-4749-9015-5d16c111766d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 216
        },
        {
            "id": "18f5be00-ded2-4b39-8e76-2e087af18b9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 239
        },
        {
            "id": "ee5bf300-2c53-4781-b616-5a4671ef23d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 262
        },
        {
            "id": "5e1621ba-f71e-4fde-b2ad-cab45b67c443",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 264
        },
        {
            "id": "a29e1d87-ba03-4a99-8f25-da92ea9c7545",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 268
        },
        {
            "id": "35d9a684-7476-4d07-9245-ceb1f0200538",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 284
        },
        {
            "id": "1f1f9295-7227-47e2-bcc5-901a3b423d1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 286
        },
        {
            "id": "9a5130a9-48e2-404d-905a-4173eaf70ab8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 288
        },
        {
            "id": "9c5b2cc5-cdae-4ab3-893e-2dcceda825b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 290
        },
        {
            "id": "d808f356-b344-43bc-baa3-52853da7da72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 338
        },
        {
            "id": "38d87a69-fb1e-4aa2-a066-8c48a5e76194",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 8249
        },
        {
            "id": "318c5383-d2d5-4230-b698-56312d3f534a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 44
        },
        {
            "id": "18c40273-f892-452b-8598-04faa8f32208",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 46
        },
        {
            "id": "27cce94c-a38f-4c97-ab86-28763e0f511c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 84
        },
        {
            "id": "45382260-500c-4aff-9648-f31e14f2f24c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 88
        },
        {
            "id": "8c7dc5ce-201a-4fa9-81c4-9235539adda8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 90
        },
        {
            "id": "700c16bd-c6cd-4e0e-86d4-8319f5a00e89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 198
        },
        {
            "id": "21b30af8-3e0f-47d7-a77c-4e97ee423b8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 354
        },
        {
            "id": "e2a1b080-52a3-4856-a2e5-e72878b5479d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 356
        },
        {
            "id": "fb881a72-26a6-4fb1-a4d8-61569d9e86a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 358
        },
        {
            "id": "a7764365-db79-457b-b216-6c8d9d5f5025",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 377
        },
        {
            "id": "2d6318d4-92c5-4443-ae7f-c0775ff94393",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 379
        },
        {
            "id": "ab0ec688-88fd-4f87-9604-cc865f5ba628",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 381
        },
        {
            "id": "a8f67910-3dd2-4abb-b01c-335dabc457d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 8218
        },
        {
            "id": "714dbbcf-4888-4e80-ba59-9af60f7918dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 8222
        },
        {
            "id": "5161f68a-a23a-429d-b5a2-7435b967660d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 8230
        },
        {
            "id": "42d9529b-6f4d-4ffe-b078-c80b95eb5e34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 69,
            "second": 74
        },
        {
            "id": "4fb612d6-b1cb-42fb-bb92-af070078012b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 69,
            "second": 87
        },
        {
            "id": "80697e33-8b75-4845-a787-3aed919ab0af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 69,
            "second": 88
        },
        {
            "id": "8a131465-388e-4f0c-8ded-8e17666859a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 69,
            "second": 308
        },
        {
            "id": "ff1e584d-116a-4bdf-943b-7120c132c0c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 44
        },
        {
            "id": "372bd272-5d64-4015-9b04-0b8b334b6f92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 46
        },
        {
            "id": "ed69857a-7d13-4cd1-b2bc-65bd3f12d0f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 65
        },
        {
            "id": "2b0cd2da-cf8a-4825-8d1a-f1e619b17555",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 74
        },
        {
            "id": "82dc6b18-f079-4d34-bb70-1ce5d88c35a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 97
        },
        {
            "id": "fba1b989-2438-40e9-8c4c-ca7ebfd35c35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 192
        },
        {
            "id": "80888504-58df-429a-85e4-e6d5fe177b5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 193
        },
        {
            "id": "facafb63-292a-474f-9a52-a5d7c6e68aab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 194
        },
        {
            "id": "0104b4eb-b9c3-45c4-81ff-2dd30e7fea9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 195
        },
        {
            "id": "7bb7d574-0eaf-433f-91b7-65877d846f26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 196
        },
        {
            "id": "bba8d353-c48e-4620-9d8f-bb6a7fb3b206",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 197
        },
        {
            "id": "0571e08f-100e-4bc9-883e-441033526566",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 198
        },
        {
            "id": "cc72ebf6-2fff-408c-997c-003c12c5a18d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 239
        },
        {
            "id": "71b27bf2-4456-4df2-9e95-1a402881fdc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 256
        },
        {
            "id": "07c96e18-5ec2-46c6-afad-3dbd135c50a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 258
        },
        {
            "id": "373de93b-07e4-4dc7-aaef-772044309206",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 260
        },
        {
            "id": "e1bfbbae-b8ed-4ede-bf4f-fce014932f79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 261
        },
        {
            "id": "c004eaa1-0f70-4344-a686-851d6da0d103",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 308
        },
        {
            "id": "f6d36170-8f5d-4cfa-9ebf-4d2d6153dbd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 506
        },
        {
            "id": "627c7847-1a81-4e3f-88c5-6a5f2ea64fa9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 508
        },
        {
            "id": "aac3e3e1-adc1-451d-b6cf-ac98e1e2dd95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 509
        },
        {
            "id": "ccd44cef-ec16-4adc-9a81-a46fa2cc23d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 8218
        },
        {
            "id": "645fb857-1df7-427b-953c-bfa41c2cc6ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 8222
        },
        {
            "id": "fbcdec67-ae21-42d9-b167-035dc8a71417",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 8230
        },
        {
            "id": "8e2dd52a-dbfe-47d4-9a48-bedc73063dd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 84
        },
        {
            "id": "844a801b-8b91-4550-988a-2d26fd743380",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 354
        },
        {
            "id": "d162371d-fb06-425b-8633-ddbd260a387b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 356
        },
        {
            "id": "bc2ca8a5-120f-47f3-9235-40fda1352c1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 358
        },
        {
            "id": "80260e46-0dee-4b37-858b-2a0c575c241d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 44
        },
        {
            "id": "fe749a68-6f69-4bc5-820c-a5e340bb7bc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 46
        },
        {
            "id": "2e670906-ceef-4ecb-bb2f-5b4df31aace1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 65
        },
        {
            "id": "44f84007-6aec-4603-a169-b1de38b1118e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 74
        },
        {
            "id": "2a9d33d5-96b4-4b15-b19f-4e3b6366e929",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 192
        },
        {
            "id": "f5e8a54c-e281-4131-b08d-99145af810fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 193
        },
        {
            "id": "f6f4744d-3a91-4489-8529-2f77c3be26f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 194
        },
        {
            "id": "2564f1c4-c671-40c7-8736-ced06aa20974",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 195
        },
        {
            "id": "701d3937-aae2-4d67-9dd1-3a7e985e5abf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 196
        },
        {
            "id": "e73fb200-276e-4f88-9963-bf00c697ea22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 197
        },
        {
            "id": "4fe0f268-b8ae-49d4-8c18-0bed6cd08723",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 198
        },
        {
            "id": "93e79757-26e3-4a4e-b75d-17f2547985a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 308
        },
        {
            "id": "1ce31a51-907e-4304-87ac-47b4cb268ba9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 8218
        },
        {
            "id": "35c0f437-c835-4604-99ce-8a9cc401b0a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 8222
        },
        {
            "id": "3ae6b917-19b9-4a24-97c4-f5ed0a297c23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 8230
        },
        {
            "id": "78a59f8a-19a4-4be6-bc6e-f28ecf982bd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 44
        },
        {
            "id": "c62e2955-b523-4102-b062-b5c72849204b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 59
        },
        {
            "id": "2fb48c64-11d8-4ede-b0b8-c67f515a0a5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 67
        },
        {
            "id": "47a95ec9-e5f5-4a14-a336-7cb9e0a8fc37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 71
        },
        {
            "id": "4819c8b2-80fb-4d13-9981-1a33e827e50a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 74
        },
        {
            "id": "41cb661a-852f-403b-81fc-b43e9903d760",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 79
        },
        {
            "id": "cb800ab1-9670-4726-baed-702dc37bda9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 81
        },
        {
            "id": "75bdaecf-47c2-4455-8831-2cd97e25eac2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 88
        },
        {
            "id": "29e442ae-4139-480c-b207-9287f91b3acf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 90
        },
        {
            "id": "1d00770e-3090-4cca-ab1b-da4f9e6aa4d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 116
        },
        {
            "id": "6201c047-9254-4218-893a-f6542d55b4cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 118
        },
        {
            "id": "a79de3cf-72be-48ac-ac0c-609e47e66a06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 119
        },
        {
            "id": "6d95a228-835f-4f5c-8248-e2f5358bd9fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 121
        },
        {
            "id": "d69f27a0-484c-4a00-91f0-1f457a3eb344",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 199
        },
        {
            "id": "7a109b37-1978-4d45-9fda-7d75fb5c52d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 210
        },
        {
            "id": "dd2993ef-57cc-4c3b-99e7-3326efe92357",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 211
        },
        {
            "id": "1b0a08d7-fbe9-49e4-a74b-a14b2b419e7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 212
        },
        {
            "id": "4ae8b9cf-3cd8-4848-bdb9-d76280ce2792",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 213
        },
        {
            "id": "fcefecb0-fabc-4298-bd54-4c5cec95fe86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 214
        },
        {
            "id": "05baa7ec-30c9-4904-a873-0657bdb532ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 216
        },
        {
            "id": "11cfb5f1-6491-440f-84f6-4d9fa024e5c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 236
        },
        {
            "id": "36c77787-3dfb-4095-af49-c9faceb5d916",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 237
        },
        {
            "id": "a86df4be-1dba-4aae-a3f8-a29fa9e13924",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 239
        },
        {
            "id": "c150ff83-64bb-424e-af3c-d207cdfddf4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 253
        },
        {
            "id": "7b2a30a9-3179-47a7-afb1-47e2777aeb6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 255
        },
        {
            "id": "b1ed731f-9bbc-4c36-a543-2fdd2b3f1449",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 262
        },
        {
            "id": "d4d13352-65e0-48f2-887a-74183f251b14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 264
        },
        {
            "id": "c6724751-8cdf-4741-8fa9-1ce23a741a75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 268
        },
        {
            "id": "543e57ac-2d5f-41e2-8b02-33d5a66c8a85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 284
        },
        {
            "id": "4330f638-c0e8-4d4d-94a1-5fae507b2568",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 286
        },
        {
            "id": "57b39a00-3930-476d-9401-08620eebe81e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 288
        },
        {
            "id": "d974f3b9-f6e3-47de-83c2-568c2f837890",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 290
        },
        {
            "id": "445d4f5c-afc6-433e-8d75-83b47dd68c3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 308
        },
        {
            "id": "ddd16f97-8eca-4785-82c7-66715fae1ef8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 332
        },
        {
            "id": "dc8cd1ef-2513-401a-9257-9d70eb098ce9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 334
        },
        {
            "id": "38c95bb3-f788-426d-9835-2433f96d1408",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 336
        },
        {
            "id": "41ca2c09-774b-4ce6-8a7c-4582f688b9b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 338
        },
        {
            "id": "cae8b003-6647-474e-b394-0e5f4edbd229",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 355
        },
        {
            "id": "c9b9405e-fe81-4b4d-a5de-173b80ac443d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 357
        },
        {
            "id": "15d68433-5ecf-475e-b62e-7b717c4f1feb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 359
        },
        {
            "id": "89778cdb-0e4d-4e88-b3cb-e8bcbd04cd5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 373
        },
        {
            "id": "4e1ed3de-84ce-4c22-a949-8d9b6e2bf955",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 375
        },
        {
            "id": "98935d93-e699-4124-bfdd-60f605bb6d6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 510
        },
        {
            "id": "5ff196e7-32ce-4a4f-81f2-0cf9577422d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7809
        },
        {
            "id": "fc772644-74da-44a0-840a-c45ed9e0d1f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7811
        },
        {
            "id": "63449f07-20a8-41b3-8342-94d9d421f4b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7813
        },
        {
            "id": "99c09970-e5f9-4c50-b94e-c63b995177af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7923
        },
        {
            "id": "27cfdcdc-36b5-46af-bc6e-759dfb07d69c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 8218
        },
        {
            "id": "dba9bf18-5743-47a5-8f67-379be76985d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 8222
        },
        {
            "id": "d10b0e49-ab90-4896-aebf-9c4a4ff60ddf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 42
        },
        {
            "id": "09a6d3d1-85ec-46ba-8213-8659564c31ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 63
        },
        {
            "id": "816dc194-4adc-46e9-8766-50a3bcff5fc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 65
        },
        {
            "id": "10547727-ff7e-4020-8404-224e67d46824",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 67
        },
        {
            "id": "afe58a9b-13af-4459-946e-13a4a9e3b41f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 71
        },
        {
            "id": "183cb6c9-e8f3-4e21-9531-b7493b022768",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 74
        },
        {
            "id": "1531f07a-a561-4951-a18e-6c2fbf0a6d15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 79
        },
        {
            "id": "8d910be9-267a-4da7-97cf-d666f326c7df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 81
        },
        {
            "id": "6d2165f2-cd6a-46fc-928a-2c0adcd1bc80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "b6a4270f-c656-4d2a-ba1c-cb203ddc019b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 85
        },
        {
            "id": "da04ad4e-ab74-46dd-9a35-2f845de7cbb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 86
        },
        {
            "id": "2742dad8-a7cd-4ddf-bc4d-e694c1b2e639",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "dc4a2503-14ab-43a1-a2c4-ad3ef714e816",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 89
        },
        {
            "id": "8acad2dd-6709-4017-9f3b-f5d523ea7801",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 90
        },
        {
            "id": "2ee85b8a-00e8-4571-b4c3-e5329ff63cf8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 118
        },
        {
            "id": "ef8fe21b-87ed-4202-9b36-ba8287bccbf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 119
        },
        {
            "id": "f3830530-ffa6-406b-b565-074f6a2fa289",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "ccfe4ff4-2a0d-47e0-bb96-53aae47c1cc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 199
        },
        {
            "id": "489b5992-66ce-4fa9-9dc9-c3ac77e9908c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 210
        },
        {
            "id": "a381cc32-2dba-4aef-8377-ed7a53ea2424",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 211
        },
        {
            "id": "b70daf48-0aa3-49d4-abea-a555deb29079",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 212
        },
        {
            "id": "234aa5b5-2d58-4408-8099-eb8530f9ffea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 213
        },
        {
            "id": "e6ac6730-2030-44b7-bccd-cf0735201947",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 214
        },
        {
            "id": "b9f09d49-992d-4458-9c3f-5068bfdc1811",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 216
        },
        {
            "id": "a04da2ec-3910-482b-b540-dc41fc751cd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 217
        },
        {
            "id": "a6bb2883-f14e-4584-bda1-c4146e74ed93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 218
        },
        {
            "id": "fb035465-1474-42ad-89b3-c8c745e5ee86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 219
        },
        {
            "id": "b9e15c10-544f-48d0-b6dd-b8f7b816ef27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 220
        },
        {
            "id": "3ccf8df5-1709-41d2-8f0d-9f4da5abe594",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 221
        },
        {
            "id": "c3447f1b-a24c-4712-9d51-7f00ded1594e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 253
        },
        {
            "id": "3dbafbe2-0069-46f4-b753-1129d903316b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 255
        },
        {
            "id": "dc039c3a-62e6-4e68-b5ad-1996432466e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 256
        },
        {
            "id": "5ef50464-9a21-4473-8902-0ece3a129d6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 258
        },
        {
            "id": "a8dbbaf4-a8e2-4c8c-af4a-33d884290deb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 260
        },
        {
            "id": "3e531cd8-821d-46b5-bc20-fd9d5aa188a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 262
        },
        {
            "id": "c25cfd33-f41f-4a87-b8c2-e83a021e7058",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 264
        },
        {
            "id": "5d5cfb90-9889-4427-842c-1e06b1b2e3ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 268
        },
        {
            "id": "3cf782a0-7344-4805-bf22-a021cd137571",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 284
        },
        {
            "id": "74e59376-1305-4266-a378-ec6a34af47b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 286
        },
        {
            "id": "451962bb-0588-4bc2-8f89-cf338e69d06c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 288
        },
        {
            "id": "a5ac559d-d6a2-4b25-8f45-92a484d38c68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 290
        },
        {
            "id": "e0ee6743-0b9b-4e68-9803-cd3174260365",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 308
        },
        {
            "id": "1589a915-0d74-4c7a-badd-1b18d733d0b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 332
        },
        {
            "id": "6edc2492-101b-4ec8-9978-49001d8fca98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 334
        },
        {
            "id": "50a53756-2322-4a99-bae8-801515ee0ce3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 336
        },
        {
            "id": "8871d6d9-e569-40be-91eb-4a02de03e50a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 338
        },
        {
            "id": "286b5941-1cc1-4cbc-b01d-5889f6cc57ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 354
        },
        {
            "id": "9f8d5a70-5a74-42e0-b350-87a877c086b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 356
        },
        {
            "id": "5a22ab06-c068-4a55-bcc9-bcf803c1d2da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 358
        },
        {
            "id": "bcf84066-c070-48dc-887f-010ed854923b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 372
        },
        {
            "id": "fd931bbd-208d-4411-a2cf-76aa0fbad959",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 373
        },
        {
            "id": "1b6aaa65-2a4e-49aa-a60a-61672e74c5ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 374
        },
        {
            "id": "6256b564-65bd-40a2-b1f9-605b49bfe319",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 375
        },
        {
            "id": "4cc55006-790a-4c69-906c-772ab6ed54db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 376
        },
        {
            "id": "decdd9b8-f500-4e4d-804f-83e4917c6370",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 377
        },
        {
            "id": "f65dff88-188c-4f06-9130-588e9743e76f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 379
        },
        {
            "id": "2353ec1c-1c1e-456a-b4bb-a7a313da462b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 381
        },
        {
            "id": "c984cb9c-a7ba-4e4a-9f50-3b8488828f8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 506
        },
        {
            "id": "bd8d1111-5c3b-4659-bf06-f0f9ead6652c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 508
        },
        {
            "id": "1a72c904-6691-4666-8591-7e14939fc37b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 510
        },
        {
            "id": "288f8d43-bfcb-41af-8f70-70e917c00d68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7808
        },
        {
            "id": "b676a3cf-cc8b-4555-94c3-0b70f7c23af8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7809
        },
        {
            "id": "069f4cb1-7b80-4c5d-abc8-eb9ac8ebdb21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7810
        },
        {
            "id": "2ad58390-656f-49cd-8515-8a318d94db12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7811
        },
        {
            "id": "dddeddc5-d183-46fe-96e6-ded03aa6570d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7812
        },
        {
            "id": "7d4f3602-79df-4cd7-aaad-04a0b299f797",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7813
        },
        {
            "id": "f8ef1ac2-0cdb-460d-805d-2c765b6dee03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7922
        },
        {
            "id": "9057fcfd-4637-4a90-8ce7-f0765044c27b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7923
        },
        {
            "id": "c6aaa4d0-9f7e-4bee-ae60-e44d930a9abe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8216
        },
        {
            "id": "35cb18dd-a592-4da5-ae9f-13e657bb8339",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8217
        },
        {
            "id": "043df273-bff4-43ec-8caa-cf48697224ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 8218
        },
        {
            "id": "5c923742-cd39-4680-9e45-e6458c1070da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8220
        },
        {
            "id": "548c50f9-1d74-4f9e-8056-1b02befacd75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8221
        },
        {
            "id": "855ce0d0-b0f7-436d-83dd-2fc7069d4986",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 8222
        },
        {
            "id": "a25f6e56-a126-4410-86d3-672c4b417d93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8482
        },
        {
            "id": "e103c0c3-462e-4d64-88ef-58eaf37fb832",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 44
        },
        {
            "id": "627dc5f7-d353-4a0f-92e9-15c8bdc08b73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 46
        },
        {
            "id": "0bb82815-715a-461d-bbb5-13a622a0fac0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 84
        },
        {
            "id": "4bcf2b74-f9ee-4462-96b1-230471ddb606",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 88
        },
        {
            "id": "4726b531-7f10-4bcd-94dd-943a0139b72c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 89
        },
        {
            "id": "ec6bb14f-9429-47d9-af5d-89b40d42229b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 90
        },
        {
            "id": "ee97ad24-4089-43f4-8f1f-05082ca8bb5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 354
        },
        {
            "id": "625acfbe-d9ed-4a0c-b0f4-fd1a1d33fa1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 356
        },
        {
            "id": "06df4183-fcb2-4a75-8463-0f0aed54c42f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 358
        },
        {
            "id": "8fe0ae18-6294-4de3-8ad4-0b4d251ee719",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 377
        },
        {
            "id": "d1bdcfc6-60b2-4247-8e5f-5c3cfbb51f77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 379
        },
        {
            "id": "97832b3f-5bfa-4a6e-88f5-fd75d0b735a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 381
        },
        {
            "id": "ecf73326-e936-44e1-95ff-153c81601285",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 79,
            "second": 8218
        },
        {
            "id": "767d3ec0-396e-4498-8b90-a94dce31b256",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 8222
        },
        {
            "id": "0dc57d1d-aa69-410e-afe6-92ab3068af2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 8230
        },
        {
            "id": "47ee5698-169f-4b1a-ba7f-b6f1d8ed9108",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 44
        },
        {
            "id": "c0eb9821-44ce-4164-9b6f-3e8148bf037f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 46
        },
        {
            "id": "ae0f8c20-9862-499c-8813-c14cabb3cfcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 65
        },
        {
            "id": "48f479b5-95dc-4a92-b147-406870572e80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 74
        },
        {
            "id": "9548de31-462a-4c1e-9dce-c14f47fe710b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 87
        },
        {
            "id": "2f69e5ea-1c41-48c8-9a9c-63e71992e414",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 88
        },
        {
            "id": "a5ee4662-2b52-4140-abeb-ace6f69423df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 97
        },
        {
            "id": "46c9a9d8-55b8-4b51-9260-d754f81440a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 99
        },
        {
            "id": "0a9e29b6-0b8a-4777-a1ed-ac9e4bd7023a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 100
        },
        {
            "id": "ff89197d-296c-4218-9ab9-958d39f1b902",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 101
        },
        {
            "id": "de0c2134-0489-45f0-8ce8-88db39b6d436",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 103
        },
        {
            "id": "763daf62-763c-4233-a46c-4e5fd69abaca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 111
        },
        {
            "id": "250495bd-7c3d-49f1-810e-b5c48062619c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 113
        },
        {
            "id": "d26e64c2-ac1d-4044-9cdb-75f672afa04f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 192
        },
        {
            "id": "7222e2a7-5b4d-4ade-b05e-29d9031c7087",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 193
        },
        {
            "id": "3b305c77-ef96-44e1-a8b0-cf6e4a71b565",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 194
        },
        {
            "id": "4c2e76a4-ee72-4563-b7e8-4bb9acf2cca0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 195
        },
        {
            "id": "f81b7913-5921-4214-828f-55e692c7a182",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 196
        },
        {
            "id": "671a9462-d7a4-43bc-a078-426fed7b5e84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 197
        },
        {
            "id": "c819dced-1cfa-4495-a509-a327d775ddc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 198
        },
        {
            "id": "1e5a5937-565d-474f-aabd-3ab15aa30fe8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 224
        },
        {
            "id": "f5bbc017-53ad-4d18-8431-551140df6ca4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 225
        },
        {
            "id": "122e2236-8c78-47fb-ba76-0b37c774a52a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 226
        },
        {
            "id": "2c1ed80f-83c0-4ac9-9dbf-5b32f4df3c8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 227
        },
        {
            "id": "e5eaa65c-9386-459d-964e-e4aadf6048ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 228
        },
        {
            "id": "fcdc1727-781d-4d69-a074-5107b94f8e7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 229
        },
        {
            "id": "630b0a4b-2a34-4db6-a0bc-e5e11ea9041b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 230
        },
        {
            "id": "9d6f1b52-e4bc-4b4d-aff8-95165c4412dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 231
        },
        {
            "id": "be6f10c5-82d3-4926-8445-ef55ad6c611b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 232
        },
        {
            "id": "e91b5eee-8823-4919-adde-3fc70e65d2fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 233
        },
        {
            "id": "0afabd65-a543-41ba-aa07-9906afd7d1fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 234
        },
        {
            "id": "92440e77-fe16-4e09-b808-bb7d0c4d84ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 235
        },
        {
            "id": "8cd3662f-2fe4-4ef7-ae55-8ed90f0dc493",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 242
        },
        {
            "id": "17820b3e-555f-45f8-a5da-645d4a13e177",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 243
        },
        {
            "id": "a5231431-4a83-4ff7-aadd-5ab8f2808a6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 244
        },
        {
            "id": "7fcb9822-2ef1-4222-b8a1-d46edd44d13d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 245
        },
        {
            "id": "ec9c85d9-96f5-4506-ab9c-84d20fecf92c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 246
        },
        {
            "id": "c7a1a833-6ca5-4e83-b2b9-a1dc797e7db2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 256
        },
        {
            "id": "550c9c00-d686-4f37-8f0c-0f8ec1c0a03c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 257
        },
        {
            "id": "dc6ffcea-63b5-4707-bf6b-aeeb81ef8eb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 258
        },
        {
            "id": "d79e1c2d-be34-41f1-b4c1-7e5c4563b676",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 259
        },
        {
            "id": "d28956b3-6286-41ce-96f6-ac63cf77d57a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 260
        },
        {
            "id": "bb6038db-112c-419a-abe7-6c4fe2480e89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 261
        },
        {
            "id": "4717daf0-e63c-492a-a526-24411e566f8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 263
        },
        {
            "id": "75c46b2c-5fa6-48c1-9eba-604722ca518f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 265
        },
        {
            "id": "77a2d39c-e936-4f47-92ad-ee40fe5f1ca7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 269
        },
        {
            "id": "a97d0402-67be-40b7-a42d-b50a59517c05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 271
        },
        {
            "id": "90d825b1-ed56-48ef-b1d3-e533956ab689",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 273
        },
        {
            "id": "3fef27d3-4419-4eec-af40-6588504f7c74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 275
        },
        {
            "id": "5d6d8d6b-b108-4b76-9c10-035643349a45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 277
        },
        {
            "id": "b0133e7e-cda9-4163-a1cd-7e94cfe39d73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 279
        },
        {
            "id": "0d59c920-ce07-4e83-b483-499ac26f62a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 281
        },
        {
            "id": "0781aa4e-2a19-485d-a128-e0d4b966c846",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 283
        },
        {
            "id": "919fd76d-d2bb-4d2b-a6ef-93e3d40d5224",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 285
        },
        {
            "id": "4803ad33-379a-434a-a519-d66dcf78cca2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 287
        },
        {
            "id": "f91efa26-a0cf-4020-b316-f4bdd7172859",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 289
        },
        {
            "id": "7c13053b-e825-4b34-976f-9eedce197eee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 291
        },
        {
            "id": "2114e37a-6433-4440-8b76-f89c4587af66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 308
        },
        {
            "id": "c281ea5a-e082-4b6e-bfdb-9daaf3bd025e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 333
        },
        {
            "id": "d277dd7f-4adc-434a-bfa8-3e17eca5f185",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 335
        },
        {
            "id": "88ef8791-15ae-4162-9b3a-5f0c08b89ad8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 337
        },
        {
            "id": "f958b50d-6f6d-4887-9298-7ef444cf4d30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 339
        },
        {
            "id": "a8d9b31f-482b-4832-ad35-118e157fa15a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 506
        },
        {
            "id": "4217317d-3661-41cd-866a-796fb724bbc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 507
        },
        {
            "id": "b0c73f3f-f5e0-44c4-91eb-fe5d0131fae8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 508
        },
        {
            "id": "215e47f5-a462-4a2a-a9fe-3ee349abe349",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 509
        },
        {
            "id": "3339dfd1-f696-411b-b211-9e4285a0ccb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 511
        },
        {
            "id": "70fcb9cf-b3d2-41a8-b46b-b2133c8605ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 8218
        },
        {
            "id": "ff415b86-62a3-46db-af32-1bb63ada8e79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 8222
        },
        {
            "id": "41e0e584-43ab-432e-96e5-c61e558b5bbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 8230
        },
        {
            "id": "2a3b8191-129c-4557-afdb-a838c913690a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 44
        },
        {
            "id": "ccc8e82b-f9d7-4814-9527-46d2f1239e12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 46
        },
        {
            "id": "ff63e282-0dea-4f52-85a5-61de3ef2a320",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 84
        },
        {
            "id": "11215b0f-a049-4b9b-b79e-c231f9ddfb3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 88
        },
        {
            "id": "cba23838-8f59-4b2d-82d0-d42c70d493ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 90
        },
        {
            "id": "a901bc00-98c9-4293-b722-68439843dfbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 354
        },
        {
            "id": "af9ad008-0528-4852-8627-45ee9d81b177",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 356
        },
        {
            "id": "c3993fba-6c59-4ce0-842c-c1eb97c6e058",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 358
        },
        {
            "id": "9ab4ec18-c3a3-4d22-9ea4-e26dec4e7435",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 377
        },
        {
            "id": "ba1094d3-d1ab-47a5-afb5-4f17e77314a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 379
        },
        {
            "id": "b39a3a50-d789-44ec-8a57-5e2bfc9ae996",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 381
        },
        {
            "id": "a7ddcfce-b686-412e-a92e-0ad5e3c417f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 8218
        },
        {
            "id": "b9b3a98c-339a-4cd9-901a-0828cd1a7ae7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 8222
        },
        {
            "id": "ca39dc52-f53e-44bf-af61-2229121b444b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 8230
        },
        {
            "id": "99f8d0d0-a16a-4e47-9c56-e49a1fbd44c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 59
        },
        {
            "id": "37e512c3-7c95-4447-bcfe-e9fd5dcd5aea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 74
        },
        {
            "id": "aa93edc8-eeb8-4297-8160-7f10712d4d92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 84
        },
        {
            "id": "8b268419-0e0c-48ed-97f3-d94d409364eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 99
        },
        {
            "id": "3a8dd7ca-06b7-47ec-9445-33f2887a45e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 100
        },
        {
            "id": "f44b725d-37a9-401b-abfc-24821532cb8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 101
        },
        {
            "id": "c4deabda-a099-451f-b806-ccdf3a6155d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 103
        },
        {
            "id": "604114f4-0671-4b26-ace5-86154ab6c36a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 111
        },
        {
            "id": "56b523e7-eeab-4812-a58e-99622baa74b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 113
        },
        {
            "id": "c2b327bf-7913-4565-a8f1-4883b24c8e54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 231
        },
        {
            "id": "841a0c19-28b8-4dbe-8067-4da75948d0e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 232
        },
        {
            "id": "e67bcd4b-2313-4bae-97b0-f63e3bf311e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 233
        },
        {
            "id": "28c80b6d-d959-47f6-81bc-6a907e645d19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 234
        },
        {
            "id": "58ff7a37-14ed-40cb-b6d9-88f9e0d26996",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 235
        },
        {
            "id": "1b427401-13a8-48b9-8c8e-c2e35a6224a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 242
        },
        {
            "id": "50f9e7e7-7e66-43ba-baa3-73db05e41a10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 243
        },
        {
            "id": "997ce37d-54b1-4612-b7f7-f5a3104551fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 244
        },
        {
            "id": "5ffcb6bc-a2f3-47df-a017-41c80265e0b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 245
        },
        {
            "id": "9225273c-dd42-47b9-afc6-f91ce430f006",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 246
        },
        {
            "id": "fd843046-22ad-4131-ba43-341a3e26a3f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 263
        },
        {
            "id": "e356f55f-b3a5-4cf5-bcdd-ca4663ba4c92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 265
        },
        {
            "id": "001c7a25-c047-4050-be34-9c666ff025d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 269
        },
        {
            "id": "8434680b-ede4-4325-95a0-3b23487879ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 271
        },
        {
            "id": "db5999ac-cf52-489c-b6ab-f4b27d9815c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 273
        },
        {
            "id": "1b006630-b586-4b9d-8a6e-4593a2eacfdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 275
        },
        {
            "id": "be552c05-0c94-4380-96af-ab1cc4275fee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 277
        },
        {
            "id": "420e8be3-4de4-49d6-b801-0dc678955a54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 279
        },
        {
            "id": "b68a3291-1f35-467c-a7e9-0920203d3518",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 281
        },
        {
            "id": "1e4555f5-aba3-41ac-824c-2de3d7cc506e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 283
        },
        {
            "id": "a0d5dbec-12cd-4d17-af95-f88a19fd055f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 285
        },
        {
            "id": "386dcc2f-fb4b-4435-ace7-d611fdf17767",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 287
        },
        {
            "id": "717740c8-3fd5-4636-98fa-7cfb9e8d2e9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 289
        },
        {
            "id": "945af45c-b048-450d-8a1b-6853a4c2d8b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 291
        },
        {
            "id": "49fd3c4c-353f-4ae2-9963-5c4d6cde1727",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 308
        },
        {
            "id": "e47b8767-acaf-4dd9-bd02-1bdebc2dddd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 333
        },
        {
            "id": "0749ce91-1426-45af-a64d-f576fc06b7f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 335
        },
        {
            "id": "aea6e09e-f698-4151-8052-9b0652dc8c76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 337
        },
        {
            "id": "1b933593-d17e-42c3-abb1-cff781605d05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 339
        },
        {
            "id": "2c192f03-7454-448a-bd54-2c998b96ce31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 354
        },
        {
            "id": "0fbe44ca-ff28-4259-8807-58a38fb49d08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 356
        },
        {
            "id": "e557bd8b-8876-4792-8d95-0dd9e7df97d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 358
        },
        {
            "id": "ef7e1b85-7b07-488e-9ea9-025b0ae0bf4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 511
        },
        {
            "id": "5229fa6a-a767-4a34-866f-c9deeaeb79fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 116
        },
        {
            "id": "f87a077e-5913-495d-9613-9f9188f9ae8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 118
        },
        {
            "id": "eb186512-00ca-4994-bc7f-4ba9159ed419",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 121
        },
        {
            "id": "f7c4ba98-a222-4a2a-bf92-6d37f4a554fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 253
        },
        {
            "id": "f0018729-fc91-4f31-b66b-73d5004cc923",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 255
        },
        {
            "id": "0a03d56e-ed11-4e6e-a820-4496fb778cd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 355
        },
        {
            "id": "db8b0bf6-ab16-49f4-a7d1-3de3300dd8e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 357
        },
        {
            "id": "b752d509-f8bf-4c84-8101-5abad1917046",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 359
        },
        {
            "id": "75f17c71-7a72-46be-8744-b0b20aac7cab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 375
        },
        {
            "id": "6245dec4-aa10-442f-815b-da735ea3c4a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 7923
        },
        {
            "id": "22c8c0a7-53aa-44b8-a674-d43846a12641",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 8218
        },
        {
            "id": "764536a5-8870-4119-a075-34578f259e7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 8222
        },
        {
            "id": "1e031dff-19d4-41e1-8382-216f368614a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "3acafe34-66a3-403b-9acf-3001ad48ad52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 46
        },
        {
            "id": "7d20cb72-42ba-44cd-a4a1-248cb1386297",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 65
        },
        {
            "id": "0af0d65f-f730-4591-8424-98fda9c8f8b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 67
        },
        {
            "id": "734840b9-9a64-4b2f-b417-5f949ff17077",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 71
        },
        {
            "id": "988329d7-e1dd-41db-b2ba-173b1b61ba61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 74
        },
        {
            "id": "e188c0df-4de5-4c1c-9169-7f90dc442f23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "104cfc8d-8699-4638-8391-7476ee836ff5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 81
        },
        {
            "id": "7b14b9e8-7a1f-4ced-a024-a0e3a404b4f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 84
        },
        {
            "id": "35c4d277-80b1-4185-800f-2471cc30b199",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 86
        },
        {
            "id": "636f74a2-2053-4ae6-8c44-a45c838bb73b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 87
        },
        {
            "id": "faee394c-272d-4331-94dc-3e4d9500b2ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 89
        },
        {
            "id": "29e6061a-7893-48f1-87cb-2c9688f209c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 97
        },
        {
            "id": "fd6ff104-8ccd-4f8d-998d-3901535e0b25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 99
        },
        {
            "id": "f946e994-af9f-400c-bc02-e5e64abb8915",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 100
        },
        {
            "id": "9d83d377-daa2-4316-8896-241ab0335d2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 101
        },
        {
            "id": "a3a8dc82-b41a-4cc7-9ff1-dcf7589d82f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 102
        },
        {
            "id": "370de7de-b7ad-4f74-bdc5-def64581944d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 103
        },
        {
            "id": "084275c7-5426-41d6-8c87-2f7313eedb60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 109
        },
        {
            "id": "bc534c6d-4e52-4a0d-8bc6-9dc49c1b5d64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 110
        },
        {
            "id": "774f51a4-e15e-4024-a515-ca897b04c33e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 111
        },
        {
            "id": "64b9cc2e-a40b-498c-8dc8-34d2ff81c6a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 112
        },
        {
            "id": "67f0c581-c71a-4419-a513-13dda78ff263",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 113
        },
        {
            "id": "259bbb40-b265-45ca-b945-230487994b09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 114
        },
        {
            "id": "d20fe16d-c808-437a-ac9c-e8a9f2fc4418",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 115
        },
        {
            "id": "8d5a7d9f-008f-44d8-a894-b558d36cb9b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 117
        },
        {
            "id": "47f721a2-3460-4a2c-807d-0034910930c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 118
        },
        {
            "id": "8c54e6df-b34f-412e-8d56-dc795390d342",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "f4f03258-eb84-49bd-a620-250751941508",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 120
        },
        {
            "id": "9d3e9d3a-058b-41f1-b705-4f78eb3d9d8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "e994799a-e545-4253-809a-b45dd9e4e33b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 122
        },
        {
            "id": "4fce92c0-a53b-4c66-9a63-486942c4d5de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 171
        },
        {
            "id": "91a643e8-0c2a-4c19-a281-fa5529e20395",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 187
        },
        {
            "id": "26ba3e3d-2c5b-4d32-bea5-17d43ca508f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 192
        },
        {
            "id": "35cd431c-9c96-4823-a6ee-3cdeab479027",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 193
        },
        {
            "id": "f3d713b8-9d9e-4f0e-b94d-48292df4ad6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 194
        },
        {
            "id": "9781fcf1-5bb2-4396-b9a5-8da3b6e925db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 195
        },
        {
            "id": "51e15a5a-28d5-4bde-aa9f-6de593e4c5af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 196
        },
        {
            "id": "29b82aa3-89f5-420d-a2f0-4251f7e262f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 197
        },
        {
            "id": "81bc2998-2e2d-4f2f-8a20-fadd0b6f3602",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 198
        },
        {
            "id": "a8a7c03f-7a9b-4e03-a501-2257e76d5e80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 199
        },
        {
            "id": "169c5ebc-792e-44e7-9449-51b82aa6a90a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 210
        },
        {
            "id": "7f288d9a-9d19-42af-97b9-80cf694c6358",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 211
        },
        {
            "id": "07f9702f-7b43-4493-b027-7579d9c75aad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 212
        },
        {
            "id": "e4e2f4f8-1e64-48ec-92bf-c99da1042b54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 213
        },
        {
            "id": "fc517490-0275-472f-aff2-484e74042975",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 214
        },
        {
            "id": "d70a18c0-abf9-48a1-bc44-6815f28ceef7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 216
        },
        {
            "id": "3652f6c1-9d20-4349-bbda-20426adcbbd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 224
        },
        {
            "id": "e5205989-e223-4aa6-b499-714c42aa1c58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 225
        },
        {
            "id": "5c053dad-3212-4d54-8dfd-1258f0c2ec13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 226
        },
        {
            "id": "2394ed78-5a3d-44c5-9ade-756e4576ad22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 227
        },
        {
            "id": "cd04359a-cf69-4fcc-80df-c00889c32781",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 228
        },
        {
            "id": "40811854-6ff6-400a-acf0-07ccf9f99d46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 229
        },
        {
            "id": "73504d5a-e9c4-4605-912c-a9a5b78615fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 230
        },
        {
            "id": "b37af624-102f-43de-b6f5-c946b16d6ba4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 231
        },
        {
            "id": "78853dd4-0c29-4aea-9d74-e987e5a4d0e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 232
        },
        {
            "id": "92a1ca6d-c7b1-4144-a6f2-e9afb762f853",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 233
        },
        {
            "id": "33f11af9-4964-4c9a-bc87-8c73c6f16c32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 234
        },
        {
            "id": "c5d77f5e-cc59-4208-af0a-405294168d13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 235
        },
        {
            "id": "95312527-6126-4d54-900f-1099c427592c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 238
        },
        {
            "id": "bab65053-6fed-48ee-b28d-106be9ea1060",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 84,
            "second": 239
        },
        {
            "id": "16aca27b-0a92-46b5-b75a-11c7f3e1f98d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 241
        },
        {
            "id": "9f4a787c-2d41-46b9-8515-a58510adf992",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 242
        },
        {
            "id": "4f90a9f3-dd09-4e57-a59f-53f262d33502",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 243
        },
        {
            "id": "48a54da8-5f8a-4569-9f8f-64e0256c3618",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 244
        },
        {
            "id": "89b9e00e-6d30-4830-9250-4281caca36cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 245
        },
        {
            "id": "67e65e86-057c-4a96-a818-703064ad0916",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 246
        },
        {
            "id": "5e17433c-5bd3-435a-8aa1-b0e0db26d41b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 249
        },
        {
            "id": "b4f353ef-2a5b-4788-9c8e-04be40e1ca01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 250
        },
        {
            "id": "efd46f65-e880-4748-9b4a-9dd609d1f6bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 251
        },
        {
            "id": "12e7bc60-cef5-4270-a86c-6110c15f5b2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 252
        },
        {
            "id": "1c3594d3-fdc2-428a-b62f-76a4d95cefa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 253
        },
        {
            "id": "13abff5d-c4d9-479d-b928-1104cb37e1ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 255
        },
        {
            "id": "5d7881ac-33f3-48ea-88b6-a0d51d0c8a62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 256
        },
        {
            "id": "e744f445-e7d8-4b36-8ea7-f6c2af1fb2de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 257
        },
        {
            "id": "2a37305b-5e2c-4af3-a423-77894588728d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 258
        },
        {
            "id": "b4b7f070-2b48-4ea6-94ef-0f8156f31688",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 259
        },
        {
            "id": "aea812d4-6c27-43c3-a7fe-72fdd318b934",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 260
        },
        {
            "id": "b2248524-ac20-4f42-8fa4-ee12902ee93e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 261
        },
        {
            "id": "05643157-36c8-49e6-bf11-c25976a19e6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 262
        },
        {
            "id": "71d1329f-713f-4db1-9180-ea767306a6ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 263
        },
        {
            "id": "b617a5f2-a77e-4713-a022-afa1c6be33bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 264
        },
        {
            "id": "dcbb6df0-b0c2-477d-9794-fc70c7c11c0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 265
        },
        {
            "id": "003f96ea-cb31-4fbf-b527-fdc22671d84a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 268
        },
        {
            "id": "221034ef-f9af-4c7b-9460-5b401a048de0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 269
        },
        {
            "id": "fb726340-781b-4151-a057-683dc1471437",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 271
        },
        {
            "id": "1fa4bf2d-3d6c-43cd-ba70-9b33b7ca3e26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 273
        },
        {
            "id": "30ce7f81-be2c-4ff4-8c04-4c5f5012c780",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 275
        },
        {
            "id": "c4124c91-5b01-4564-8c50-887ccd8e7cab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 277
        },
        {
            "id": "374b9d5b-8df3-4f98-84ed-d526f50cea61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 279
        },
        {
            "id": "68f7677c-209c-48ce-a6e2-c97ba03b8bf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 281
        },
        {
            "id": "4fa603ea-12e9-449d-84a1-020f704b1a8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 283
        },
        {
            "id": "583a9d2d-f993-4f98-b1dc-684a2f0f5bb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 284
        },
        {
            "id": "97e742f2-4404-46fe-bffa-c2dc1857cd7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 285
        },
        {
            "id": "66cfea46-fa0b-4ada-9827-4dd6fca9c495",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 286
        },
        {
            "id": "95661e76-15b0-4339-b02a-2017d6c3d08f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 287
        },
        {
            "id": "6f3d2cc0-7f55-428a-9711-dc4527d73a3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 288
        },
        {
            "id": "c37025c2-935c-4ec0-8790-c00c6fe984d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 289
        },
        {
            "id": "4d6d6e38-6c8c-4d8e-acf3-c9294285e499",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 290
        },
        {
            "id": "01396af7-b430-4e93-bd18-a582cb621ea0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 291
        },
        {
            "id": "b6a0d571-20b0-4d7e-abef-7183336667e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 297
        },
        {
            "id": "c2532b58-a6b8-4a31-bd1d-63c8508a1708",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 299
        },
        {
            "id": "4519b86f-1cbd-490e-9f02-971df0719fb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 301
        },
        {
            "id": "06b28aad-481b-43bc-8700-4d6f8c8397c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 308
        },
        {
            "id": "b8acb42c-8031-42ad-8168-1a58744bfc6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 324
        },
        {
            "id": "cb641cd9-ecf1-43cf-88af-7333daf12536",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 328
        },
        {
            "id": "451c033a-ab01-457a-b87f-71880d0aa41f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 331
        },
        {
            "id": "85cf8d0e-9072-4d3d-b06e-e7e2690f62c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 332
        },
        {
            "id": "72444b20-c397-44cd-ac77-613bad757195",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 333
        },
        {
            "id": "5ab46b0f-c7cf-4617-89bb-e7b03cb73a69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 334
        },
        {
            "id": "d8c165b6-a460-4ea2-9c56-7a13e34c837f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 335
        },
        {
            "id": "9b90e88a-4b5e-4aaf-b7d2-9f394871147f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 336
        },
        {
            "id": "924ab95e-7c52-402d-a1bc-9d03a2641219",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 337
        },
        {
            "id": "98b57aa1-79d4-4cde-8521-818030d7cdda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 338
        },
        {
            "id": "fff4b335-0e08-425b-a99f-ceff5bee6e58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 339
        },
        {
            "id": "df994238-64d5-48c5-b06d-7b4162933f42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 341
        },
        {
            "id": "a68160ef-f4b8-46a5-afe8-1cc44bfc684b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 343
        },
        {
            "id": "ea815234-c561-49d2-b0f3-ea48691ed78c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 345
        },
        {
            "id": "87c8add6-7aca-4f25-a777-c5574cf15acb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 347
        },
        {
            "id": "d5957fba-ee9a-4034-a198-3bc9d3affdc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 349
        },
        {
            "id": "f59f8e42-d4dd-4d52-8431-b2302d9f2cd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 351
        },
        {
            "id": "b0c00687-33e7-4d8f-914a-02dd7075613b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 353
        },
        {
            "id": "471e5f27-86a5-4a5f-ad52-2da87be47615",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 361
        },
        {
            "id": "5647a6c5-3dba-4db7-a1ef-0b9685c90d0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 363
        },
        {
            "id": "01adb886-37a9-41ba-b9e0-bd0dd384c4a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 365
        },
        {
            "id": "2e5a0a4d-52f5-4099-803d-e722526293f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 367
        },
        {
            "id": "d6288692-8aac-4835-8c94-97ca2d311736",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 369
        },
        {
            "id": "766bb4b3-9139-4f4e-b791-3edaae9c255d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 371
        },
        {
            "id": "94dc0b1d-0940-4282-9c5b-c58a469ddff0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 373
        },
        {
            "id": "16f50a56-1ec8-415b-8a44-03ebfdb7c1d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 375
        },
        {
            "id": "2ef4430c-b008-4167-aee9-f588fac46a87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 378
        },
        {
            "id": "1c276e50-b8ec-4ead-b0d4-05dec9ea7dbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 380
        },
        {
            "id": "fa06d8f1-cfe2-4e90-9b9d-4d7628d549d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 382
        },
        {
            "id": "55087c74-383c-4cdd-8149-2e8f9a558a99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 506
        },
        {
            "id": "b9391b1b-d9ec-4230-baa8-ca65dbd99e0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 507
        },
        {
            "id": "42822777-c874-4cab-bd34-1281ba073aff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 508
        },
        {
            "id": "bf679879-3830-4955-8c66-29b40bd566a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 509
        },
        {
            "id": "8d3de740-3230-4b58-819e-014db11067aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 510
        },
        {
            "id": "d57fcf2b-9a24-448a-8fee-ccd72601e8b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 511
        },
        {
            "id": "48cc6779-ec9b-41f2-8a2a-92536852484e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 537
        },
        {
            "id": "98a3fa01-9d43-43a4-b6df-3b5f2d414e07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7809
        },
        {
            "id": "760f6b1e-1e70-40a2-951d-1e49347e3914",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7811
        },
        {
            "id": "1ee9706f-ca2c-443b-97f9-797a6bf3bebb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7813
        },
        {
            "id": "f7705c2f-d8b2-4e09-b31e-e0d2794f02ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7923
        },
        {
            "id": "9b5649f2-364f-4622-aca5-0640697e5215",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 8217
        },
        {
            "id": "aced2e4f-9ead-4924-8054-b5c31d434df9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 8218
        },
        {
            "id": "e78ee8a0-8414-42ab-9747-cdad1d7381cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 8221
        },
        {
            "id": "f7a13d37-1009-4e94-884f-3f33eacf19b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 8222
        },
        {
            "id": "4fe4557a-6a4e-4d31-b10b-e4af9172c504",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8230
        },
        {
            "id": "8d258d69-29fc-45db-8c41-14b77c201144",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8249
        },
        {
            "id": "28642bc4-396f-4330-9b9d-3fb8ce15cbc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8250
        },
        {
            "id": "6cc35199-43a8-4954-bae8-2d403d6e39b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 8482
        },
        {
            "id": "9db23396-54c7-4c35-be43-881e8a5a3cbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 65
        },
        {
            "id": "6350c17b-a353-4e86-b135-b2ac7a83533b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 74
        },
        {
            "id": "0437c4e7-f4d0-41d7-91f0-56c32da9ea4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 198
        },
        {
            "id": "4bf1a263-49f8-4a8b-bdb7-f5d7fbff3933",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 256
        },
        {
            "id": "62c9218d-9bce-46e5-beaf-c17e7744d907",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 258
        },
        {
            "id": "cd29e6fd-0305-42f9-bad6-c0dd99794ff3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 260
        },
        {
            "id": "88a7150d-f37f-4f8c-9d84-9d04db771dc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 506
        },
        {
            "id": "2a048345-a782-41d3-989b-7932ee2e74de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 508
        },
        {
            "id": "fe21697b-6076-4f08-b760-62eb7fca8c2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 8218
        },
        {
            "id": "130772c6-9426-426d-b89d-f9dbd7787823",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 8222
        },
        {
            "id": "fae6efee-ab3b-44a5-80f9-1b4bd9464b06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 44
        },
        {
            "id": "ff369194-e94c-490e-a723-b25f25e3fdfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 46
        },
        {
            "id": "9d16dff1-0541-4cb7-a768-4b3e666e5335",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "43fbe09c-d8c7-42d4-8c7b-df1cf5409f87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "7a0de710-1a83-419e-93f0-6a81acd2f1b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 65
        },
        {
            "id": "1aba8414-c365-41b7-9e55-d6a30c16c95f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 67
        },
        {
            "id": "bcd1258f-784b-4f3a-91ef-8f4c423c7292",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 71
        },
        {
            "id": "898f51d6-8c4a-4a37-8403-4fea6d9cbce0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 74
        },
        {
            "id": "e36d4e8c-444e-4490-a975-64f97697e7b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 84
        },
        {
            "id": "1fb9d6c7-ded1-4e06-adbb-fa121c48b1ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 97
        },
        {
            "id": "47d0ec1a-c232-4bc2-9c88-797a11ab7237",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 99
        },
        {
            "id": "f80eebaf-36b4-4766-9129-9f4208391816",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 100
        },
        {
            "id": "7380516d-917e-43f2-a1cd-60d541164b61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 101
        },
        {
            "id": "19437686-b85b-4bee-a351-ea6844698f81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 103
        },
        {
            "id": "bc236e22-0d1e-49b9-a3bd-4e66adb09c1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 109
        },
        {
            "id": "735e54ae-ed0c-4c79-b074-baa9c1e99f2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 110
        },
        {
            "id": "b45c28c8-7346-4f0b-b15b-e3a291429df6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 111
        },
        {
            "id": "b4314cf7-dc4e-49a9-93f4-e85e174651cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 112
        },
        {
            "id": "5199df80-df9d-4fe5-9de4-d11eeb6d5ed7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 113
        },
        {
            "id": "85229ca4-03c9-41d1-b380-70bc0b15094c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "1a556779-8f20-4e34-bdce-d687202dd5a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 115
        },
        {
            "id": "a3c57def-6647-4ae6-be15-11fd9db70ed4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "a1780d03-086b-4f4d-a6fd-be4f14b677d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 171
        },
        {
            "id": "049a2e98-057d-4028-9a08-45fcba781cee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 191
        },
        {
            "id": "d49969c7-1c09-4388-9c00-92a5d75e014f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 192
        },
        {
            "id": "50ee4a1b-ef82-42c2-92af-fceaa032f911",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 193
        },
        {
            "id": "95caaa03-fab6-4a0d-8c0d-e7cfb220deb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 194
        },
        {
            "id": "e30d9703-6157-494d-8d3a-8e76f44d88de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 195
        },
        {
            "id": "5606be0e-720e-4ab0-806b-5fb62e9a17eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 196
        },
        {
            "id": "58023d7d-b0b9-48dc-8bcb-0ad3b8562e75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 197
        },
        {
            "id": "0a1f975b-2455-4f6e-9c73-017bbd6ee0d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 198
        },
        {
            "id": "7e728490-f4b6-46ec-a41d-6380013a7d35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 199
        },
        {
            "id": "0197d82b-cde7-48ac-b4a7-81ac66ce5c52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 224
        },
        {
            "id": "084aeec8-d33a-4aad-8dee-4bab1fc6a0ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 225
        },
        {
            "id": "f1f8ff1c-6e16-42c9-b048-b56b0f367d4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 226
        },
        {
            "id": "75bb51f1-ae67-4a29-b954-b11821020792",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 227
        },
        {
            "id": "a992cde5-4479-4eeb-be4c-c70f32d4e805",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 228
        },
        {
            "id": "f079209b-ad96-4b37-b71e-1fe40c9204f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 229
        },
        {
            "id": "e818bb7b-1a9b-4a9c-ac4e-5adbe93c6d49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 230
        },
        {
            "id": "cf794f73-b179-4c1c-8047-cdd89f2a02fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 231
        },
        {
            "id": "81586b85-029c-4007-b2be-27949eb4cf7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 232
        },
        {
            "id": "90db4c2b-4ea0-441f-8057-d1e43da7d2a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 233
        },
        {
            "id": "79d0332d-41b4-4308-8d42-c5acc116e544",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 234
        },
        {
            "id": "e2dce81e-6fe9-47f5-b78e-65f81514ec65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 235
        },
        {
            "id": "57e15cc7-e5ce-408e-8902-5816f6a6a66d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 86,
            "second": 239
        },
        {
            "id": "d283c506-71ef-4d90-9359-6669e417b4a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 241
        },
        {
            "id": "2b6ece24-3b80-4a84-b21b-b8f2044fa914",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 242
        },
        {
            "id": "d4f78ae0-0b52-43ca-9793-6ea5c8749187",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 243
        },
        {
            "id": "65cc397c-832a-483c-8569-7ca02a230e05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 244
        },
        {
            "id": "37b7d907-9a3b-4698-93fc-51912f9c05c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 245
        },
        {
            "id": "ccc714df-7dbb-4e28-84d4-1dff7832aa74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 246
        },
        {
            "id": "912fc9ee-4cd6-4b81-86ab-c7cc9201fbe2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 249
        },
        {
            "id": "bb1d0cc7-f90c-457d-a695-c0bd4fe0c28a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 250
        },
        {
            "id": "dcdab48f-083b-4d9c-b262-66463a7ad085",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 251
        },
        {
            "id": "e9b23607-664c-4a29-9853-73f3d2a88428",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 252
        },
        {
            "id": "6ee33acd-9627-47ad-ba6b-7708d11d633b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 256
        },
        {
            "id": "551dabea-9fc4-477e-a6ad-d591209eee20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 257
        },
        {
            "id": "63f96158-3995-4427-8901-96fcc6bcdcd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 258
        },
        {
            "id": "6ac1c4b9-565d-4351-912c-30cb6875b7af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 259
        },
        {
            "id": "a1c6f1c6-98d1-47d1-ada4-607895da5bff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 260
        },
        {
            "id": "2fb7d9bc-68e6-4e35-8ee6-49ace7d485d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 261
        },
        {
            "id": "807405e0-078c-4559-9916-dfbb61a5fe4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 262
        },
        {
            "id": "2fe641b9-c3a9-4f7d-a797-9b2fa9bd8ffe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 263
        },
        {
            "id": "567afbd0-64cd-426b-8207-410fde1870b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 264
        },
        {
            "id": "ca18ce37-e25c-4f06-b557-8637da96e908",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 265
        },
        {
            "id": "05e1902f-2096-4772-96a4-ef12fdf4c215",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 268
        },
        {
            "id": "b646a5be-eba8-4e09-8088-1f6775c4b08c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 269
        },
        {
            "id": "ec3e9f3c-66c1-4267-8159-766eeddbf55e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 271
        },
        {
            "id": "e68bbb41-89dd-406f-91b6-9036744637fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 273
        },
        {
            "id": "4469b583-2390-4641-8a6a-7e8e56de7a9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 275
        },
        {
            "id": "74722b4c-03f3-4375-be2e-e8ef71a46534",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 277
        },
        {
            "id": "9f73bc15-8713-4eda-ae3a-167e95fd72cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 279
        },
        {
            "id": "a3bacdb9-fe07-49b0-9b93-e557416a8cfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 281
        },
        {
            "id": "1932df7a-88d3-40f3-b52d-f681032ab7b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 283
        },
        {
            "id": "10d0fb02-d6a5-4191-aeec-49a43555c552",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 284
        },
        {
            "id": "944a21a6-8692-4145-9d90-5a79f78c8233",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 285
        },
        {
            "id": "d613c33a-d051-4226-870b-aed57aee661c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 286
        },
        {
            "id": "1194ccb4-3626-4e85-8fe7-35cb2170518a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 287
        },
        {
            "id": "8bc89d12-6ecf-4152-98b3-e3f24616335b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 288
        },
        {
            "id": "7faf7508-da47-4388-a0d0-d003c9e647d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 289
        },
        {
            "id": "d902e0a3-637a-4fe0-aa8a-5be6fa780c65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 290
        },
        {
            "id": "2183927d-c1bd-4331-8ff5-619b7c13420d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 291
        },
        {
            "id": "4859c906-854a-47ae-9e45-3c516f4ab196",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 297
        },
        {
            "id": "f4e29143-e59a-4db5-abd5-9fc3ca34a283",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 299
        },
        {
            "id": "76958212-78bd-4cd0-814f-56e2b64989ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 301
        },
        {
            "id": "f461edeb-84dc-4e43-80c1-8b8b1b96c9e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 308
        },
        {
            "id": "91e263ae-2ee3-49f5-b424-6d7467da18be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 324
        },
        {
            "id": "23ac402c-0b3c-4be4-b07e-4f59b88fbbba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 328
        },
        {
            "id": "617b6630-bc6f-40dc-a4db-272328ce3d6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 331
        },
        {
            "id": "b7ec086f-e1f5-44d2-bf4c-6fd5155b393e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 333
        },
        {
            "id": "387e8c6b-d5a1-4249-b34a-3b7e2b399f36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 335
        },
        {
            "id": "8ce855a3-08e0-4b06-9ae2-9bd1cfab2623",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 337
        },
        {
            "id": "4e06a386-df90-4375-b73d-f76486d0c226",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 339
        },
        {
            "id": "af46d164-f625-414c-a901-c5a4e551edb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 341
        },
        {
            "id": "466471fd-0cc6-48a3-a247-f653abc94960",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 343
        },
        {
            "id": "5e6041be-1447-4448-b4f4-392d5ed91737",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 345
        },
        {
            "id": "088d2d3c-c3d7-4a08-8b4f-e02db8a2b796",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 347
        },
        {
            "id": "18c2ba49-3357-4a64-8f72-ea6615464885",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 349
        },
        {
            "id": "67005f8e-691b-44d6-bea8-0d11f19a4cb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 351
        },
        {
            "id": "21d67cd8-aa14-4cad-affc-de5ef2432c56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 353
        },
        {
            "id": "7eccd93a-5597-4e4d-9fa4-59498d61d1bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 361
        },
        {
            "id": "9cacaf6b-de76-434b-af66-41dd650a9f2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 363
        },
        {
            "id": "6df7ca83-1328-460d-94f9-59cbc8e82dcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 365
        },
        {
            "id": "662ee5e9-9352-4704-88d7-2cf3649c770b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 367
        },
        {
            "id": "214b8f5c-d22b-4b0b-8b92-cf6385240b94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 369
        },
        {
            "id": "3d242775-0803-4351-9d5b-48926147dd22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 371
        },
        {
            "id": "4a85ebe6-e6d3-4e5a-b96b-b62b0be053d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 506
        },
        {
            "id": "1579bd98-1ea7-4e3a-8b19-ad3a764d82bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 507
        },
        {
            "id": "e980abe4-1437-4549-a5f3-7c10139866c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 508
        },
        {
            "id": "839a8453-f7ba-497a-b10d-0f112dd0da9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 509
        },
        {
            "id": "871caa28-9dc6-4dd6-913c-fddf1169f51d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 511
        },
        {
            "id": "5e035091-41f3-4267-82db-c089169ec33f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 537
        },
        {
            "id": "5a80464c-e735-4ca9-ad01-8e99429bdf83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8218
        },
        {
            "id": "437f4fe2-c5af-4c92-9afa-6492319eba0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8222
        },
        {
            "id": "219c997d-6c2f-4d86-a360-238b5ecbb004",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 8230
        },
        {
            "id": "685618f4-b75b-4531-8f70-bd480fcde12e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8249
        },
        {
            "id": "d24e3e63-b552-4de7-9ea1-4064cd67957a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 8482
        },
        {
            "id": "709a957f-02cf-40ae-bbcd-d172f074a042",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 44
        },
        {
            "id": "8c7e03ba-14f3-4927-b8ee-af0dceefd2bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 46
        },
        {
            "id": "cac60e38-3492-47e2-b37a-eded474140dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "2c6fecda-4d92-4d59-83de-1e46045a472c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "080b8b1f-ed65-406f-a2e0-ebb695ad9ec9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 99
        },
        {
            "id": "a42f21c0-9126-4660-8aca-cc6f2255798f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 100
        },
        {
            "id": "e988bad1-3909-401f-8f85-26fd69139fac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "8ca0af32-942c-4bd8-88bd-519bebc7ad82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 103
        },
        {
            "id": "dced1b75-9a94-40c9-8b4d-9f370d6ee19e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "aa95351e-2310-4f41-86ab-138c94de04c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 113
        },
        {
            "id": "fe5f7b92-e399-475d-bb69-cbb9990cd825",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 171
        },
        {
            "id": "e292f1d7-e2c6-4ad8-92ba-34814e93a4af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 187
        },
        {
            "id": "1ac9dd0d-c98e-405d-978f-9f4d4003b523",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 191
        },
        {
            "id": "eeb89be5-0960-4a01-a7b9-ab364288bbd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 192
        },
        {
            "id": "2bc5a809-55d3-4d09-8380-f3a75739bcd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 193
        },
        {
            "id": "0b2abd85-70c4-44ac-8bf7-85c971f68d0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 194
        },
        {
            "id": "058cd2c0-5553-424e-b074-4c9860f6a47c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 195
        },
        {
            "id": "4a14935d-3e7f-4fda-9b6e-696c528532a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 196
        },
        {
            "id": "c171db75-0299-4478-b75c-4b7106b4516c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 197
        },
        {
            "id": "0bf2857e-933b-45df-8dbc-8ca0666da731",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 198
        },
        {
            "id": "8a462a89-0583-4102-a19a-9cf9881ca9c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 224
        },
        {
            "id": "4d9b29f5-25fa-4f98-9a70-80985a2d062b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 225
        },
        {
            "id": "c0d7e9da-dd9b-4ac6-ae6f-333ca572a784",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 226
        },
        {
            "id": "88997a6f-bb15-4116-a71f-6d4b1b5bcc10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 227
        },
        {
            "id": "be88f30a-991d-48b6-9a5d-f6f83d9df59b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 228
        },
        {
            "id": "48db524b-8f58-4339-8b03-594f02fc45b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 229
        },
        {
            "id": "e5be399d-cd53-4cbf-ac9f-050c7717c6bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 230
        },
        {
            "id": "04b9a1e6-9022-4450-a264-e20470c0180a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 231
        },
        {
            "id": "a9b5c9fa-6411-474f-8c4b-807ece0de5e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 232
        },
        {
            "id": "cc7ec4ab-4008-43a7-8027-799997161205",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 233
        },
        {
            "id": "6290ff49-347a-4cd7-9838-19da6a6fd5d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 234
        },
        {
            "id": "f907de3a-bdca-4467-89eb-08529352a593",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 235
        },
        {
            "id": "43b5ccea-fb56-4ec6-a731-e6d59598cf0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 238
        },
        {
            "id": "f9e87716-e530-4e48-b99b-70cf3baef721",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 87,
            "second": 239
        },
        {
            "id": "eeb21680-1ff1-47f5-8f30-2665d2483467",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 242
        },
        {
            "id": "fdc11f60-2032-4f1d-ada6-619f4acc15f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 243
        },
        {
            "id": "874761d8-d16d-484e-bf5f-33baf8d76030",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 244
        },
        {
            "id": "9537b619-07d3-4d33-8840-a3822bdf0cb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 245
        },
        {
            "id": "bc1c82eb-03dd-40e3-9a87-3823e292f582",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 246
        },
        {
            "id": "5628006e-f126-4c50-b168-51d049697d40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 256
        },
        {
            "id": "702b90d1-7902-44da-8d05-0d3649cc8f95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 257
        },
        {
            "id": "d745b1c3-ee6b-4803-8193-b50e80fa39c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 258
        },
        {
            "id": "6a5b71eb-ccdf-4457-a83b-0aa64ed772e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 259
        },
        {
            "id": "2088ab14-f928-45ad-8dca-9e76f535e67f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 260
        },
        {
            "id": "80a47cc2-b73f-435f-ab9a-1201ad393b30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 261
        },
        {
            "id": "92f9e30d-d6b2-4729-8fdf-3a6c4500dafe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 263
        },
        {
            "id": "dd8eab29-e1e2-4df3-ae9d-5054381e4601",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 265
        },
        {
            "id": "e2bf1c0e-3872-44b4-a790-0353e3801156",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 269
        },
        {
            "id": "91b4ea4c-9bdb-4236-abd2-177871d264a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 271
        },
        {
            "id": "f948ca0b-da30-4fd8-ac4a-572450cb268c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 273
        },
        {
            "id": "fc4cbf94-6679-4f02-95bf-9679dd0e0106",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 275
        },
        {
            "id": "98c98381-ce7d-44ae-8f2d-48c4f1c210de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 277
        },
        {
            "id": "c8db3c2a-ed01-457b-b081-e43fea92405e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 279
        },
        {
            "id": "9db3009c-6728-4745-bda0-759bec6954a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 281
        },
        {
            "id": "436c2f15-d8e4-4653-b666-c706b61881c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 283
        },
        {
            "id": "d6eb8cd3-f94f-41b9-8e0d-fbf546ccd6b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 285
        },
        {
            "id": "6b4cd4ab-0656-443a-92c2-bd365195e0b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 287
        },
        {
            "id": "c666a75a-b072-4918-ad21-c7422eb88589",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 289
        },
        {
            "id": "22125acb-a907-478c-b0e7-1730939f6809",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 291
        },
        {
            "id": "b21a6ba1-90b8-4c9f-95a1-a280aadaa9a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 297
        },
        {
            "id": "a9bfae80-398b-48b6-92cd-c1a7446deedb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 299
        },
        {
            "id": "b0bb3adf-ed83-49fb-9804-d9849a89ad84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 301
        },
        {
            "id": "1a924cdc-f6d6-4aa3-8e75-f34d18adb8b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 333
        },
        {
            "id": "523f76f7-4398-454a-8e2f-ce5637a9b756",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 335
        },
        {
            "id": "ed626734-afa9-46b1-9218-932a17ba0c51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 337
        },
        {
            "id": "10ef7298-5f7c-4126-b381-20551e9ed17b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 339
        },
        {
            "id": "37edab73-e05e-4b8a-aac7-168f3029b745",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 506
        },
        {
            "id": "1455004e-302b-4585-aba9-2d779a8212ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 507
        },
        {
            "id": "702934c7-800f-4047-b604-31bd35e73ab0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 508
        },
        {
            "id": "12c4b5b5-96bd-4022-be21-1b87b02b3a82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 509
        },
        {
            "id": "65f11ab0-3eec-4ca4-af21-b8472d2c8bea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 511
        },
        {
            "id": "a17f66d9-b0b9-47cd-b977-9c1a0e20d6fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 8218
        },
        {
            "id": "617a422f-6fe2-4a1f-93e2-1b1c6c78cc6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 8222
        },
        {
            "id": "89d6052f-6011-49f2-acfe-d3309a14db9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 8230
        },
        {
            "id": "d2fdf7d9-3e99-4a8c-b89d-5df9312caa78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8249
        },
        {
            "id": "e1e6571c-427e-4d19-843b-8e517ee2c557",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 8250
        },
        {
            "id": "51fed957-c93b-4088-8f64-31ead72ca6ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 44
        },
        {
            "id": "5e234494-5d46-448f-984b-e8bc1bf3fabc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 46
        },
        {
            "id": "5460fbb0-7131-4c6f-a9a5-de14553b9c2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 59
        },
        {
            "id": "bb50c0c2-2e5a-4dcf-b882-156e64519f71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 74
        },
        {
            "id": "a9c5964f-1c00-4ce1-9778-1d00301c11e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 84
        },
        {
            "id": "fa06b805-ff6f-41ac-85d5-8947b5b15f0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 239
        },
        {
            "id": "53742c4c-47db-4ed6-9022-6ce4d1402f4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 297
        },
        {
            "id": "686e2568-5d88-44b8-b774-6210c71d4974",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 299
        },
        {
            "id": "27756138-f335-40b6-b377-e2f0cac10d67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 301
        },
        {
            "id": "bdccc256-771a-4e51-ad58-951f6cad9155",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 308
        },
        {
            "id": "a9fa45cf-17f8-46b3-832a-17cbc9ff99af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 8218
        },
        {
            "id": "a1a58ba8-004f-40a7-be81-aef52e7d5ffd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 8222
        },
        {
            "id": "98602dce-8484-40b4-9aec-aefda83e11db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 8230
        },
        {
            "id": "be661503-0d6d-4c74-b875-7b151373f1aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 44
        },
        {
            "id": "f44a56b6-7354-40ef-98d0-59dd3cf6807d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 46
        },
        {
            "id": "210e80f7-237c-47bc-bfac-3836a084b05b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 65
        },
        {
            "id": "37796d35-c57d-4566-acf0-5df6268be7f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 67
        },
        {
            "id": "3efa8e12-dcae-4d69-bbc5-b7f6fd00ea61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 71
        },
        {
            "id": "411f302d-8065-4af7-b478-cf749ef05518",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 74
        },
        {
            "id": "c9064655-cf29-4e75-8929-a468a5a73196",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 79
        },
        {
            "id": "748cc22a-4046-4fab-b298-5852b3b61885",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 81
        },
        {
            "id": "da05e1af-bc61-478c-bf7c-29cd9c6a883e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 84
        },
        {
            "id": "3dff8e5c-975e-465d-9a93-8b5b83f21b87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 97
        },
        {
            "id": "4fb37cf2-f5a3-4a14-a781-dbb9e9f65ff5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 99
        },
        {
            "id": "9e82f183-e765-4eca-89db-63b3cdcc49d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 100
        },
        {
            "id": "ac0116fb-3db2-42d4-8b9a-2ff05a750642",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 101
        },
        {
            "id": "88884ace-64b3-4fbe-9d1c-495eaf9c6720",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 103
        },
        {
            "id": "1708fcb7-5734-428e-a064-764a8c12c5a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 109
        },
        {
            "id": "1815d83c-0792-4851-95bb-dd493184e90e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 110
        },
        {
            "id": "db5abafc-438c-4001-a16a-55ea06d050f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 111
        },
        {
            "id": "3e4591cf-a435-4816-b46f-0f2ce94c7279",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 112
        },
        {
            "id": "92b5b4fb-d977-4875-8d91-2b87a9815365",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 113
        },
        {
            "id": "bbd3b420-fab1-412e-8421-5975494436bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 114
        },
        {
            "id": "182a0cf9-b719-445a-b909-d8c1939954b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 115
        },
        {
            "id": "2c845c94-afa3-4034-bdd3-883455159a0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 117
        },
        {
            "id": "6dddf4de-9321-401b-88ef-9f080d5b66aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 171
        },
        {
            "id": "d97b3070-fd01-4962-ac31-0febb7fef5c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 191
        },
        {
            "id": "17676cdb-2196-4f6a-8daf-7a5155b455a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 192
        },
        {
            "id": "9f2bf1d5-7d5b-48fe-8df0-91c81b9d3817",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 193
        },
        {
            "id": "68f86aa6-2d0e-4eef-ad1c-dcf734bbeb05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 194
        },
        {
            "id": "c5c4b890-34dc-4c9b-bca0-966abb3bd7e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 195
        },
        {
            "id": "9fb9fdbb-0da5-44c3-9f5d-d9c0b7263664",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 196
        },
        {
            "id": "fd1e1c74-e1e1-4025-b617-c19c1559110c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 197
        },
        {
            "id": "f14ffd7e-40e6-479a-b102-e599d221b261",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 198
        },
        {
            "id": "f51a8954-e956-4655-9db0-a7ea7688f5d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 199
        },
        {
            "id": "bfca6b40-31d9-4499-8c59-80ecbdbe7d8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 210
        },
        {
            "id": "a8dc09c4-bba5-41a7-b494-0df847b73490",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 211
        },
        {
            "id": "e88c25c3-7617-4849-a4e6-15cb4e4c6563",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 212
        },
        {
            "id": "e8ccf8b3-a4d0-4130-af01-25f14cad69a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 213
        },
        {
            "id": "8a2b483c-4a4f-490c-988e-f12c425dc4b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 214
        },
        {
            "id": "3b137cd1-1642-4220-bfea-18ce549a87cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 216
        },
        {
            "id": "c56f6604-7cab-44d7-8d62-061cef4bab50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 224
        },
        {
            "id": "62425f50-1c14-45f7-81f5-fc04544465cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 225
        },
        {
            "id": "900ad481-6a6d-4567-9311-68ee2034a572",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 226
        },
        {
            "id": "1fc9f6d2-e7a4-4699-a698-125038b3ea14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 227
        },
        {
            "id": "3349e501-7177-4a47-a769-3ad4506bfe1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 228
        },
        {
            "id": "2443aa47-a881-4b5b-84e4-239ef93db03e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 229
        },
        {
            "id": "e5c58876-2e73-4cfe-8ff2-2f3106425a31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 230
        },
        {
            "id": "5af2f5cb-9794-4390-97da-64faade8eed6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 231
        },
        {
            "id": "88d3eeb0-66a8-425d-9503-2fc486af5c2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 232
        },
        {
            "id": "564919f3-f272-4a59-9659-496fb1a6db72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 233
        },
        {
            "id": "859fef23-54fd-4990-9e92-48b56143aaa7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 234
        },
        {
            "id": "b30f0c5f-67cb-4b88-8c30-103d1bda800b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 235
        },
        {
            "id": "aa235ffc-60bb-4d30-94db-54e21d0e23e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 237
        },
        {
            "id": "b3f50576-f645-4a35-8a68-52df4c6fd3f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 89,
            "second": 239
        },
        {
            "id": "8c715496-0020-48ae-b60f-87bc8e610834",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 241
        },
        {
            "id": "51468264-5b4c-43e2-96d6-8d508f80ac62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 242
        },
        {
            "id": "07a69469-9551-4b39-818a-8522ce74ed1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 243
        },
        {
            "id": "6ff02580-d09b-4494-9007-c64de1ce05ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 244
        },
        {
            "id": "a2fa5df6-b1d3-43d5-aedc-b8e0a13652aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 245
        },
        {
            "id": "a5951249-36b5-41d3-9e21-c6d5612b14d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 246
        },
        {
            "id": "f10e11ae-6c30-44b9-af9b-25b1ff5c877d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 249
        },
        {
            "id": "013afbb3-5707-4f3b-8cae-7c8594df66e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 250
        },
        {
            "id": "d836751f-2fa6-4d16-836f-473eb2ad0237",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 251
        },
        {
            "id": "ca7a1e0d-3636-4552-9baf-4d42a7971f26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 252
        },
        {
            "id": "25ccf1e8-4f6f-46f5-80c3-80dfdc004362",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 256
        },
        {
            "id": "e701e323-c57d-4b16-bf0f-68b7b47316e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 257
        },
        {
            "id": "4ca2f052-73d9-4088-9d58-4587de1bd2d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 258
        },
        {
            "id": "5d656742-5164-4d37-90b8-63d2d0e02ff6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 259
        },
        {
            "id": "011e9177-a09a-4b6a-a5d1-9fc1dd109b4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 260
        },
        {
            "id": "cfd423fc-e5d7-4392-98b2-660a3ebdd808",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 261
        },
        {
            "id": "40f3dc66-4fb9-4e41-b20b-8d4b0b1ef98e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 262
        },
        {
            "id": "2f53b3db-b8b3-4a7a-8558-c2aac0d4c2da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 263
        },
        {
            "id": "51778efa-1206-4e87-aba4-d206dd605f5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 264
        },
        {
            "id": "d6d12ee7-ca76-4dac-9034-967fd737432f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 265
        },
        {
            "id": "fe78a602-cec7-4044-960e-ecd4adef79dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 268
        },
        {
            "id": "6a6c3e8b-4cc3-465f-a097-d42016c97df0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 269
        },
        {
            "id": "ecbbd38c-227c-4353-ab9b-6641eba35d97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 271
        },
        {
            "id": "67924dcd-cd1d-46c7-bea0-f5c12de44092",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 273
        },
        {
            "id": "c85526d8-4bcc-4dbf-904d-60a39b98502e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 275
        },
        {
            "id": "48cb870e-7b80-45db-9348-5fd2eee2796d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 277
        },
        {
            "id": "b71ab188-d6ba-44c1-b744-b91ac0bfa58d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 279
        },
        {
            "id": "542cc326-5fa0-4e04-8e80-bcd3ca9272a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 281
        },
        {
            "id": "4f93896c-e2f2-44e8-9bc1-b87825ee2459",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 283
        },
        {
            "id": "a6ac3f52-d9aa-47f6-bf50-41487f21c16a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 284
        },
        {
            "id": "5f33baee-1fd3-40b1-a9a4-439c786012cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 285
        },
        {
            "id": "6c0a633f-0591-402b-835f-d04c2924bb31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 286
        },
        {
            "id": "d5e6eb01-ba3d-41e0-b7b0-48e42c6fa0ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 287
        },
        {
            "id": "21a89552-a5b0-424a-b2dc-fcfc7e5d1805",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 288
        },
        {
            "id": "dabff1b3-4ac8-48db-b24b-249a7ec0bd37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 289
        },
        {
            "id": "6e54f87c-71e9-4551-b4ad-4ecb79bb0c62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 290
        },
        {
            "id": "ce79f268-a5f4-44a7-ab32-fe5afe9afcad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 291
        },
        {
            "id": "7af9016e-6b6e-40c3-ae08-8a4de851d1ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 297
        },
        {
            "id": "b37d5e61-e492-4bac-bf20-1cd2b8b45705",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 299
        },
        {
            "id": "0f1894a5-30d3-485b-884b-68c30b41fe0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 301
        },
        {
            "id": "635a30fd-2483-4690-a978-700ea1315821",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 308
        },
        {
            "id": "402d312d-004a-40c0-bd19-e149077c6587",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 324
        },
        {
            "id": "0dd337a6-5794-4ca4-ad60-ff9e152b20d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 328
        },
        {
            "id": "9a238d32-07bf-4a70-b9f4-aac3af5f8a95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 331
        },
        {
            "id": "2d0ec5f3-f1dc-4803-8a5c-49ee489cc6d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 332
        },
        {
            "id": "e03fc6a1-4da7-497d-be83-c8d648d35351",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 333
        },
        {
            "id": "e122fb35-1332-4bcf-a56d-fa11ee495239",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 334
        },
        {
            "id": "ce2b644d-9a9a-4420-b92b-ace591bf83a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 335
        },
        {
            "id": "1d2d5613-a057-4831-8752-ec0e5563c795",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 336
        },
        {
            "id": "227b0a11-b5cf-4712-a1cf-a7be3831f21e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 337
        },
        {
            "id": "79eea688-be28-4784-bd48-e2b9bf26c5bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 338
        },
        {
            "id": "a877509d-776c-404a-ba6f-8c0408c22328",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 339
        },
        {
            "id": "9755cd68-cbc4-42c6-9e11-9ab82372a210",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 341
        },
        {
            "id": "82bbd480-028b-4d78-ad6d-5192a9278247",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 343
        },
        {
            "id": "dab809d9-c4bf-499d-8271-44275803f1f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 345
        },
        {
            "id": "b0bf7939-fbad-482e-94db-6e9c1ca21399",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 347
        },
        {
            "id": "86ca24af-144a-402b-8d51-b0b5f4949809",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 349
        },
        {
            "id": "098af4c7-d297-4cf7-88b8-96035561d845",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 351
        },
        {
            "id": "3029e348-ed14-42c3-8490-35632e0d1dc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 353
        },
        {
            "id": "95623130-a3e3-4d73-90f5-e31f43601a84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 361
        },
        {
            "id": "559b99a2-d40d-4cf9-939d-161aed7c8881",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 363
        },
        {
            "id": "5ab5076f-312d-4a97-b769-20306e0ea6da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 365
        },
        {
            "id": "80cfb740-1829-4900-9549-0a4b723305c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 367
        },
        {
            "id": "0ee78542-f629-4b0a-83fa-38aa738f24c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 369
        },
        {
            "id": "a933c8ab-e0fe-4ba8-8486-e3e725a92a92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 371
        },
        {
            "id": "df088bbb-f7a9-47cf-be8d-53997c9c0c42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 506
        },
        {
            "id": "e3f20e16-0ed5-4d54-825d-9808698993bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 507
        },
        {
            "id": "2afc2964-0854-4f4b-9796-780befe806cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 508
        },
        {
            "id": "7432bf87-8571-4069-8e47-23942a209c44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 509
        },
        {
            "id": "a3c2411f-5549-4401-9c3b-460b11e425a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 510
        },
        {
            "id": "e7077e46-31ed-4fa5-854b-eb04679693d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 511
        },
        {
            "id": "16bc859f-0346-494a-ab42-cb8c6d22e032",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 537
        },
        {
            "id": "51990ec0-ac7f-43f2-9716-0121b3655839",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 8218
        },
        {
            "id": "8fcdc854-c7a6-41b3-b9f5-0d0872172a24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 8222
        },
        {
            "id": "e344be27-108e-4caa-bf6d-f1bd9e767c5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 8230
        },
        {
            "id": "544112ed-1767-4169-a99b-64574511bbb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8249
        },
        {
            "id": "7295cd56-c5e3-43fa-b3a1-b5b2610f4910",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 74
        },
        {
            "id": "78472caf-c914-4b44-beac-ecc62094a308",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 84
        },
        {
            "id": "49caccec-979d-4ffd-80d3-a9549f2090fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 121
        },
        {
            "id": "bd47844d-4aeb-4d1c-ab5b-337ff47611a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 187
        },
        {
            "id": "f93142e6-2332-4a68-bc8e-6d7fa4f43de2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 239
        },
        {
            "id": "47360635-76b3-45ac-8075-39416cf363b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 253
        },
        {
            "id": "b28dd12f-3e6d-4411-b188-45ef5612d729",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 255
        },
        {
            "id": "4cb7a6de-59df-4c0e-8005-73456273115b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 297
        },
        {
            "id": "eb40f7c9-0c91-4bf9-91b3-1488119295a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 299
        },
        {
            "id": "20b4aa22-22ff-4348-b4dd-60084d0f17e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 301
        },
        {
            "id": "0afb2572-ec5c-4346-a613-e317458b14b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 308
        },
        {
            "id": "abe5f467-6dae-47ec-934c-61763af90ce5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 375
        },
        {
            "id": "87c025b1-f845-4acc-ac51-d1749a7a08dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 7923
        },
        {
            "id": "5dc4fdb9-845f-4753-b97b-a3faa10064c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 8250
        },
        {
            "id": "a60e71c2-51d2-4ade-a8b9-8dbbb7d1c29d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 91,
            "second": 106
        },
        {
            "id": "9185c1ac-ad95-40b4-899b-89973d6855cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 91,
            "second": 309
        },
        {
            "id": "2e7be85c-805a-47a7-a9b5-c0eea7c1edf8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 120
        },
        {
            "id": "cf3cb8f1-1c11-489e-9123-28726adc9822",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 74
        },
        {
            "id": "63c6820f-1a85-4632-ae7c-0bb483ccc58b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 84
        },
        {
            "id": "24edd2e2-55ec-4aa5-90a4-eba64470acbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 89
        },
        {
            "id": "c4ec63d4-869f-48a7-a7d8-6103a82874d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 221
        },
        {
            "id": "39d27b6f-36d6-43e0-881f-85cdd0dea5f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 308
        },
        {
            "id": "5fd17c19-797a-4f75-903e-98088252b07f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 354
        },
        {
            "id": "233529de-75bb-4db8-b97a-6267d26c454d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 356
        },
        {
            "id": "f9d6278b-6d8e-4fef-b8ce-d93b530d0fe3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 358
        },
        {
            "id": "adda390b-6f53-4f7e-98e0-f98bbf60c0e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 374
        },
        {
            "id": "b021f2b7-4fa6-41f3-ba25-3f4a2c92b722",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 7922
        },
        {
            "id": "eebf1d2d-9b48-4f4a-9b14-290fe2aee52e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 34
        },
        {
            "id": "3188c45c-976d-4413-8362-ef4e8cd81c78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 101,
            "second": 39
        },
        {
            "id": "5c817df5-efff-42b2-9c34-a024c124cc8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 41
        },
        {
            "id": "b40d9320-9f74-4934-96b7-0ccd8271c234",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 42
        },
        {
            "id": "931e8711-8670-4ebc-a201-a383b8838537",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 44
        },
        {
            "id": "d505121f-6f17-463a-a0bb-969dcdcd5281",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 45
        },
        {
            "id": "1551a3e8-c877-42f0-a4f6-ae3434c781e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 46
        },
        {
            "id": "baf81de8-8428-40f8-91ad-5aeb6b7f482c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 58
        },
        {
            "id": "4a3f71fe-f6b4-46c0-95c4-902dd4238817",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 59
        },
        {
            "id": "18677794-c60c-4bca-86f9-6cee667e815a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 63
        },
        {
            "id": "5a761b0f-eaa8-4c49-acd3-9098b171f862",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 93
        },
        {
            "id": "4e609c7f-ec6c-42a2-b77a-5a3bd22b76b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 116
        },
        {
            "id": "0aa68fdf-7dae-42ce-8f35-41af5e2375fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 118
        },
        {
            "id": "014c7e5a-4f26-48bb-9fe0-b7395399465a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 119
        },
        {
            "id": "1fe3e593-614a-4914-a0c0-597b58794dee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 121
        },
        {
            "id": "2a464a5a-8258-41ca-b44b-cfe9c2ca1ceb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 125
        },
        {
            "id": "be45a253-9b28-4dc0-b854-94836ef20bc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 187
        },
        {
            "id": "a525e635-4c58-4e3b-84df-40f335d6e243",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 236
        },
        {
            "id": "08422321-7163-4eb4-b71e-5980affea544",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 238
        },
        {
            "id": "a01ec0f5-c39f-4ccd-8860-f452db1b1523",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 102,
            "second": 239
        },
        {
            "id": "b04e7a83-df1e-4241-a397-b80ecbd9faf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 102,
            "second": 297
        },
        {
            "id": "8401d87b-b5f0-4090-b7a0-c0ba30c12663",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 102,
            "second": 299
        },
        {
            "id": "e910a6ff-80b1-41b3-bfa0-31bf8edc7558",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 102,
            "second": 301
        },
        {
            "id": "3b2fa012-c709-4402-83c4-cd9faf564c27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 8208
        },
        {
            "id": "c89961fd-3332-48b9-be43-ed5360d6370b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8216
        },
        {
            "id": "4b11131f-c071-443e-b80d-c72304a0c689",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "4a3f1f9e-bfed-4c3f-9240-2bddc7156a75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 8218
        },
        {
            "id": "2426dc3f-e92d-41c9-8fdc-41b235cdc203",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8220
        },
        {
            "id": "21697e62-54a9-4b47-8e5a-93f42f34ada9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8221
        },
        {
            "id": "0de996d5-d433-48d1-8245-a9138f6085b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 8222
        },
        {
            "id": "3f684341-b3fd-4bbd-bda5-f87c2a3f59ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 8230
        },
        {
            "id": "d2b07b42-a896-44e5-b902-640dacda44c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8250
        },
        {
            "id": "400065d3-61af-4ba5-a69c-1b37f1318428",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 102,
            "second": 8482
        },
        {
            "id": "4ed4ea16-256c-490d-b00f-711c386ab5e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 64831
        },
        {
            "id": "4bce3584-74f7-49b3-9fc5-66a67c5dcb70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 309
        },
        {
            "id": "1031682f-c524-4076-899a-b881e565abb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 44
        },
        {
            "id": "a746be72-6e1b-42d4-9c96-748edd418365",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 45
        },
        {
            "id": "29c9ecf8-048e-4931-be2f-47cbfe83285b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 46
        },
        {
            "id": "a482f135-8ca4-46e2-a593-237d859df756",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 58
        },
        {
            "id": "aa4a4c26-9efc-4613-9f80-5a32a1edf4c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 59
        },
        {
            "id": "9266801f-6c3f-4290-8c62-b9ea054f9840",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 263
        },
        {
            "id": "9d9eb487-7185-45a6-b090-815d04438163",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 265
        },
        {
            "id": "01f959f3-4e67-4e27-9bc1-e87605f33d9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 269
        },
        {
            "id": "3e894b7e-775f-4875-aaa2-0b6b37380364",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 275
        },
        {
            "id": "a02944d4-2e58-401c-a52f-7dcb814de75b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 277
        },
        {
            "id": "c4faa513-df19-49ba-9ea7-ebeb95171495",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 279
        },
        {
            "id": "c9a55dfd-3a71-4918-8951-f422c739a761",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 281
        },
        {
            "id": "b1d6f3e3-6d68-429a-b7a0-40bb86816c87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 283
        },
        {
            "id": "3135ff4f-8689-4a77-ba72-6f798b7d3e60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 285
        },
        {
            "id": "3c2e74cc-bccc-4f7e-979e-e6e25b1861b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 287
        },
        {
            "id": "01037439-ca3b-487a-b695-35db8839015f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 289
        },
        {
            "id": "f55c5c36-708d-401b-897f-704183ec9cfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 291
        },
        {
            "id": "49b0fc8d-eb83-474a-a1d9-240967dea79e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 333
        },
        {
            "id": "94baab2c-88ef-4adb-929b-f7c92c079497",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 335
        },
        {
            "id": "b4d873d1-b8d4-4051-adf9-01615d83bc81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 337
        },
        {
            "id": "4497ab23-9770-43ff-a391-8d4e01fbfc12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 511
        },
        {
            "id": "e3695efa-2e5d-4a48-a467-98d3ade612a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 8208
        },
        {
            "id": "77a1477f-ef98-4447-8190-656539aa1d57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 8230
        },
        {
            "id": "34d22dc8-d6cf-4bfe-ba75-1b91db6c39f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 34
        },
        {
            "id": "65b62a7c-41e0-4d47-ba2d-439a69ed8827",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 110,
            "second": 39
        },
        {
            "id": "401284d6-77d2-4ea2-8846-f835bf1ab0b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 34
        },
        {
            "id": "7a45e44d-72b6-4261-bde9-9fb600ce9cb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 111,
            "second": 39
        },
        {
            "id": "af510c5d-a6e0-4861-9737-ff9a34862947",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 120
        },
        {
            "id": "95678810-078e-4044-ba95-0cedf35e0982",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 8216
        },
        {
            "id": "99d775b1-2b73-4e46-892d-0e588dd9e711",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 111,
            "second": 8217
        },
        {
            "id": "acd85c69-6b0b-4021-b0e0-50b6c0882b0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 8220
        },
        {
            "id": "a779df14-d836-4969-9417-1bc984783e81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 111,
            "second": 8221
        },
        {
            "id": "566f42a6-1936-4ac5-94dd-2b59fee18340",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 120
        },
        {
            "id": "a5f9edf0-5ff9-4e65-ac93-075c74f65743",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 112,
            "second": 8216
        },
        {
            "id": "c390489f-ff5b-4e31-947c-f57685ef6aff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 112,
            "second": 8217
        },
        {
            "id": "b6ad0c6d-b8bb-42ca-8d6a-88368b88ee53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 8220
        },
        {
            "id": "598b6615-9ce3-4f48-866c-0cfa63e43382",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 112,
            "second": 8221
        },
        {
            "id": "1f593744-f183-4c1d-9ae2-f73555bb6af4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 106
        },
        {
            "id": "9f6c5cd2-cab6-4546-ac80-3c17ede37405",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 309
        },
        {
            "id": "ba8c4c16-2f64-4c72-95d5-dfc344868786",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 44
        },
        {
            "id": "6c7357b5-d5df-49fc-ab19-f5e046079b9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 45
        },
        {
            "id": "2e75470d-df57-49b2-be14-62725c92d8ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 46
        },
        {
            "id": "d180c844-bc81-42bf-91d8-7a1570925d7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 58
        },
        {
            "id": "58d0f5c0-b84a-4f22-acc6-6bea1cb9be82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 59
        },
        {
            "id": "7c8f322a-2c2a-4cde-882e-b4dc543a3548",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 102
        },
        {
            "id": "e3d341a5-2474-4ba4-9b96-961fcfa0a5e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 116
        },
        {
            "id": "b9d067a1-bc5e-4315-8ff6-dba8efd45e4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 118
        },
        {
            "id": "27e8e801-6f78-4e40-b14b-00c20bba2fef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 119
        },
        {
            "id": "be163f16-b7eb-41f5-9334-e13314f89df4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 120
        },
        {
            "id": "a37e0311-e464-40c5-ba7b-60906edbb227",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 121
        },
        {
            "id": "00412a13-29e1-465e-97ab-8cb4dd63f1f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 122
        },
        {
            "id": "4611676a-bbed-4a04-9dc6-d6dd8da2514e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 253
        },
        {
            "id": "75f8f3b1-5415-461c-a584-641b0f479f4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 355
        },
        {
            "id": "3c20e946-5580-4dd7-9836-74fc40480ed7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 357
        },
        {
            "id": "e70a4f7a-5869-456f-8cc4-20ca249aa7b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 359
        },
        {
            "id": "8a75c4c2-2c0f-4fc4-af0b-5a656f8c9796",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 373
        },
        {
            "id": "af547a68-5866-4c8e-8708-03c7b90e42d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 375
        },
        {
            "id": "d5c90379-d6c8-435a-ba87-0201c52c4e68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 7809
        },
        {
            "id": "4ac1c8a9-51f3-450d-b7f3-0431799a9f4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 7811
        },
        {
            "id": "b1e1b195-9423-456d-9f44-c5c38f510233",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 7813
        },
        {
            "id": "0b1d775e-71bb-4b3a-ac10-701176273ff3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 7923
        },
        {
            "id": "971e9398-33c2-4fb9-b750-b027aa985b37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 8208
        },
        {
            "id": "47731bd0-9fd5-4f8e-bac7-18bdd383b5c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 114,
            "second": 8216
        },
        {
            "id": "cbf8d832-24d2-4720-85e7-e297d1127261",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 8217
        },
        {
            "id": "62722dda-4b17-4db9-bbba-8b86a19831f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 8218
        },
        {
            "id": "08731a21-41df-4db3-a4ba-08d9576f07ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 114,
            "second": 8220
        },
        {
            "id": "4ba54ca1-65d0-48bf-8043-390808f6c883",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 8221
        },
        {
            "id": "f2db12da-220c-420b-a1b8-8ae44e98ddb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 8222
        },
        {
            "id": "273fd7ee-1638-44b7-ae98-a72af97e9c9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 8230
        },
        {
            "id": "1b8405cc-1aa0-48e9-81c7-66f147ab6163",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 45
        },
        {
            "id": "a0e73dfc-db9b-44cc-ad8c-00be69802c9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 63
        },
        {
            "id": "01e636bb-d3b2-4de6-b774-319d00898604",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 187
        },
        {
            "id": "f14b2c68-76b3-427e-b389-16337631b8b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 8208
        },
        {
            "id": "3649a311-26e8-44b9-8c12-b6fc96204c2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 8250
        },
        {
            "id": "53c6a822-66e5-41a3-87f1-738548b73a6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 34
        },
        {
            "id": "532e825c-f93f-48a3-adc0-e309becaab44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 39
        },
        {
            "id": "cbdfa5cb-9956-4594-9f2e-2837492dd6b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 44
        },
        {
            "id": "096c56f3-d78d-4547-b3bf-9e6551719486",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 46
        },
        {
            "id": "6f169dfd-1701-4748-b086-19664fc94b7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 8218
        },
        {
            "id": "8e8f4582-cdf8-4036-afaa-7082d1c995a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 8222
        },
        {
            "id": "ab9d5a77-61af-4157-8478-ca051c58aa71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 8230
        },
        {
            "id": "1dfaa146-178d-45b0-823d-02ad1317bac0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "af70eaf2-db05-4cde-bcbb-155eba99f483",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "534e9922-1693-44f3-b2db-df30e582e164",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 8218
        },
        {
            "id": "08539ac7-c2ff-479e-afe7-123988c77a8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 8222
        },
        {
            "id": "d6888615-2a32-41f0-9e46-096688f4deca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 8230
        },
        {
            "id": "7293b08e-66ea-4d6b-b486-817e178daa0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 99
        },
        {
            "id": "68b84e95-b667-4c65-805c-41940c2bf1c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 100
        },
        {
            "id": "d4098c6a-df85-47ce-9691-05acaa158500",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 101
        },
        {
            "id": "1403a2dc-cf12-4eeb-86c0-d86aecbdfc69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 103
        },
        {
            "id": "8a83e9bf-31b5-4241-8f66-6ebd6936b7c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 111
        },
        {
            "id": "8d1e3d02-480f-48fa-be7c-6dcd7acfbd33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 113
        },
        {
            "id": "61b67dd6-9bee-4c49-b0ab-0e973beb6e05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 232
        },
        {
            "id": "c6b51211-ad24-4a4b-b3fe-5e74c5edc492",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 233
        },
        {
            "id": "1ca8e5a1-ab13-4a78-a798-2a3d764bfed2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 234
        },
        {
            "id": "047ac531-8def-4e60-8f3c-1bb195095bae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 235
        },
        {
            "id": "099998d5-bb20-4d5d-afda-d8415d0f823e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 242
        },
        {
            "id": "70660545-78e8-4a95-b3b4-328ba663b11a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 243
        },
        {
            "id": "f192c8b2-f5ea-4db8-ab92-a3eb7054a523",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 244
        },
        {
            "id": "051defe8-1807-409b-8424-270e36f2a3cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 245
        },
        {
            "id": "d41cba9d-cadc-458c-a2ab-53164fc4587f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 246
        },
        {
            "id": "dd025891-a6bc-436b-b3df-5ce09c9896c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 339
        },
        {
            "id": "66a82abe-f253-4bcc-bc9b-071bb6b79a85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 39
        },
        {
            "id": "2c7c73e9-520c-43e1-a80a-bb263f839753",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 44
        },
        {
            "id": "7ae8ec89-f190-42a6-ad58-34fbb9dc4f20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 46
        },
        {
            "id": "7b30a71e-57bc-426b-b1df-db457df6d517",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 8218
        },
        {
            "id": "39f3bfd3-487d-4216-a0dd-c52e58139f89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 8222
        },
        {
            "id": "9226fb28-c152-42d1-9969-3d3bc400b510",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 8230
        },
        {
            "id": "16dae657-1c08-4636-91e6-eae92e59cad8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 123,
            "second": 106
        },
        {
            "id": "96422231-1c18-4ea5-bf7d-667b327370a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 123,
            "second": 309
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 24,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}