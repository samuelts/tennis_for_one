{
    "id": "0174a428-4e3f-44b5-95eb-27a0fda2d375",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fScore",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Segoe UI",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "bd71730d-11ee-4c4c-8f50-89fadf8ccd8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 43,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 61,
                "y": 137
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "de03cc68-4ef4-44d2-a136-f4902378ccd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 43,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 129,
                "y": 137
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "45e453c7-8799-4bcd-9345-986a84589ae0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 43,
                "offset": 2,
                "shift": 16,
                "w": 11,
                "x": 437,
                "y": 92
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "1ec39cd2-bd30-4e88-81f4-18ec2091f22f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 43,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 2,
                "y": 47
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "f77b4661-a3e4-4e78-aa25-dc2da6d7110f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 43,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 164,
                "y": 92
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "5a189cbb-5eda-4ddb-a9b1-d7fcb9f8330a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 43,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 150,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "b2741be1-352e-49f4-b432-9673bde89e1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 43,
                "offset": 1,
                "shift": 27,
                "w": 26,
                "x": 178,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "794e1468-2506-4f74-94bc-239ca0ee841d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 43,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 163,
                "y": 137
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "37c93fef-47d9-43d8-a6d3-865d37daf0ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 43,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 488,
                "y": 92
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "5c9f2792-14ef-4ead-9e73-0c468bd87de3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 43,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 137
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "b58c295c-b5cf-4edc-8edb-4c3d6358a826",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 43,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 394,
                "y": 92
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "b40fd772-9e51-4024-a9c1-bc0477389229",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 43,
                "offset": 3,
                "shift": 23,
                "w": 16,
                "x": 146,
                "y": 92
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "cb718d59-eae3-40be-91da-002c1ad4a7ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 43,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 138,
                "y": 137
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "c60ce8fc-1fcf-40e7-b93a-55c720651c7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 43,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 50,
                "y": 137
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "dabcc1b0-696f-4997-a819-d0802dadc160",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 43,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 111,
                "y": 137
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "aa3e284f-3112-4136-9592-1b6e2d5d1bec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 43,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 56,
                "y": 92
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "933e77f9-2dca-42f9-9d3f-f90f11aef520",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 43,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 453,
                "y": 47
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "c016cabd-adf4-4437-88d4-765b18fb2364",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 43,
                "offset": 3,
                "shift": 18,
                "w": 10,
                "x": 38,
                "y": 137
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "f3d88919-240f-4f27-93d4-ac1197a10d5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 43,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 2,
                "y": 92
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "903cc55e-ca1d-42db-ba92-7e2c2f4358ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 43,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 20,
                "y": 92
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "89e96ab1-4a9a-445a-9ae9-ad7de43ff4e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 43,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 126,
                "y": 47
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "1839a428-9fbd-4859-a38c-b93a2e5b7f7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 43,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 234,
                "y": 92
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "e60a9ae7-66f9-43f7-ae49-f0be84332a16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 43,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 396,
                "y": 47
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "4d68ada0-0793-43f5-8dfb-e89e7de2214b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 43,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 415,
                "y": 47
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "4fee3c6d-b3db-4c88-a070-29e01e8006e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 43,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 434,
                "y": 47
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "f22255f7-54e8-4d91-9a8e-9b7764145ac2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 43,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 38,
                "y": 92
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "572082ac-9f6f-4550-8e6b-3a5807b969f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 43,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 120,
                "y": 137
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "17727c73-8835-4fca-8d4a-7a58e00169df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 43,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 72,
                "y": 137
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "672cd55e-6ef2-48dd-887f-2d91ce9e03f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 43,
                "offset": 4,
                "shift": 23,
                "w": 15,
                "x": 200,
                "y": 92
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "5ccba17e-816a-497c-a0e5-5e0eaa74baba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 43,
                "offset": 3,
                "shift": 23,
                "w": 16,
                "x": 110,
                "y": 92
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "945f81eb-e726-4a13-b9cb-b9e76588ea92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 43,
                "offset": 4,
                "shift": 23,
                "w": 15,
                "x": 217,
                "y": 92
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "9abe4a4f-14bd-432e-a631-de6d368d80aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 43,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 316,
                "y": 92
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "06a62b7d-37f2-41ac-a8d1-40b15d8ab910",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 43,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "5fb74570-4de3-4f78-889a-69ff50f07174",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 43,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 258,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "a4e7e697-0b21-4325-a73d-f0139f815710",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 43,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 166,
                "y": 47
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "9be2fc9c-7abc-45c4-a79b-9fd798b52184",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 43,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 186,
                "y": 47
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "254d90f8-d515-4603-bcd3-3e0c61ef7528",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 43,
                "offset": 2,
                "shift": 24,
                "w": 21,
                "x": 330,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "3aadf4a7-e4c6-40e8-8b82-626a29322665",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 43,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 284,
                "y": 92
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "35c722a1-4d3a-4149-b8c8-783a993f40bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 43,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 268,
                "y": 92
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "fcca48b7-d96d-464c-b4c3-e6fa23868e29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 43,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 464,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "dacee09e-7df9-439f-8f84-6439fc822bca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 43,
                "offset": 2,
                "shift": 25,
                "w": 20,
                "x": 420,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "7d80581e-db1e-44b9-a999-455ae18f23b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 43,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 155,
                "y": 137
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "4b810806-f673-49a1-b59d-d4107aa73157",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 43,
                "offset": 0,
                "shift": 14,
                "w": 12,
                "x": 409,
                "y": 92
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "13d589af-8314-432d-aae9-f58f66d9869f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 43,
                "offset": 2,
                "shift": 21,
                "w": 20,
                "x": 376,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "83814483-4f2d-4f8d-b8a6-87a7cae463fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 43,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 348,
                "y": 92
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "f1899271-2275-4a8c-9d3d-82d511ec08d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 43,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "c19e7587-b3b5-4264-a095-2ea75517ee86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 43,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 307,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "ffbca677-bd97-4ad0-b0d6-6e110d07bd37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 43,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 233,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "fce66c37-419c-47de-ab41-7db2a06a6b91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 43,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 358,
                "y": 47
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "f16e316f-a3c9-4fdc-89f3-046377ca4a17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 43,
                "offset": 1,
                "shift": 24,
                "w": 25,
                "x": 206,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "9a05ce3f-8d37-4e48-a78c-c5f7585f39d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 43,
                "offset": 2,
                "shift": 21,
                "w": 20,
                "x": 398,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "ab87518a-e4e0-4dad-8c5c-510bdfa38c3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 43,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 472,
                "y": 47
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "221f98f0-edd1-455d-b467-3679246a312c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 43,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 23,
                "y": 47
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "6d38112a-42c4-4549-b047-55050a2acc63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 43,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 44,
                "y": 47
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "215dd2c2-7577-470c-b852-92c7ffd698a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 43,
                "offset": 0,
                "shift": 21,
                "w": 22,
                "x": 283,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "2adeed75-d230-4754-8ea5-00e5e3b5de64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 43,
                "offset": 0,
                "shift": 32,
                "w": 32,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "141663ef-7d43-43bc-8154-a5d24e6ab722",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 43,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 353,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "51cb196a-6492-4a28-8a61-e49fbcd27b8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 43,
                "offset": 0,
                "shift": 19,
                "w": 20,
                "x": 442,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "7b80b428-d944-443b-8621-d36497080096",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 43,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 65,
                "y": 47
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "c3e4b6a3-4318-4568-9401-f0aadbff7850",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 43,
                "offset": 3,
                "shift": 12,
                "w": 8,
                "x": 82,
                "y": 137
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "aafe7cef-140b-4994-b3f7-7a69b9f4611e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 43,
                "offset": 0,
                "shift": 14,
                "w": 16,
                "x": 490,
                "y": 47
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "5d598c85-abe6-4fb0-b87a-7d28e7e5d20e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 43,
                "offset": 1,
                "shift": 12,
                "w": 8,
                "x": 92,
                "y": 137
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "0272d28b-d606-4477-aec3-ea87f2a24b40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 43,
                "offset": 3,
                "shift": 23,
                "w": 17,
                "x": 263,
                "y": 47
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "2fce4e59-6788-48a8-95ed-1b95b001db3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 43,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 332,
                "y": 92
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "2d6eb964-08ba-42ef-96a6-54d298d89417",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 43,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 14,
                "y": 137
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "cd02c1a9-595a-4ad3-bc34-dab97af403a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 43,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 74,
                "y": 92
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "fbdf6a39-cab0-426c-8eb8-7fdea1b1c903",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 43,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 320,
                "y": 47
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "4701a240-2310-449f-b2e2-1c9bb4061f23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 43,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 300,
                "y": 92
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "44e8aa85-4436-486c-8040-29bbeb61627b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 43,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 225,
                "y": 47
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "9d8f0822-428e-4c99-a6bc-bdc21e62d56e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 43,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 182,
                "y": 92
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "2a72fc7d-f4df-4fab-b372-e372ce999a09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 43,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 364,
                "y": 92
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "7b94d501-ca86-43a4-b8e2-badfdbba6bdb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 43,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 206,
                "y": 47
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "851587d4-9986-417d-a0bc-c808fddae0f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 43,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 92,
                "y": 92
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "22b7d9d8-e48b-4fac-8a92-6d87025ee112",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 43,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 102,
                "y": 137
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "86a8e473-b188-4e64-836a-4f63677c2d5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 43,
                "offset": -3,
                "shift": 9,
                "w": 11,
                "x": 463,
                "y": 92
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "8c1b8613-2c60-4c3e-8992-7c1e0b8dd475",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 43,
                "offset": 2,
                "shift": 18,
                "w": 17,
                "x": 339,
                "y": 47
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "227d7dbb-df8d-4864-a9ee-192afc000210",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 43,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 147,
                "y": 137
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "6a38da8f-9c2e-4429-bc10-ebe61685a77b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 43,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "2394d304-2d8b-4cd8-b0e9-f79e119ffd43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 43,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 128,
                "y": 92
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "1642d720-4bf0-4406-8ccb-5c18ad3305e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 43,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 86,
                "y": 47
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "559b2e5c-494b-4f59-a482-a9eae3e1f92a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 43,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 244,
                "y": 47
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "8900c093-ccd8-4092-863c-394fcd2157f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 43,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 282,
                "y": 47
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "0cd4cd64-56ad-4aa5-b010-ae0fbf8df893",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 43,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 450,
                "y": 92
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "3dc32d09-e929-458d-8a12-611d2076852c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 43,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 379,
                "y": 92
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "19d2c619-b9bc-48e5-aecb-82e37c34642d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 43,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 423,
                "y": 92
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "145975b9-7edd-40de-9de7-2390c880826d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 43,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 301,
                "y": 47
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "1dead82f-48a2-4ff8-8a4d-b30aa2b84e25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 43,
                "offset": 0,
                "shift": 17,
                "w": 18,
                "x": 106,
                "y": 47
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "40c17f82-b38c-420c-ba57-4234c4748e61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 43,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 122,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "c07e172f-5271-4b5a-a690-8ea2befbd9a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 43,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 146,
                "y": 47
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "d96c72de-380c-4068-aca1-838b451cfa66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 43,
                "offset": -1,
                "shift": 17,
                "w": 19,
                "x": 486,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "50a6a589-38ad-45ad-9454-bcd58d8e4912",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 43,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 251,
                "y": 92
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "c92a5505-5182-4671-9561-f9fd7700ab0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 43,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 476,
                "y": 92
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "978c5a16-80ed-4f29-b776-ec66775de90c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 43,
                "offset": 3,
                "shift": 10,
                "w": 5,
                "x": 170,
                "y": 137
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "10a5f95b-378e-4d0d-abba-6278b9054c66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 43,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 26,
                "y": 137
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "a0d0110c-e41b-41f2-880c-5a5c3421d9ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 43,
                "offset": 3,
                "shift": 23,
                "w": 17,
                "x": 377,
                "y": 47
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "1a333fc5-8ba8-43e8-a2a1-a67148d6de6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 114
        },
        {
            "id": "ad0ba93a-22cc-446a-ad0a-50e7976c0cb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 115
        },
        {
            "id": "072a4fe9-cfcb-4002-a329-4eeda4d870a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 341
        },
        {
            "id": "60ef133a-c49c-4630-af88-67e665f95222",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 343
        },
        {
            "id": "1711a707-9232-4432-ba5a-6244473b029a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 345
        },
        {
            "id": "a41c3de6-d1dc-4954-8f2d-78c650cfd95c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 347
        },
        {
            "id": "f876bb29-3d0a-456e-873e-42db87d1b3b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 349
        },
        {
            "id": "3f4674d5-dec5-4503-a07a-3ed0c8227812",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 351
        },
        {
            "id": "48c4ae74-c72d-4873-bec9-339a83ad2b88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 353
        },
        {
            "id": "4bc06a98-59f8-41cb-a12b-843d6accae00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 537
        },
        {
            "id": "c7f1d9dc-a471-4c10-b14c-93c7bae019dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 969
        },
        {
            "id": "ae0ae57b-de38-4cbf-95f0-7a2fb6b3585b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1109
        },
        {
            "id": "bddbf26b-9f2a-4116-8891-e003014ee654",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 114
        },
        {
            "id": "3f55a2a3-7da7-4c36-9d38-63a94a125eb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 115
        },
        {
            "id": "34988cad-93ad-45a9-8457-63cdf8fa3fa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 341
        },
        {
            "id": "370affa3-b31e-4ffa-8e17-306d2e6ffa47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 343
        },
        {
            "id": "beb0d86d-e5f1-4f87-a1e8-2ccc9ab72f26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 345
        },
        {
            "id": "6c9dcb86-6b52-466e-b4b9-c48994140dd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 347
        },
        {
            "id": "858d65af-29ea-4ed2-a1b7-4a656e09a331",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 349
        },
        {
            "id": "a298a3c2-fec0-4769-b9e6-ed00a167c20b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 351
        },
        {
            "id": "64a43bf0-14e5-4583-aca3-b04d7643b4ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 353
        },
        {
            "id": "9662ec22-7c06-42c8-b209-9c84893c9ea8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 537
        },
        {
            "id": "81951e9a-3766-4d38-90eb-829054b52e1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 969
        },
        {
            "id": "332d00c5-7474-4b41-8501-9b1d3ec8af6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1109
        },
        {
            "id": "5c40fded-9f8c-435c-9fda-821adb764401",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 40,
            "second": 106
        },
        {
            "id": "29eeb8a7-1533-481b-810b-845fc9fae8bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 40,
            "second": 309
        },
        {
            "id": "e07cb9fb-b3a5-4d1c-b695-4227b07e00ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 65
        },
        {
            "id": "c35ce671-35db-4db0-9238-efb5623a1d3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 74
        },
        {
            "id": "3da4b27b-c670-4705-afcc-587b53ee2040",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 99
        },
        {
            "id": "b7ff4da1-8e9d-4916-a325-0662eecbc8ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 100
        },
        {
            "id": "1df4d159-55a2-4b5f-a02c-5543daf62faf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 101
        },
        {
            "id": "27c6cbb4-485f-4893-909b-b90ef2243e57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 103
        },
        {
            "id": "7bcc9967-148a-4e01-97a9-8241d15bbb54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 111
        },
        {
            "id": "387b6551-fa4a-49ef-8f60-a1bb2fe5fc96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 113
        },
        {
            "id": "a4ccfc53-b342-4a03-b868-89b1121d7460",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 256
        },
        {
            "id": "87459cb8-f5e7-4e3f-a842-fd8f669d7dca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 258
        },
        {
            "id": "45a72a02-e982-48fe-bb19-37beafb1622b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 260
        },
        {
            "id": "abd88ce4-cfb7-4496-9753-488dfa7d6b32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 263
        },
        {
            "id": "84888024-5197-4c24-b344-ec5a13eb8bcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 265
        },
        {
            "id": "41216eb0-900c-4f7e-89c0-7644f690a8e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 269
        },
        {
            "id": "4c77a078-c49f-4fe4-8388-c488ecbe48fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 271
        },
        {
            "id": "52be5f35-cb09-4426-acb9-a347cf633306",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 273
        },
        {
            "id": "18e55cd3-cca8-4b48-8628-be4941c8f6c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 281
        },
        {
            "id": "afe8467f-901b-486f-aee2-37596f724078",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 285
        },
        {
            "id": "a10886e4-4fbb-40c5-bed5-03e59dc86dfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 287
        },
        {
            "id": "ca126243-68eb-4c2e-bbdf-cb2bf26572b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 289
        },
        {
            "id": "ed2680b9-08c1-4382-a212-465d02ffd6ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 291
        },
        {
            "id": "12b18768-7659-42b7-afff-7a1f11c34799",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 308
        },
        {
            "id": "1bdee895-8092-4c28-afcb-b1f09b6f97dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 506
        },
        {
            "id": "a12d7ed9-7c8d-48ff-b5c4-00d8d910c152",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 508
        },
        {
            "id": "dfdc2bc2-27b1-4fa0-a648-ccc1afa7ede6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 913
        },
        {
            "id": "215f1c6d-a523-4092-92fe-eea612b1db22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 916
        },
        {
            "id": "24967b3b-07e8-4ead-9984-4a7fd19dd28a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 923
        },
        {
            "id": "8079db81-3b4e-4582-8be6-271fd1cae145",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 1040
        },
        {
            "id": "b432bd34-9a33-4b8f-93ca-7d193ba3ac47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 1077
        },
        {
            "id": "517722cd-a53c-4aef-b117-c0ca72f385a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 1086
        },
        {
            "id": "710515e9-70b9-4631-96e2-1a93a59e8f65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 1089
        },
        {
            "id": "9604418a-94a2-4848-9ed7-1e3d85e5de01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 1092
        },
        {
            "id": "d53eac96-37c0-4877-8074-efd2d94ef4f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 1105
        },
        {
            "id": "62473369-37cd-4dc7-be14-03611ddb2112",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 1108
        },
        {
            "id": "531ab110-56f3-44e4-ac9a-f1d07c64fe46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 8216
        },
        {
            "id": "4ce29398-9489-48f9-b59d-b27e65731d4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 8217
        },
        {
            "id": "d4a5ca31-7f8a-410d-8567-a9cacffe7431",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 8220
        },
        {
            "id": "62b0d5a0-cda0-4441-8cc7-a56094df2f1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 8221
        },
        {
            "id": "32a106a3-476e-48de-91c5-07e34e42747b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 8216
        },
        {
            "id": "513c8871-74f7-480a-8f49-dc30b3d88df6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 8217
        },
        {
            "id": "d71cb582-29df-41bf-a42c-f627e8ab2fed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 8220
        },
        {
            "id": "7a4b5d28-6084-4f57-bf6a-5d0df03548f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 8221
        },
        {
            "id": "4ca95756-9064-4d99-b1ca-872e90cb87d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 42
        },
        {
            "id": "3d23fea7-3691-42d3-a0a9-ada914ff2d48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 44
        },
        {
            "id": "d295d3c5-db2f-487f-8b90-4c64ddc761e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 59
        },
        {
            "id": "98f73dd2-7f39-4d83-9b16-cc1b2d6bd2f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 74
        },
        {
            "id": "51ecd9b0-666a-4d20-9bda-cb28b79172ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 84
        },
        {
            "id": "85a25fcf-fe6a-4280-9b39-f179f6ebd720",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 85
        },
        {
            "id": "6d7400c6-8efd-4742-bf4f-5dd12739fa4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 86
        },
        {
            "id": "4b552be4-def5-4a01-99b1-279079e82f2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "7e9a6394-89b7-418d-aedb-a0ef587cee82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 89
        },
        {
            "id": "dcc94fdd-0301-4699-9207-4b3eda552c88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 116
        },
        {
            "id": "b45b8a02-f396-4b61-a3a1-980989a550b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "b335ba12-8ae3-4978-a5b9-219f0549f296",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "6d53c785-4fbb-4eb7-b812-b28b6f3fe9f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 221
        },
        {
            "id": "1e353d55-a781-4406-bbc2-bbab3961a004",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 255
        },
        {
            "id": "6db496c9-f550-4be8-8ec1-92b576930c3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 308
        },
        {
            "id": "191f6ba6-498d-42da-8548-5d0b8988e4d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 354
        },
        {
            "id": "ad494c13-b57b-4ccc-b486-224408bfbe63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 356
        },
        {
            "id": "647a8de5-ed87-4ec6-b893-beee3039f1db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 358
        },
        {
            "id": "752a6f8d-2781-469f-a793-0775a3f176e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 372
        },
        {
            "id": "14206053-ade3-42c8-8394-deddaf4d078b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 374
        },
        {
            "id": "e221e888-372f-465e-a6e6-6bfa48b6bf0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 376
        },
        {
            "id": "17155442-da6f-4d41-9884-c50f35839957",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 377
        },
        {
            "id": "7a8ef311-2e9e-43c2-bf0d-f8b77ed35d02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 379
        },
        {
            "id": "0dab2132-a95b-4436-a2a1-1c46ee72a9f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 381
        },
        {
            "id": "8e547a3d-84bd-4cdc-a5ab-16bbc35e9fbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7808
        },
        {
            "id": "a6ebf36c-efd7-4843-954b-e1644739de02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7810
        },
        {
            "id": "128ba942-30c6-447b-844a-31c0779e161b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7812
        },
        {
            "id": "546db16d-eee9-4289-b9c7-749f85a9c4b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7922
        },
        {
            "id": "871d8300-8c4c-4463-9a78-194b7b2b91dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8216
        },
        {
            "id": "0b7e1037-3beb-40ee-8d89-f082a7d49541",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 8217
        },
        {
            "id": "de4469ab-9d9c-472e-843e-7ee919c5c313",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8220
        },
        {
            "id": "77a9457f-c879-47e9-96e2-8312627d296c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 8221
        },
        {
            "id": "94b5855d-02cc-47c3-ab74-82462171ce8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8482
        },
        {
            "id": "04c02a24-5545-4cec-90de-10d582b48ff9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 84
        },
        {
            "id": "a20424eb-e4bb-4dd9-9cf1-3404d527632e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 89
        },
        {
            "id": "511792c5-b23b-4ce3-8d98-1531cdff48bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 221
        },
        {
            "id": "548734a9-60f7-4299-ac75-dc19f044cf15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 354
        },
        {
            "id": "f11ea4ea-5bff-4432-a5c9-17375a017ae5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 356
        },
        {
            "id": "ee89821b-4a3d-4902-9125-4ea33096f6a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 358
        },
        {
            "id": "f573a71b-b0c9-4699-a34a-522da4fec880",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 374
        },
        {
            "id": "a06060e0-964a-48a4-81ae-233fc611c243",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 376
        },
        {
            "id": "8f60ca61-e6e0-4143-81fe-136d718f205f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7922
        },
        {
            "id": "03fe44aa-e3e1-4c3f-9668-2ecfca9729c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 8218
        },
        {
            "id": "e350e264-8733-410a-ac12-c804b7e3600a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 8222
        },
        {
            "id": "4492fe00-e828-4200-87d7-e4fa581d3fa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 67
        },
        {
            "id": "a5447f60-8f67-46a2-8b59-959105d14860",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 71
        },
        {
            "id": "80dc0272-7644-47b2-97dd-abe194c4f847",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 81
        },
        {
            "id": "583acb81-a3f3-45ea-a83c-d63b48f3b740",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 171
        },
        {
            "id": "6b3624bb-e7f3-4780-8113-99d1c00ce1f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 199
        },
        {
            "id": "3d8c0ade-7fee-4e91-bce7-3a9efeee8c31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 210
        },
        {
            "id": "80ee81c6-5687-4a41-83c2-fd6ad02caa04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 211
        },
        {
            "id": "e63ff1bf-47da-46ae-8721-dd1b2b5a1cd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 212
        },
        {
            "id": "f6e0fe73-7d53-4428-b597-84bedb3838d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 213
        },
        {
            "id": "1c144e06-40ea-4161-8c45-cbe9c135b3c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 214
        },
        {
            "id": "bd242cea-0ef9-428d-a732-203d043dcb57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 216
        },
        {
            "id": "2b1d986e-c50b-493e-ac31-c9d8598732e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 239
        },
        {
            "id": "0674116a-1883-4eee-bd25-0441e2ac6676",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 262
        },
        {
            "id": "281b26bd-c4e8-404d-9a4a-30b79672527f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 264
        },
        {
            "id": "f36786a8-6d2b-43bb-b89b-ea9a9d9437ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 268
        },
        {
            "id": "bfb30670-9d61-4f93-a60d-6c7a374ea52f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 284
        },
        {
            "id": "d5e553cb-af77-423f-bf45-c004d5e38e23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 286
        },
        {
            "id": "0df2631a-51ba-45c1-945e-1fb2008aa92f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 288
        },
        {
            "id": "97cc5a95-536d-4ff0-b113-a9be86f150a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 290
        },
        {
            "id": "defd2efd-7c44-4176-9619-1689b78a4f8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 338
        },
        {
            "id": "4f10fad7-2b1a-4d33-ba44-2e6022082381",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 8249
        },
        {
            "id": "4a76bc04-caeb-469b-b718-5ec0c2fe4626",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 44
        },
        {
            "id": "6f3f4fba-5da9-46f4-aa0a-d9701f4a14cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 46
        },
        {
            "id": "01e0c9d1-1efe-4174-85e7-e4716c61a226",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 84
        },
        {
            "id": "295e7e23-3868-4d51-85af-cc68047ad9b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 88
        },
        {
            "id": "db3273f4-e3df-475e-b49d-b80ff2c1334b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 90
        },
        {
            "id": "07b6619f-63a7-4849-b889-b33707c4e14b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 198
        },
        {
            "id": "d8b10aec-9e3f-4122-a7c2-d9a563b2e722",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 354
        },
        {
            "id": "b5b56244-31a4-44fb-9465-0914cd118ade",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 356
        },
        {
            "id": "e68d56fa-4908-45e2-95db-32f8410810a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 358
        },
        {
            "id": "8f38703c-dc24-4182-8eea-b45a251924e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 377
        },
        {
            "id": "8f853e04-25e1-43a9-9eab-6cd8deeadf40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 379
        },
        {
            "id": "00879f1a-0623-4581-b14c-9ceec7fb62cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 381
        },
        {
            "id": "6e4ad735-6df2-4e91-ad30-32738280f3e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 8218
        },
        {
            "id": "28f553e0-162f-4488-a8d6-39ba1d53dd3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 8222
        },
        {
            "id": "5edf5924-4ba6-4c00-aa5d-599b59ad6176",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 8230
        },
        {
            "id": "d35a3734-3b43-4ec9-9dd2-be6429427c14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 69,
            "second": 74
        },
        {
            "id": "99e81059-1654-4ed1-8409-8a097d1d33e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 69,
            "second": 87
        },
        {
            "id": "673edbef-7abf-42a4-9c83-19af43d77379",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 69,
            "second": 88
        },
        {
            "id": "6aa74e9f-a7e8-47ad-8f71-775e99cbcfc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 69,
            "second": 308
        },
        {
            "id": "940c4449-6e37-430f-8009-04b4ceaa681f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 44
        },
        {
            "id": "a5c834be-e10f-499c-af76-8ce441217f27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 46
        },
        {
            "id": "19e4e4f8-68a4-4a8f-adf0-70ac42fc5822",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 65
        },
        {
            "id": "9e027d42-412c-4167-a8ec-8f010157c173",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 74
        },
        {
            "id": "8ebcafeb-85f6-4911-8a70-1cf0c800de26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 97
        },
        {
            "id": "641fc021-b8a0-4754-ab67-1db1cfab2b33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 192
        },
        {
            "id": "e21c3cc4-a617-4cb1-a6dc-7538f31f48e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 193
        },
        {
            "id": "b4858089-351a-487a-950e-d221137823c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 194
        },
        {
            "id": "8e6bab2f-05b2-406a-874e-2b91cf2e5c20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 195
        },
        {
            "id": "dfb16be5-b1c3-47b2-8e43-d1ca8e1e227d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 196
        },
        {
            "id": "5ecd6ae2-2ccf-4430-b950-798c52cc7ba6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 197
        },
        {
            "id": "be7a1e96-feb8-4ad4-99f5-44c2151d4922",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 198
        },
        {
            "id": "93dfca10-f6d6-4f4f-9857-177e881afb93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 239
        },
        {
            "id": "e79d19ce-67be-4771-a9a6-f9b069044ccd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 256
        },
        {
            "id": "4cf829f8-7769-4220-8a0b-23adbb7374b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 258
        },
        {
            "id": "e5e026bb-1106-4e56-a749-872f85598f8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 260
        },
        {
            "id": "ac60c791-3c72-4992-a27e-83087c68be50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 261
        },
        {
            "id": "38ca983a-25a5-4b8a-987b-7970a3a2f74f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 308
        },
        {
            "id": "21676519-707e-445c-85ca-cfd907ae7175",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 506
        },
        {
            "id": "2bb9bff4-c2ec-4c83-8347-9393e049a96a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 508
        },
        {
            "id": "0f1926f3-5e2d-40e7-8f3b-dae08567eeb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 509
        },
        {
            "id": "12c84e99-8bbd-43a9-b088-d66ce3c4b382",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 8218
        },
        {
            "id": "91cec9c5-4fbd-4996-bb79-8a52409eea1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 8222
        },
        {
            "id": "200e352d-da06-48c5-9eaf-f698f4cf0669",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 8230
        },
        {
            "id": "92a95d6f-fcae-4a74-ad52-b86d023c1f5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 84
        },
        {
            "id": "3b12dcc8-d06f-442b-a692-919df115b8b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 354
        },
        {
            "id": "ef692783-63c8-4baa-836e-fb4c9074cfc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 356
        },
        {
            "id": "bfb5d4e6-3e05-4170-8389-55f4bff45b06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 358
        },
        {
            "id": "359ba144-e2de-47ab-85e4-ffcbb6ac6999",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 44
        },
        {
            "id": "34fc21ec-b880-446c-95ac-409e5388d3b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 46
        },
        {
            "id": "26aec8cb-270b-4eda-83b8-012db00c9428",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 65
        },
        {
            "id": "2ca1d20a-71de-4950-a81e-99dbc5bfb351",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 74
        },
        {
            "id": "3252dda6-9856-4f2b-b59e-bfc21dd3a3e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 192
        },
        {
            "id": "470ff9fe-bbb5-446f-b869-d190799e7d4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 193
        },
        {
            "id": "506d60af-e1a9-476c-9b76-5376eac7b0fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 194
        },
        {
            "id": "963a9250-4dc5-4905-9a2d-ee53554f1442",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 195
        },
        {
            "id": "01067ee8-2a5c-40cc-afc9-684db19a0715",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 196
        },
        {
            "id": "992cd1c0-7fcd-4ee6-911a-70b5e0272271",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 197
        },
        {
            "id": "58bcffef-1e8e-4e8e-9f87-4a05b3f0018b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 198
        },
        {
            "id": "08be9adf-0851-4c4a-b1ca-f825f9754c52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 308
        },
        {
            "id": "ada4b46f-c2b9-4ccd-8356-f04e667c7459",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 8218
        },
        {
            "id": "299a881d-dbf8-4498-8f37-08164f1acafe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 8222
        },
        {
            "id": "ba2077e4-74c8-470c-9d5d-556da813cc7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 8230
        },
        {
            "id": "865619c7-7ca5-4dff-876d-a4f00edb1189",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 44
        },
        {
            "id": "343f4d99-47ce-48fe-a9e8-aa3f1750e608",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 59
        },
        {
            "id": "9e3f4c1c-7371-47ef-b1e7-ff235d785a65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 67
        },
        {
            "id": "053d9267-358b-44a1-a2c1-99853a3d2196",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 71
        },
        {
            "id": "0013693f-6a0c-4066-b2b9-ecb839cb4e64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 74
        },
        {
            "id": "3de0d5e3-2e5f-41bd-8121-0a9b0c555731",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 79
        },
        {
            "id": "c93ca76e-ec06-4350-9884-162e65aee4da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 81
        },
        {
            "id": "42790f8c-3ca4-4605-b6a0-bfebd4df38c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 88
        },
        {
            "id": "19a0ab19-fe44-4d19-940d-72a3310e329a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 90
        },
        {
            "id": "320050e1-3798-4c3d-93d8-c6e78730c5be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 116
        },
        {
            "id": "5ca4065b-f2a4-4f42-965b-110e0f777411",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 118
        },
        {
            "id": "03c64fad-7243-4734-821b-ced8258a80bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 119
        },
        {
            "id": "d89f26cb-edd9-41e4-80ac-b7a7f35e59de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 121
        },
        {
            "id": "0fcb3f56-450a-4586-8b5c-6cea478a1ec9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 199
        },
        {
            "id": "90334f5c-8c85-4c78-81cb-01c61772843d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 210
        },
        {
            "id": "11421878-4eaf-45df-9bde-b344276fbea5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 211
        },
        {
            "id": "ee1e3642-92da-489e-91eb-a1198b6592d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 212
        },
        {
            "id": "6230f3fe-b815-4f1e-a265-b8d913ccfe44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 213
        },
        {
            "id": "3595b696-6cdf-4d5a-8757-4ea050b30f89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 214
        },
        {
            "id": "954667f4-addd-402c-bb43-c98f04a9b62b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 216
        },
        {
            "id": "bd1633e1-ed77-4cb8-bda9-baaf3df1183d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 236
        },
        {
            "id": "f409728b-9d91-46fb-938a-59c264d21e01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 237
        },
        {
            "id": "525f71c7-87ec-44b4-b97a-8a0926accdb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 239
        },
        {
            "id": "1e87e46f-81f8-4002-8d05-48034873f761",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 253
        },
        {
            "id": "e6fe3563-b783-4d75-96b8-335856d5ae27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 255
        },
        {
            "id": "44f0f5fe-9b4d-45a4-94d7-400092252568",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 262
        },
        {
            "id": "9a8cc6bc-1306-4775-bed7-38abce2c316e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 264
        },
        {
            "id": "36b62a5b-88cd-4c36-910a-f559a63b8cc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 268
        },
        {
            "id": "0d6c045d-c976-4c55-b713-abf6ad492429",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 284
        },
        {
            "id": "39d2c46b-6a17-4a10-a482-bff2367cdfe9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 286
        },
        {
            "id": "94472de8-5f8e-4314-9bed-a9ded701b6cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 288
        },
        {
            "id": "7094105f-865d-4e82-b590-529377890801",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 290
        },
        {
            "id": "d2548cb8-f9d2-433c-92d1-f6c6aca03efe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 308
        },
        {
            "id": "5396ef30-5127-4a5f-8f74-5b12e2b2d4df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 332
        },
        {
            "id": "134576d0-237a-49e6-b067-fd58c7b99442",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 334
        },
        {
            "id": "386d1f3d-afcd-435e-ad42-b121d487ef38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 336
        },
        {
            "id": "3d7b62f5-f23a-4496-abaa-9df39ec80c1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 338
        },
        {
            "id": "722675b5-4a95-4504-b955-3028155d7e23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 355
        },
        {
            "id": "1a0bf81b-0dec-45b1-8d20-783d548c0e81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 357
        },
        {
            "id": "c1416245-9781-4c4c-b0a8-0128798daeed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 359
        },
        {
            "id": "970c2b8d-ef3c-48be-926b-71aacf59475f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 373
        },
        {
            "id": "2ea31135-478e-42e9-91f9-851dad2c9c86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 375
        },
        {
            "id": "4511a2da-7595-4f43-b56e-9fe41092043f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 510
        },
        {
            "id": "aada5371-3ee7-43c5-a688-2220e2a38313",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7809
        },
        {
            "id": "6c7bb8ed-9b1b-4fc9-9b6a-efb6645fd9e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7811
        },
        {
            "id": "9a7f1c36-9508-467d-b4fd-ae8bf2426aa8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7813
        },
        {
            "id": "d6e8cb9a-7320-4cd2-b3f0-5ed6f4fda9bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7923
        },
        {
            "id": "7af54567-f982-4f85-9379-e3860ec245ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 8218
        },
        {
            "id": "8440e9a6-c343-4d36-b4fe-d2a8ed2cd9b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 8222
        },
        {
            "id": "567e8087-8620-451d-a250-7ee4cc068370",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 42
        },
        {
            "id": "16513db5-3219-4a01-a20b-7f72510273bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 63
        },
        {
            "id": "22bbea71-cdea-4e9f-9cba-c69b8d8b1f8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 65
        },
        {
            "id": "8626cc94-76ea-40e3-a1be-df0fb0f0d213",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 67
        },
        {
            "id": "15fc38de-2627-45ae-885e-160511c507ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 71
        },
        {
            "id": "cb2dfe35-da50-4300-bf4a-ffdb269ea0f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 74
        },
        {
            "id": "a29f2664-79d7-47c1-a9cf-3acc10428713",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 79
        },
        {
            "id": "0d0237ce-f6cf-42c8-8ccb-a86e4dc0074b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 81
        },
        {
            "id": "ca2b1c94-2a95-46d0-9590-5583b8b7e262",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "71eb99e5-c8b2-4f79-8fc5-e989ea43f588",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 85
        },
        {
            "id": "593b5691-2430-417b-96c3-8a676970a004",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 86
        },
        {
            "id": "d9a4d746-865b-47a1-a32c-0ab9039279d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "0c6f7f97-2294-41ef-9b7a-b1586ff0e146",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 89
        },
        {
            "id": "2fd919a0-d2de-4da9-ba81-277c4a4d4fc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 90
        },
        {
            "id": "0fbcf75b-2e8e-4049-9ea7-d0119ba109fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 118
        },
        {
            "id": "f652087c-703e-488f-b043-f5ed334a2cff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 119
        },
        {
            "id": "e90a9d3d-1d1a-4dd6-82f3-d37ffb84597a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "529fce3e-5122-4cf1-824c-d23c6609b3b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 199
        },
        {
            "id": "767a0317-52c1-4481-9b14-aaa654989619",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 210
        },
        {
            "id": "93a05bae-bcb3-4a26-856f-7d95dfbf160e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 211
        },
        {
            "id": "daf1e6b9-c058-48dc-95da-b76f15119d42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 212
        },
        {
            "id": "82acd001-a7b6-4615-9a9f-54825addcdbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 213
        },
        {
            "id": "b0b2e76a-7efb-4fb5-bcca-633ffaf27237",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 214
        },
        {
            "id": "d9cdf914-4928-4113-94a3-af91df85ee36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 216
        },
        {
            "id": "d54ca847-29bb-4b6a-9501-93015ec319cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 217
        },
        {
            "id": "715b12f6-23d5-45d2-8f96-ab093893eefc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 218
        },
        {
            "id": "74be7cca-35eb-4c17-a5f4-c8b33146008b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 219
        },
        {
            "id": "52e78eae-9104-4498-92cd-b75481d1d6ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 220
        },
        {
            "id": "41752455-0f97-40fb-b87d-a70ea2334dbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 221
        },
        {
            "id": "f5ac9fa6-ed3b-4c07-a300-03f82979f5c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 253
        },
        {
            "id": "376b26b3-c073-42b4-8d3e-db873f71697b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 255
        },
        {
            "id": "10a23ca6-638e-47d3-8065-9c97d279ea8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 256
        },
        {
            "id": "ef345e05-7b09-4f59-9cfa-3b10081ae917",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 258
        },
        {
            "id": "9384f06a-8160-4cfb-84c3-60b1072d6dec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 260
        },
        {
            "id": "e27d3e39-27ba-424f-a00d-e78c66ffc4d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 262
        },
        {
            "id": "a6c40c5f-4c01-4f94-9792-7582222207a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 264
        },
        {
            "id": "73f96e9a-1ca3-492f-88da-7fe004da2a95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 268
        },
        {
            "id": "68b1aa0f-a479-4493-96ec-4c4c58eeaf56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 284
        },
        {
            "id": "b9c3211c-63ed-41e0-bb6e-d029e1842af3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 286
        },
        {
            "id": "e543ebf5-62d5-4400-8155-49a5fc2f966a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 288
        },
        {
            "id": "a861f5fa-b3d5-4cfb-9d01-218dad9abe42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 290
        },
        {
            "id": "27c90fd0-bde3-4ca7-b568-aee2690b3ec4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 308
        },
        {
            "id": "e34a8859-1899-440d-97f5-3ee80b02d7c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 332
        },
        {
            "id": "e3ba4fa0-14e0-4dc0-9c2e-3bd78f7c06a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 334
        },
        {
            "id": "0eab6372-2b1f-4d6b-8c2f-a92a3725f881",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 336
        },
        {
            "id": "8cebdc71-a582-4f11-bf0d-cbc476be8b93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 338
        },
        {
            "id": "67d7051a-8ef0-4bde-8f3c-04935f0f1542",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 354
        },
        {
            "id": "db1e4175-1ffe-42c9-86a1-e39dcfceff41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 356
        },
        {
            "id": "9d2504f9-40e7-4279-8d5c-cfa444adbac9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 358
        },
        {
            "id": "9f45418c-23cd-4e89-9c75-aaee6d7ae9ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 372
        },
        {
            "id": "23e8c19d-dc5d-48b8-859b-77c27b39d0a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 373
        },
        {
            "id": "b7330246-075b-4ab1-b14a-c268317085ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 374
        },
        {
            "id": "46e6cf70-9aab-46c2-935d-b714450fac60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 375
        },
        {
            "id": "349983e2-7907-4104-88e3-8cf34211965f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 376
        },
        {
            "id": "e14af9f4-ae13-4b7e-8454-59ba502f66dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 377
        },
        {
            "id": "d9ec83b9-0483-40dd-823f-f2c7daba2019",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 379
        },
        {
            "id": "ceb52421-f85e-4355-aa4b-82f0e1c806be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 381
        },
        {
            "id": "0ce411a9-ae0b-4833-a4c7-f148d9b7777c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 506
        },
        {
            "id": "7e153bd2-9832-42a4-9d5f-7ae1dd3769c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 508
        },
        {
            "id": "79744674-7525-4d58-b90c-5827da60a674",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 510
        },
        {
            "id": "1328c7aa-85ba-45d9-9720-caf521e4bf7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7808
        },
        {
            "id": "5e9ce67b-4c8e-438b-9f13-735c952b6890",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7809
        },
        {
            "id": "9edeb760-4313-4bd5-9340-b310dc7d39d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7810
        },
        {
            "id": "d077e99a-72b1-4c48-a384-001cdde6493b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7811
        },
        {
            "id": "bfb4d535-a098-40dc-b9ef-44af06868adb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7812
        },
        {
            "id": "af44468d-204d-4fa4-9fa8-7cef709ec8ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7813
        },
        {
            "id": "77c0d4e0-b884-4596-9532-af229a001af6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7922
        },
        {
            "id": "8caca663-fb8d-4829-ba30-7f4f421f41bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7923
        },
        {
            "id": "c3cf7aa6-9bd8-4ee9-a9e6-116fab29bd5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8216
        },
        {
            "id": "c44cf994-d550-450f-bac8-ee0b531d3cef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8217
        },
        {
            "id": "e9960282-200c-41c7-b04e-2a512f7c2a83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 8218
        },
        {
            "id": "c590ada4-0906-4989-a87f-7ef59d3ec321",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8220
        },
        {
            "id": "3d733be9-d90e-4b3f-b79c-fb3614795148",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8221
        },
        {
            "id": "d3d77ea5-fb28-4b04-80af-c24a258dba7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 8222
        },
        {
            "id": "66a4d805-b2b9-469a-a0ae-1794c7fc4352",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8482
        },
        {
            "id": "e475b3d7-6099-4e4d-9271-50d7ecc28189",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 44
        },
        {
            "id": "8f454a98-6fc3-4621-b400-b93f1288d2b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 46
        },
        {
            "id": "5a29777a-d361-4531-b58e-a9d67c449e3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 84
        },
        {
            "id": "cc6bb356-590d-424c-82cc-3dbb54609614",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 88
        },
        {
            "id": "924a2dca-d7e9-4065-92da-724d086adc03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 89
        },
        {
            "id": "377403ad-0f16-4ea6-b820-fd2cf81bc057",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 90
        },
        {
            "id": "98414259-2a6f-45b0-bccf-f966c45e6776",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 354
        },
        {
            "id": "1255b95c-6908-49e3-9ca5-0540e15db381",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 356
        },
        {
            "id": "702000d6-5446-4f20-a9c5-7ef5ae17bafb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 358
        },
        {
            "id": "91604128-09f9-4cb5-baf8-b179d96cbaf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 377
        },
        {
            "id": "2f1555b0-e5e3-4718-8a64-5766c65a5cfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 379
        },
        {
            "id": "42366e86-4218-43f2-882e-8706a63ae615",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 381
        },
        {
            "id": "97d9a0ed-1cca-4c8c-b015-4135cb59335b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 79,
            "second": 8218
        },
        {
            "id": "47a7c06f-ffd7-4908-b307-400ee6acc2b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 8222
        },
        {
            "id": "564f168e-6ce1-424c-abab-b20a8467bb2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 8230
        },
        {
            "id": "977450e0-de14-4679-92bb-ab507c75d5f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 44
        },
        {
            "id": "95fdfb71-d5be-487e-83af-11a5325afc2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 46
        },
        {
            "id": "35e0a1cb-eb6e-43d6-a917-86d70ec6a4ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 65
        },
        {
            "id": "6cf82b6e-2981-4a67-86b5-2f2e0bef2f99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 74
        },
        {
            "id": "ba3a21e6-748f-4a50-9640-e53e5700cd39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 87
        },
        {
            "id": "aa4470fa-e981-442e-8361-caed32ae6914",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 88
        },
        {
            "id": "8f5126aa-54aa-4022-a6f0-0d6fb7187bd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 97
        },
        {
            "id": "476dc498-d280-41c4-b8d6-8006fff4b145",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 99
        },
        {
            "id": "884335da-2e5d-456d-b5f6-1db5e905d04d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 100
        },
        {
            "id": "c2b19e96-07cb-4d35-9466-2807764a2ec8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 101
        },
        {
            "id": "ff7fe3ad-9799-4307-bde1-fc4636063804",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 103
        },
        {
            "id": "e7be0c75-85da-4a2a-bb20-dd3de2707ed7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 111
        },
        {
            "id": "554dc382-5794-458d-befb-220fe8ca7fef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 113
        },
        {
            "id": "117bf13c-a2a1-4770-909f-1cf549f2a2d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 192
        },
        {
            "id": "072e5675-e031-4178-b4d8-6537f48e928f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 193
        },
        {
            "id": "4451f423-fab6-4c12-aebe-0157e312b6ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 194
        },
        {
            "id": "90d4931d-92e2-4f05-bc2c-d41ed29cd613",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 195
        },
        {
            "id": "ea6c4fbd-196d-467c-8017-ba35bee3aa72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 196
        },
        {
            "id": "57ca6c85-0b22-4281-9299-b387c2e49e59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 197
        },
        {
            "id": "1af2439f-0acb-49c4-ac64-fd8dfa631147",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 198
        },
        {
            "id": "96c71409-46cf-4f6a-8fc4-cf4a6ed31af4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 224
        },
        {
            "id": "b370235c-3c45-4eae-8c57-6b3c5494ade6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 225
        },
        {
            "id": "0107d4d9-e181-461a-86f9-75f8ed5b3f1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 226
        },
        {
            "id": "d996c87a-73ca-434e-9b60-be53c26ee8ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 227
        },
        {
            "id": "49624f5e-2bf9-4379-9324-f692935a158f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 228
        },
        {
            "id": "25f2cd76-201d-4176-afdf-6d7a8726a246",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 229
        },
        {
            "id": "cda1eccb-5cf4-4c10-9921-d86d5f40bb3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 230
        },
        {
            "id": "162124b1-87ed-4876-9a3e-30d194e67fc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 231
        },
        {
            "id": "52fa148e-c655-4ff4-901e-6fe6fa5a201f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 232
        },
        {
            "id": "0dec7a54-0f79-48ce-846b-02a844eeb08f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 233
        },
        {
            "id": "510a794b-9cd0-40aa-b6ad-0b1f36b4bb67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 234
        },
        {
            "id": "64d0978f-6628-441d-a0fe-e3164e7cd3ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 235
        },
        {
            "id": "b65d5738-5486-478f-87c3-123a557202f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 242
        },
        {
            "id": "b0ae7aae-e1a9-4afe-8b16-b86120f2e496",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 243
        },
        {
            "id": "3ca3586a-b7dc-49eb-a50a-3dd4213e9311",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 244
        },
        {
            "id": "25d07ddc-242f-427d-a2ee-a5e887fe67c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 245
        },
        {
            "id": "c3cec1f0-6f83-4a90-ab90-a710daf1a56a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 246
        },
        {
            "id": "61c589bb-9fb5-4ca2-b7ea-e66c4614257d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 256
        },
        {
            "id": "1a43b313-49c4-41c0-bef9-25cbf6817baf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 257
        },
        {
            "id": "77706460-b43c-4674-b208-ea1a2dae34a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 258
        },
        {
            "id": "717eb254-8b84-4055-904f-4eae6fb222a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 259
        },
        {
            "id": "3eb6bbbe-7e24-4cf6-8738-73cbfb9fee45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 260
        },
        {
            "id": "885a2830-fe50-4db7-8105-95c0de2a64d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 261
        },
        {
            "id": "46574598-274a-435a-8520-6d81af12aa19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 263
        },
        {
            "id": "209ea319-09f3-4267-bf67-281bb1cb3d78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 265
        },
        {
            "id": "60efd337-e358-418d-8848-360fd94d6b6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 269
        },
        {
            "id": "aa0da5a4-0e9c-4116-bfc9-20b2e62dfde6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 271
        },
        {
            "id": "6db04257-5f13-4129-ac06-4fd8a005f771",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 273
        },
        {
            "id": "c82ea943-0035-49d8-a149-ee1c11bb3015",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 275
        },
        {
            "id": "11a6739e-20cd-4615-aa73-98f4bc924aa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 277
        },
        {
            "id": "042f1cee-95c7-4d97-9af6-05731515db98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 279
        },
        {
            "id": "da0c847b-47dd-4267-883e-d70189ef837c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 281
        },
        {
            "id": "91ee7083-cf65-40e8-8f0d-1d561215002a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 283
        },
        {
            "id": "72a2de76-8021-48bf-8b78-d8d04af38466",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 285
        },
        {
            "id": "964b62f6-a49f-4027-9770-1edc572f1a73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 287
        },
        {
            "id": "ea57b23a-6d12-451b-8cc9-0f25d50cb4ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 289
        },
        {
            "id": "4e5f98cf-60bf-40b1-95c8-6d9d397972fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 291
        },
        {
            "id": "234c7a61-8f17-4678-b570-cf2badf907bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 308
        },
        {
            "id": "c20007bc-cb60-4446-b1fb-517fbdc59028",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 333
        },
        {
            "id": "cd31eb49-0009-4ddc-96a4-2c9673a38ff7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 335
        },
        {
            "id": "9f7aae12-af02-4e97-b7a4-7c1d27b8ee00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 337
        },
        {
            "id": "1542f2ae-2fce-4241-b3f7-58703f3922a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 339
        },
        {
            "id": "400459ec-5ac5-4fee-a7aa-a1c0f7db13d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 506
        },
        {
            "id": "77f6fbf6-bdb9-451d-b563-71fdcff6172c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 507
        },
        {
            "id": "89d70380-ed93-4a74-8c9b-cc88f83fce91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 508
        },
        {
            "id": "f894be20-dbe2-49a8-9e3c-d7be4e67bd5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 509
        },
        {
            "id": "41f95895-f793-42a3-ae51-54d9d1582dc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 511
        },
        {
            "id": "9398e462-9fe4-42a2-aa88-6b86e95103ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 8218
        },
        {
            "id": "724bb339-ffed-4f73-b4bb-5c2dda7bc252",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 8222
        },
        {
            "id": "c56c9a09-d5d6-4e4f-8ae3-27bb50019aed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 8230
        },
        {
            "id": "44ea4360-2195-4a6d-abb3-e7d76daa974f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 44
        },
        {
            "id": "60db6179-db26-4219-9f7b-67f82de01acf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 46
        },
        {
            "id": "30f0ca12-6f4f-447f-bf41-72a41fa21ddb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 84
        },
        {
            "id": "71203055-0f2b-4156-8398-379798b2cb55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 88
        },
        {
            "id": "12caadf8-9b26-4d57-9c18-72599a28aa8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 90
        },
        {
            "id": "4c7c0314-8129-4090-a6c0-4726a338de49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 354
        },
        {
            "id": "435fcae6-ca44-41a3-8f7e-2ff97dcc523a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 356
        },
        {
            "id": "4931898e-bc73-4131-9690-388b3fa5088e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 358
        },
        {
            "id": "88755cb1-03b3-4544-90d0-82547903f7fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 377
        },
        {
            "id": "0a8f8ed1-20a8-4843-bee7-1f8298c38cab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 379
        },
        {
            "id": "3cb3b879-f943-4fdf-8e64-50855b428fb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 381
        },
        {
            "id": "e73c7733-3731-4ccd-a577-3e07a5813e3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 8218
        },
        {
            "id": "fb234dd2-c948-45aa-8032-79a9c770d2e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 8222
        },
        {
            "id": "fad99695-ff40-4aca-8f6a-0be2384765b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 8230
        },
        {
            "id": "5d7309b3-d220-4201-8426-bc6321a87ae9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 59
        },
        {
            "id": "608c327d-bd2a-4b1b-9c22-86878bea6f37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 74
        },
        {
            "id": "d0675ab8-70f7-47e9-80f1-db6325ef519c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 84
        },
        {
            "id": "3c891c02-2cdb-48e8-b15b-4154f126349c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 99
        },
        {
            "id": "c7d8bd97-df5e-46d5-afeb-fe407193372d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 100
        },
        {
            "id": "6f3c9618-356f-4403-93b8-095c5b230294",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 101
        },
        {
            "id": "466df86c-4f15-4c02-8984-656e61fa9928",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 103
        },
        {
            "id": "fa3dec28-ed15-40d7-bcc4-012de94d3cca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 111
        },
        {
            "id": "0c702550-3330-4279-b8c0-b4261bd1bcc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 113
        },
        {
            "id": "d0820227-84da-469f-b2dc-a998ee06bd8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 231
        },
        {
            "id": "93d3f5dd-0402-452f-8da9-1653c270cd75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 232
        },
        {
            "id": "446137f4-d113-4538-88d2-2859225acf73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 233
        },
        {
            "id": "953d9226-a3f4-4522-be2d-a3dabfb1186c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 234
        },
        {
            "id": "4b1ab577-9a66-4056-b071-a58d1abe4a29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 235
        },
        {
            "id": "1a67dfaa-f991-4023-8b96-d511b8ee5140",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 242
        },
        {
            "id": "030644b0-73ca-4ba3-b817-3af038429bc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 243
        },
        {
            "id": "6c680eee-7097-4728-a810-08f30a23dd0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 244
        },
        {
            "id": "cc32515c-6350-4c16-afa7-645d61b7f7d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 245
        },
        {
            "id": "fa21c84f-17f5-4304-9c8b-0352fd2c0b0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 246
        },
        {
            "id": "0350152c-415b-4fce-be66-6fc94b72cda6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 263
        },
        {
            "id": "d05a6105-2402-4f9b-b2fd-f77c7cdadb4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 265
        },
        {
            "id": "73ece87e-4c08-45b8-b47a-934b4f03ae18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 269
        },
        {
            "id": "94776517-5fc7-4db8-bca5-3412f12426d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 271
        },
        {
            "id": "4ce970a9-9f3c-47f2-92e7-755b4652d566",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 273
        },
        {
            "id": "5ac1f37b-00d6-4432-a551-a4f42222c99d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 275
        },
        {
            "id": "84fea010-c1bc-45b6-8785-8cc50862191d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 277
        },
        {
            "id": "abc1515c-5971-4b26-82d0-c6ad903c8de2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 279
        },
        {
            "id": "ba00e776-adc2-4fd8-a643-cb390b662e80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 281
        },
        {
            "id": "12e8ad64-744b-4967-a770-06165b25b88b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 283
        },
        {
            "id": "f0a0dc88-ba1a-4f05-b423-ad9d896eecef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 285
        },
        {
            "id": "d10669db-2d43-4384-b62f-7449f8900dfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 287
        },
        {
            "id": "2efba044-a6e8-4915-a003-28d8a2228621",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 289
        },
        {
            "id": "4eb5c074-0eb9-4ca1-a751-0f21f53820cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 291
        },
        {
            "id": "6304f624-38dd-468c-bda3-b4c760bd27d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 308
        },
        {
            "id": "eee47b18-a92f-4a06-a95a-83cbc85ca09e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 333
        },
        {
            "id": "5f34137e-2cba-411d-85f2-627416eb5c1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 335
        },
        {
            "id": "e795e3b1-1fc2-49f8-a9cd-6cde730d480c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 337
        },
        {
            "id": "026a2542-35b5-4940-95f3-9331016b5ee9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 339
        },
        {
            "id": "09b58ca1-6828-4896-95ba-ed4959712c1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 354
        },
        {
            "id": "1e512bd6-c6c6-4847-84eb-3c908a1da5fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 356
        },
        {
            "id": "89a0294a-54a0-4be1-a40c-f2c61c40fd06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 358
        },
        {
            "id": "b7d58306-5b8f-4080-ac37-e2e4ffb430d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 511
        },
        {
            "id": "41bb66a8-8699-4c41-a4a1-c0174af273f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 116
        },
        {
            "id": "4e9184a6-2581-4c69-9f72-9be41275a2db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 118
        },
        {
            "id": "f40fb8d2-4fd1-4c6a-9b99-34156d42aad3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 121
        },
        {
            "id": "54d3166e-1966-49be-a2a7-c514baa9f3ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 253
        },
        {
            "id": "15f0d9e1-6e0a-406b-8bf6-1e85bd329037",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 255
        },
        {
            "id": "36377bc1-a331-4356-a168-df0ae30f1ea2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 355
        },
        {
            "id": "7b67c1de-1117-4e5f-89c9-6ab595f053e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 357
        },
        {
            "id": "6d1ac8b2-af54-4b88-abe6-fbd7624f7f04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 359
        },
        {
            "id": "39efbae9-7b1c-442a-8446-9f92cfc6b874",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 375
        },
        {
            "id": "2b39f3bf-c721-4885-8b95-ddc42fa88b75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 7923
        },
        {
            "id": "c34516fc-38de-401d-958d-4f23bd3e33b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 8218
        },
        {
            "id": "7c17fc37-4568-440e-8236-bba366329089",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 8222
        },
        {
            "id": "973cd446-ffa6-4015-ba2a-0c11f9ac02c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "6de7af56-da3a-4fdf-b6a0-b09ff7f51564",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 46
        },
        {
            "id": "340894f9-ea26-44d2-a2ad-3510a2df71a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 65
        },
        {
            "id": "7d49ad8c-d26f-4e5a-96c3-2d73ce789351",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 67
        },
        {
            "id": "fbbf8688-e2dc-484a-81f8-7f6cde09853b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 71
        },
        {
            "id": "631d2007-e57f-4fc6-b781-6a1a9a9e00e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 74
        },
        {
            "id": "8857b17e-e16f-4131-8bec-e382b0eacf33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "bb7d4936-dc9e-42e5-a324-fef866306c6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 81
        },
        {
            "id": "a5b62fe0-e536-42b0-a391-c624c54eeeaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 84
        },
        {
            "id": "77e25f74-db2a-4ca5-9630-95838892e865",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 86
        },
        {
            "id": "296a5525-8bc9-437c-b99f-704d8bc6d786",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 87
        },
        {
            "id": "d4d1c29e-1a30-48b3-8764-4e7924d68fa9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 89
        },
        {
            "id": "07911144-e292-4e1b-a297-33febb576464",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 97
        },
        {
            "id": "a91cd913-bb41-4a46-83f6-ac2e7240b68a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 99
        },
        {
            "id": "c707f398-9138-45b7-af5c-3a59e36d1547",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 100
        },
        {
            "id": "510bb7c0-c115-4adc-b138-3b613ec89ab9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 101
        },
        {
            "id": "91f6bfae-0179-41ca-a91b-81c9abef2395",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 102
        },
        {
            "id": "12aac47e-7692-47d7-a19b-dce9b27c034b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 103
        },
        {
            "id": "fa5523d7-52af-4505-90b5-51a5883f7aea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 109
        },
        {
            "id": "21d237ea-e8d5-41fa-a916-083a4f97c232",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 110
        },
        {
            "id": "a2e34a74-d9e9-4076-b00b-3a5887c1781c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 111
        },
        {
            "id": "66d56a12-1a49-47de-9355-28a0c643da1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 112
        },
        {
            "id": "ca841dc1-e36f-4e80-b258-5eee9aac4b8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 113
        },
        {
            "id": "34441ea3-3825-467b-a993-ae2e4bfbe8a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 114
        },
        {
            "id": "1a115edc-681e-493a-951d-2229124bcfd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 115
        },
        {
            "id": "5fbfeb07-edb8-4249-a447-10b9581e1d91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 117
        },
        {
            "id": "487825ce-825c-4d20-9242-306d792db85a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 118
        },
        {
            "id": "712e7c9f-f38b-424b-8da0-b801ffc9ef2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "2b4f1987-f8f7-4c7d-ad43-3b1f91cb2b47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 120
        },
        {
            "id": "b7893a58-2c1f-4689-bf49-9d5a17a5144a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "a137fedd-db66-4a18-9467-a28d773a7952",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 122
        },
        {
            "id": "9d697e1f-107a-4ec4-9ad4-bec138b6f14b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 171
        },
        {
            "id": "bc83e6a6-a486-487b-9d61-ce9a337ea519",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 187
        },
        {
            "id": "2f7d7bc9-beba-4a46-91bd-98c21654f0d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 192
        },
        {
            "id": "cef4819e-0fec-4c5e-95b6-ddfcae2f7b98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 193
        },
        {
            "id": "ba6cb70b-f361-4edc-a9ee-bd806df4281f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 194
        },
        {
            "id": "1a2a13e2-9105-4432-b9f6-8bb742095667",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 195
        },
        {
            "id": "db10eb7e-6b32-4672-ada6-85fe1c1db5a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 196
        },
        {
            "id": "5fef6b36-1582-4481-b95f-22578cb5d84a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 197
        },
        {
            "id": "a83c6876-57d0-4d02-8ca8-11849e3d6cb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 198
        },
        {
            "id": "7a238609-2c57-406b-b51e-658b5fcdcb20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 199
        },
        {
            "id": "c1f01891-4271-455c-9013-cde8e2004e28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 210
        },
        {
            "id": "52535af4-29cd-47af-a7e3-a55ceba042d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 211
        },
        {
            "id": "06f0ee83-b233-4297-9260-06dfd28de386",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 212
        },
        {
            "id": "2c1be1fd-a7df-4343-870a-bf7d22a2c68b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 213
        },
        {
            "id": "abc98b5a-bd4a-423e-9b36-6fba5ee2afa8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 214
        },
        {
            "id": "9e397efc-8525-47c5-ad7b-853655d0b46a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 216
        },
        {
            "id": "7441bfe9-2a99-4750-8e29-94cdfc153a63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 224
        },
        {
            "id": "0b33b675-8e93-461c-a574-7292e48af9b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 225
        },
        {
            "id": "057603f1-a360-44d7-b91f-8107a1b07315",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 226
        },
        {
            "id": "ec9a6c2f-b0dd-446d-bcc2-0229271b3133",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 227
        },
        {
            "id": "29b0aad7-2b5b-4c84-8f6f-e5efda3734da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 228
        },
        {
            "id": "86401341-d689-4377-beb5-842cb9d9e11c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 229
        },
        {
            "id": "8a470f70-b0ec-437b-9653-ddbcba46e925",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 230
        },
        {
            "id": "05638cc2-98bc-4bb1-950a-25b5c9dd16b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 231
        },
        {
            "id": "066ddb57-0c29-4a4c-b69e-8aecbc9cd885",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 232
        },
        {
            "id": "c3288e22-6427-4fe8-a145-b29d4eaf1429",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 233
        },
        {
            "id": "718c69a1-4308-49a9-8f19-ed12bb37a89f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 234
        },
        {
            "id": "2c5d164d-c636-414f-a0b1-a52c05fbfa9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 235
        },
        {
            "id": "e566623d-e0fd-4ead-ab9f-80940133c14b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 238
        },
        {
            "id": "1ec70fc5-6205-4765-a34a-3203e4d82c12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 84,
            "second": 239
        },
        {
            "id": "e04b498a-b2d8-430f-ab28-09af4b20b451",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 241
        },
        {
            "id": "e0944ea7-4e10-4938-ae9e-329bbf0e6256",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 242
        },
        {
            "id": "9656981b-fc12-408f-a0d9-db3cbec1590a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 243
        },
        {
            "id": "27f0cf44-eca6-4fba-bd19-1b60c8e4a87c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 244
        },
        {
            "id": "26591503-d95c-488b-94ef-1862bcf1afb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 245
        },
        {
            "id": "37fe658e-36d5-4190-acab-fe2e13fb6765",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 246
        },
        {
            "id": "3529a6aa-c13e-4cad-a154-3be6e25f8406",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 249
        },
        {
            "id": "d4082c72-fe7e-404c-be8e-523c0d439037",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 250
        },
        {
            "id": "e47ee3d3-315a-4a6f-a202-24b372b2d22e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 251
        },
        {
            "id": "9273c4d6-8cf7-4c15-8df2-9cb5a35e428a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 252
        },
        {
            "id": "e821f678-04ea-45de-b787-c8f54b94c352",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 253
        },
        {
            "id": "afbed801-c33b-458e-8442-e8a30273f895",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 255
        },
        {
            "id": "1f942b6e-679c-418b-834e-56234ec3b9db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 256
        },
        {
            "id": "1bc0cccc-e237-4e9d-88b8-4be27c98834b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 257
        },
        {
            "id": "72128afa-5c4d-44fc-8dab-d68a5c57d094",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 258
        },
        {
            "id": "75c8cd90-d982-47af-b49b-56868c0dadb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 259
        },
        {
            "id": "fdd6fc36-a671-42b2-af35-be6acab7df14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 260
        },
        {
            "id": "ea82d7a8-5bb2-41b8-96af-e53f26e32130",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 261
        },
        {
            "id": "37a69428-a967-4ada-a696-30b05e8a79be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 262
        },
        {
            "id": "9efa4532-07bf-48ec-b7ea-c8e44de6e304",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 263
        },
        {
            "id": "6d448700-fa3f-4afa-afa6-3cc26f907eb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 264
        },
        {
            "id": "2b326409-4fe1-43ef-9394-7519baddbaf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 265
        },
        {
            "id": "25da7584-e2cf-4456-9482-01817a21f7bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 268
        },
        {
            "id": "80f72cdf-b6e7-48ef-a766-7b58330f6697",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 269
        },
        {
            "id": "ad907f15-4add-47cc-aef7-d4affc38459e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 271
        },
        {
            "id": "c344e63f-33aa-41de-9ef6-5fdab82a15ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 273
        },
        {
            "id": "5a08f831-ddd1-4992-a7b7-3e9aa8bc181a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 275
        },
        {
            "id": "88c7ee67-3892-4934-9bc2-e42b2ebeb169",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 277
        },
        {
            "id": "d40c8f70-90af-4a88-b5cd-938b1d7998c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 279
        },
        {
            "id": "9cb83005-8bb9-413e-b058-b63d9d793816",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 281
        },
        {
            "id": "89c70600-7cb3-4402-aa27-a3fe651be06a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 283
        },
        {
            "id": "5a23132b-009e-4c8a-8cb6-84edd2c6bd87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 284
        },
        {
            "id": "d2f376d1-17c0-4b36-a867-05f897a35421",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 285
        },
        {
            "id": "d6d09d73-e617-4bb7-92e2-7d77e3a3b0b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 286
        },
        {
            "id": "40dcc4cd-9ca3-4cb5-88b0-51f3e1a1e3f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 287
        },
        {
            "id": "113ea9f4-2e07-431e-93bb-5964905f0995",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 288
        },
        {
            "id": "95c1bca8-3f01-41d0-b1a7-ec6f1ef430e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 289
        },
        {
            "id": "93165b0f-b8ed-45c4-b908-439185d65117",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 290
        },
        {
            "id": "0c79272a-4b30-45a4-9732-c85d3fa20b16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 291
        },
        {
            "id": "06df26a4-115b-4e52-8e0e-ccfbc3426f68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 297
        },
        {
            "id": "c4ac44e5-9e43-4ee8-a974-c0d4f800d23e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 299
        },
        {
            "id": "d4002ac0-fd7d-4b14-8d07-2b75c4d78995",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 301
        },
        {
            "id": "b7fc985a-a7d8-4d71-b875-deb0a76d052f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 308
        },
        {
            "id": "db8b89ef-b1a5-4baf-9d45-1be69f2dca2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 324
        },
        {
            "id": "d5afa654-76ba-42ca-bf30-c69264947e37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 328
        },
        {
            "id": "7d9d7137-3dc0-47fd-b1bc-ca513ca8e989",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 331
        },
        {
            "id": "21175bd3-558d-416f-bfe9-b46bbabcd5c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 332
        },
        {
            "id": "e68db48f-d256-43db-8631-d9fbd30a9c8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 333
        },
        {
            "id": "093531d7-d90e-4d91-82a8-2562181c57b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 334
        },
        {
            "id": "6260010c-4d56-4cac-9abf-7c2f51cd220a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 335
        },
        {
            "id": "c861d0f6-01da-4c9b-b649-af0d87ee10d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 336
        },
        {
            "id": "855f33b5-31e7-4ca7-a72e-bab977c075d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 337
        },
        {
            "id": "3cba5383-4589-47df-9918-8b893e30f974",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 338
        },
        {
            "id": "59444cfe-a9b6-4ad4-b872-db2fae2ef105",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 339
        },
        {
            "id": "672e33bc-74e6-4a4f-97db-ecfdd6845ea3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 341
        },
        {
            "id": "0effeda5-db1a-4451-b55b-5dbf30cbb7dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 343
        },
        {
            "id": "b1a4c867-5567-475e-8e30-93351f7ce89d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 345
        },
        {
            "id": "082a4ca7-ceab-4d1d-b2e0-a06535dcd0f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 347
        },
        {
            "id": "e59233c1-0d86-4646-a66c-f57f0580f510",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 349
        },
        {
            "id": "a5fd9148-a7f1-4ccb-8877-440c0a4bb4e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 351
        },
        {
            "id": "bd5ea442-1dd9-4a59-8c48-f0876cf190b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 353
        },
        {
            "id": "56b93fe6-d883-4bad-886c-eca211b2e4ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 361
        },
        {
            "id": "4d4747df-fce0-4d1c-a7a6-08f8e6c16c2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 363
        },
        {
            "id": "3d6d0577-e76e-444f-853b-92eddc276805",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 365
        },
        {
            "id": "06b1e93c-fb6a-4713-92df-334553bc937e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 367
        },
        {
            "id": "105581b4-89b0-441a-8081-d2d3e96e3b4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 369
        },
        {
            "id": "307a5638-b8d1-488d-a204-f65976a6ca6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 371
        },
        {
            "id": "40735752-e3b4-4479-a29b-ff1bbc929c40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 373
        },
        {
            "id": "6f9868ed-d92b-428b-9c25-993a744af30a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 375
        },
        {
            "id": "15443d56-5932-4aaa-b893-cd395434048e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 378
        },
        {
            "id": "70958fbb-4ff7-4cc0-a300-a948bf2fb4bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 380
        },
        {
            "id": "c2466140-6879-416c-88cf-0b171d644916",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 382
        },
        {
            "id": "6c6ecb73-240d-4e3b-b6a9-192278511c26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 506
        },
        {
            "id": "d8dbcc4c-d908-4d1b-919d-fcab9a4788fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 507
        },
        {
            "id": "f093ece9-309a-40db-9240-f005c4e3a4b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 508
        },
        {
            "id": "8417e435-9f4a-4cea-8fad-1acb7e7b88a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 509
        },
        {
            "id": "b00af03f-272c-44a9-a3bd-6467faae25c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 510
        },
        {
            "id": "83a1606c-085b-4e85-bba0-270e2d325a31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 511
        },
        {
            "id": "6e47a1d7-953f-4146-b37a-7da82668afcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 537
        },
        {
            "id": "555203cc-8372-4d8b-a7a7-d6c07301d1fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7809
        },
        {
            "id": "c4e64c3a-8006-45ec-90e7-e9bcf6a7f0d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7811
        },
        {
            "id": "4f9330fa-202e-43c8-87f2-d1e462578deb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7813
        },
        {
            "id": "499db1cc-c004-4bce-b13b-17d4218e3186",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7923
        },
        {
            "id": "6bcb39b1-3a17-4a6d-b624-310452b07bf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 8217
        },
        {
            "id": "2af63136-04bf-4fcf-a559-31fb61d299fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 8218
        },
        {
            "id": "9f42fe63-cb71-4108-92ed-c4996f42a07f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 8221
        },
        {
            "id": "4deff45a-9556-4aee-876d-1e12e1c17ed1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 8222
        },
        {
            "id": "b34cde01-359a-4a3b-844f-bdf8f187a11f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8230
        },
        {
            "id": "0f98b153-b0ec-4d39-96e0-08df7d78ec9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8249
        },
        {
            "id": "8c8b395c-22a0-444a-88bc-cd87ac8a0b71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8250
        },
        {
            "id": "45983043-b11b-41db-9f2b-ceb28d2bd6ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 8482
        },
        {
            "id": "9366fcb3-5efa-437c-a94b-38b44fdff0b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 65
        },
        {
            "id": "a52fc534-247c-4bcc-91f4-a942a912a9c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 74
        },
        {
            "id": "4277d071-fcf2-4ef4-b6ab-653b387c4c58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 198
        },
        {
            "id": "58f7b048-6712-4b38-9d2d-d19c0f73212c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 256
        },
        {
            "id": "16bb64a7-0346-4a5e-a96d-a009c535294e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 258
        },
        {
            "id": "d1d0ada3-9a06-4ac6-b08f-f26c1211b7c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 260
        },
        {
            "id": "29669182-1af5-4780-9e44-66b8ebbc84d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 506
        },
        {
            "id": "effb1421-dd03-4e6a-821e-9588f7165936",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 508
        },
        {
            "id": "a8002716-38fe-4d07-b183-ad4b048d4cca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 8218
        },
        {
            "id": "db31f35e-a3c6-4ef1-9409-e4a3b198b00f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 8222
        },
        {
            "id": "8bb86d55-63a3-46ea-b7e6-3fb9f2392b65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 44
        },
        {
            "id": "4f95e930-a7de-4e82-95df-f7d05bbc6335",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 46
        },
        {
            "id": "d5649e97-c490-4500-a305-43cd5eccb9d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "1954a7f6-6e23-4c0f-be73-1bfeb1a261a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "a392f37b-dd25-4414-9cc1-13265e783614",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 65
        },
        {
            "id": "f1f35ef6-e20c-45fb-87be-933c3133217b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 67
        },
        {
            "id": "b126a178-f6dd-4787-aecf-1efbb38288aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 71
        },
        {
            "id": "b487da39-4690-45a0-b170-652a3705c12d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 74
        },
        {
            "id": "23365fbd-1881-411c-8dbd-0f72b04d4af7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 84
        },
        {
            "id": "a4ea0e91-132c-4e49-ac73-5df23d9938cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 97
        },
        {
            "id": "ce285c51-ec91-4504-ac05-2d7973cef416",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 99
        },
        {
            "id": "5ed36759-a90d-4850-a01b-8817114345b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 100
        },
        {
            "id": "9226a1aa-ee08-4c94-bd54-1eb4f3a8dbe0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 101
        },
        {
            "id": "e0a46885-c9d1-4ec0-9398-d2cdd8bcbde1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 103
        },
        {
            "id": "56b1f6dc-0cdd-4286-80f9-6a0e488bd07a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 109
        },
        {
            "id": "89f89004-1519-4f0f-af63-142f79efb0f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 110
        },
        {
            "id": "63dcae39-745d-4638-9c46-27caa5d92db5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 111
        },
        {
            "id": "c3b6f5e7-3c1f-40f5-b587-adc8f946e94e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 112
        },
        {
            "id": "97ebc517-a5fa-43c9-92de-e5f1e25376e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 113
        },
        {
            "id": "f62d5ebe-2ef8-4460-a820-17850a2d50d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "7c4e5b66-2184-4f8f-861d-a1005f751af9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 115
        },
        {
            "id": "d515df1e-f198-4a4e-b5dd-456420db5b00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "43748b58-764c-47bb-979d-8dfe6e40576d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 171
        },
        {
            "id": "3d1cc57c-65e3-48ac-96d2-6c53ab709726",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 191
        },
        {
            "id": "dcab1a81-71ba-415f-bc34-94ff8fe9d251",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 192
        },
        {
            "id": "9e162fb7-f5c3-4ecf-8c09-bd10f6965e80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 193
        },
        {
            "id": "6227e4f2-0acb-4846-9b6e-2076882d7848",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 194
        },
        {
            "id": "b094400f-580f-4ffa-87fc-81490eee5372",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 195
        },
        {
            "id": "0daf201e-a586-40c2-a083-f417b510563b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 196
        },
        {
            "id": "e17e0b26-9795-492b-a344-fb38777fb8c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 197
        },
        {
            "id": "2fb1181f-130f-4470-baad-66717010a421",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 198
        },
        {
            "id": "4c3e7921-c488-4a67-bee3-dfa23fc6e15a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 199
        },
        {
            "id": "421ffbc1-bf8d-47a2-90ea-45b66a65f883",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 224
        },
        {
            "id": "f93ad166-ce0d-460e-adcc-17b77e176c34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 225
        },
        {
            "id": "1745cdc6-c6f1-4def-b902-7b0fa125fd99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 226
        },
        {
            "id": "9a063398-69b0-481f-827c-c937ab1fbea4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 227
        },
        {
            "id": "4ba98415-d02f-490d-9f0a-ea245b0eeeed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 228
        },
        {
            "id": "91b9db00-442b-46b8-b407-ae1ffe0603e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 229
        },
        {
            "id": "b8e35afc-64f1-47ec-b4e0-78d70bc884bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 230
        },
        {
            "id": "2f67ba7d-c5ac-40c6-9db7-9c899bd5d641",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 231
        },
        {
            "id": "3502fcd9-9b63-4b4e-9fb7-959218f929b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 232
        },
        {
            "id": "6676142d-98cc-4495-a2d1-a77df7bbf888",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 233
        },
        {
            "id": "43bc8072-8db6-41e9-b780-a1a64e2f6b2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 234
        },
        {
            "id": "9ffff4f8-a180-47b2-b69e-2d5f503a4b9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 235
        },
        {
            "id": "d0a1809f-d45a-468e-ba34-d55eb928866d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 86,
            "second": 239
        },
        {
            "id": "a91e5321-2ae3-4cf3-819f-a8b87954d10f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 241
        },
        {
            "id": "d0a9b7d1-0169-4413-82ba-f45787950546",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 242
        },
        {
            "id": "d4989883-c4ad-40ee-96af-667c150f9500",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 243
        },
        {
            "id": "2dc041ad-e902-4829-8101-10b8f5ee11b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 244
        },
        {
            "id": "5de9d355-9332-46da-b904-7e15e54ad7d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 245
        },
        {
            "id": "d87fde5f-e2b9-4733-b81c-f78cee34f9a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 246
        },
        {
            "id": "62163e93-d467-4190-9a8c-4504676889d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 249
        },
        {
            "id": "fd9d2279-f7c3-412d-8441-4330f9ad0d7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 250
        },
        {
            "id": "67b20588-474f-46fd-9889-bae7be15ed26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 251
        },
        {
            "id": "de4a2eb0-ff50-48c4-b151-51c324056111",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 252
        },
        {
            "id": "dd59972f-4236-4b04-a69e-9e66685e0c82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 256
        },
        {
            "id": "4762a0f6-f1ed-45cf-a4f2-cfbb4eee3923",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 257
        },
        {
            "id": "28c45462-2b96-4228-8b4c-38d8afd1e0fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 258
        },
        {
            "id": "a83ebff9-3df9-4f9e-a6cd-0d85328ad389",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 259
        },
        {
            "id": "7808d75c-34a4-4590-a048-e835814363fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 260
        },
        {
            "id": "1a842e88-5b81-4b60-9da8-e246bbeeb039",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 261
        },
        {
            "id": "fc909499-c527-4069-b388-c07f60abdccb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 262
        },
        {
            "id": "7ea869a9-b67a-4eee-a9fe-5a27d76d1887",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 263
        },
        {
            "id": "93df74c6-b687-4ad9-9db5-4638511fdcd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 264
        },
        {
            "id": "15853311-33e1-4b33-86f9-5ea43958c7b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 265
        },
        {
            "id": "3b66f907-4f26-4678-9ebb-123790984cde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 268
        },
        {
            "id": "e1a774b0-dcca-4b83-8fa3-813498170049",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 269
        },
        {
            "id": "90b28cc5-3a08-4b2e-a090-af9f995f0388",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 271
        },
        {
            "id": "c5b2aca2-fb40-4c32-9cbd-952aab82aada",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 273
        },
        {
            "id": "a732eeb9-22fb-432b-8639-49d811a0c24a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 275
        },
        {
            "id": "853d8e0f-bda1-4dd1-a254-59ef043b704e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 277
        },
        {
            "id": "7e97294c-8298-4ae9-a1e1-38cea405a48e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 279
        },
        {
            "id": "28f1ae28-ac55-4848-bfd2-2ceca78eda72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 281
        },
        {
            "id": "dba13b14-0c69-4658-a1b0-38774d9f5245",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 283
        },
        {
            "id": "e0ab91c6-71ea-4992-b89a-8bf4e992caf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 284
        },
        {
            "id": "6026ab2d-103e-4da7-a617-4f1ed862b4ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 285
        },
        {
            "id": "629bf858-3504-4c1d-a48e-81079c2f0bac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 286
        },
        {
            "id": "a692dcda-46be-4917-ac71-8bdb561ced0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 287
        },
        {
            "id": "cd14a957-8e7a-4020-8ada-e14d4fa4cc32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 288
        },
        {
            "id": "7674a46e-40ed-4a7a-bb5a-7af04cdf5af2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 289
        },
        {
            "id": "0ef9fcc3-f44a-4a15-a394-c00794dd1e8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 290
        },
        {
            "id": "4bb31c60-12d8-464e-b0f2-bc0983ee65b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 291
        },
        {
            "id": "c37685aa-7692-4341-b245-2125278b8527",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 297
        },
        {
            "id": "2a076c47-b269-44c2-8195-f38dadab079d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 299
        },
        {
            "id": "826cc802-5aa6-4cc9-9f71-7a9aa4d04fb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 301
        },
        {
            "id": "5fd3c39a-a375-41f8-a198-b3fafa594920",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 308
        },
        {
            "id": "67226c3b-13ed-4f89-b14c-4505032b2398",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 324
        },
        {
            "id": "033fad2c-20bb-43f3-812c-9e5dd5bd56b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 328
        },
        {
            "id": "23e36e6b-c234-4bed-ad02-1cc2d6380242",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 331
        },
        {
            "id": "056034b3-b521-4ead-a2f3-c877330c6c8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 333
        },
        {
            "id": "fa3d1118-5d0c-4f39-bbed-92a82dbcff2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 335
        },
        {
            "id": "085faed3-35d6-4e38-86be-5f7a957098af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 337
        },
        {
            "id": "6a017991-06ac-48bc-bbf2-42c69b54a6d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 339
        },
        {
            "id": "b5b8d7a5-26e9-481b-8ef0-ec0e82eed3f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 341
        },
        {
            "id": "a588d77f-788f-42f8-8654-ed29b9b7ead2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 343
        },
        {
            "id": "4b79a04b-b307-4bb7-9913-942bd893ce88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 345
        },
        {
            "id": "aff9397a-4a17-414d-a6e2-83d458483b0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 347
        },
        {
            "id": "31ec85d7-e159-403d-80d4-c931669e2bd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 349
        },
        {
            "id": "4fa3a374-1381-4cde-abfd-f6afa372113c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 351
        },
        {
            "id": "d6c7f18d-5d54-47c0-869b-4f7a5b350078",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 353
        },
        {
            "id": "a03257df-059e-4c8d-a40a-8978ed7b6dc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 361
        },
        {
            "id": "59872b63-21a3-42f0-9f74-56957faa714a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 363
        },
        {
            "id": "e92f7711-70e6-44e0-bc6e-985a3767cdcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 365
        },
        {
            "id": "ad2d4410-ec69-4207-afb6-e74955dc44a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 367
        },
        {
            "id": "0f53428e-a984-41de-912c-88842f1eb2a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 369
        },
        {
            "id": "279882b9-e4da-4867-b127-1bf2efd86f44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 371
        },
        {
            "id": "91e36eb7-a3db-47a4-878a-37ed75140798",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 506
        },
        {
            "id": "ff8f5e53-5216-4021-959f-13832fdf4807",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 507
        },
        {
            "id": "0251c1e1-0790-4149-9202-004162530b32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 508
        },
        {
            "id": "3ebeb263-96bc-4f97-8ad5-a0620ef6074c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 509
        },
        {
            "id": "018da4eb-5b8d-42a8-a601-04107a0df448",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 511
        },
        {
            "id": "052d847e-2478-4c9e-b7e5-377dcc5cf324",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 537
        },
        {
            "id": "2f3712a4-a8ef-4349-bd9d-0d06eb7bffdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8218
        },
        {
            "id": "e9b59aa4-f1ce-408f-afc4-55291811aa86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8222
        },
        {
            "id": "b6ac8b2f-480d-4c34-93f0-a53bdef49579",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 8230
        },
        {
            "id": "1027bc24-e172-407b-80ef-c46c46ca3bf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8249
        },
        {
            "id": "4002b2ca-df8f-4e6d-a3f1-ac1aeb56302a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 8482
        },
        {
            "id": "973cc974-7ad9-4c67-bedc-f3b2ca477910",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 44
        },
        {
            "id": "b08c8209-c390-4b18-a16e-458127f6b40d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 46
        },
        {
            "id": "4bf0c057-c509-4ff1-b9ab-16a3d44c64a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "54993d19-e3c8-44a6-9569-e8a52942998d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "6793d755-9f80-496a-9c6b-862dc418b699",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 99
        },
        {
            "id": "14556511-b9e7-4b52-b465-bc486aa8c660",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 100
        },
        {
            "id": "d13010ff-8588-41c2-9e79-f6e941dd7ed5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "fc9a1378-a698-4614-b0d7-83fa39ce2331",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 103
        },
        {
            "id": "119cf15b-c323-47db-b49e-93c8d0b6412b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "7557c0ce-ffac-426a-8613-72d95dc32937",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 113
        },
        {
            "id": "ec07dc7d-1a26-4e43-8032-8ac1304c9c1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 171
        },
        {
            "id": "d95809ae-9b94-4273-a163-cfc0b0500e84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 187
        },
        {
            "id": "9bae12bc-31f2-45e4-9b64-03dab7fac66e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 191
        },
        {
            "id": "6ac69ac1-9f71-4661-9ea0-ea7d0cb2bf23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 192
        },
        {
            "id": "c42a19d4-da9f-4897-a986-6155cec4c46a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 193
        },
        {
            "id": "dfadcb18-e87d-4460-a2e7-ffb3484ca540",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 194
        },
        {
            "id": "900ba39d-2eb0-4648-9899-63e3222225af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 195
        },
        {
            "id": "63854ed8-8ee1-48fb-9d7f-31e7dba02462",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 196
        },
        {
            "id": "f353f444-42a4-432e-9630-f4842d918d04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 197
        },
        {
            "id": "ff3b7ade-6f18-411a-a10c-68ebad772a04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 198
        },
        {
            "id": "82adb06b-22c7-4c38-910b-30062b37303f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 224
        },
        {
            "id": "a22394dd-f77d-499b-94f9-65319d3fd349",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 225
        },
        {
            "id": "f8e803a2-278c-4fdd-8aa3-2d109ba7ba80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 226
        },
        {
            "id": "35880d4c-d141-4a9d-8166-041f69d879b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 227
        },
        {
            "id": "4fd9e228-53ca-45f4-b8ad-a29eb297e30c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 228
        },
        {
            "id": "94631a1b-502b-46a8-9f62-34f62c088edc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 229
        },
        {
            "id": "8d1efe15-7f3e-4c26-bcd3-1398490b8080",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 230
        },
        {
            "id": "f8c27747-1cd7-4b86-ae9f-f5fb5b032760",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 231
        },
        {
            "id": "1a495a37-e3a9-4756-bbca-d6e48a2a5424",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 232
        },
        {
            "id": "7a450e1e-4af6-463e-bf78-6fab01f79618",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 233
        },
        {
            "id": "634b3779-e611-4ef9-b2e6-9fdf09af4ed4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 234
        },
        {
            "id": "ddeb27c3-da93-4b90-9e4c-6adccdae099f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 235
        },
        {
            "id": "90e4fe1e-0dce-4fcb-9408-19b4e4d5a380",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 238
        },
        {
            "id": "3ad26e7a-7343-48a9-ba04-e365a8d72d77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 87,
            "second": 239
        },
        {
            "id": "89780e7e-48b4-44cc-ac7e-77571b9c0bc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 242
        },
        {
            "id": "5299f29b-9cdd-4141-b9b4-295ebdb60423",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 243
        },
        {
            "id": "63178bd7-a85d-44e1-a7c4-43bad82878fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 244
        },
        {
            "id": "29915472-852f-4ffd-bff9-bb70eed1c74a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 245
        },
        {
            "id": "7d64a99c-8e5b-40d7-8690-41e376486e6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 246
        },
        {
            "id": "93f20ed2-299b-448c-b4fb-8ec589f25e81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 256
        },
        {
            "id": "4d816151-a4dc-4f82-b943-24a805e98dfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 257
        },
        {
            "id": "748e83e1-e0dd-4f59-a1dd-3061f8db363b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 258
        },
        {
            "id": "2695697f-7ad3-42b8-b9dc-b82e3396ae1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 259
        },
        {
            "id": "ae3e758d-3665-4639-8619-8a18c4dbddae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 260
        },
        {
            "id": "762953bd-36fa-40a8-a98d-e8c88434a9db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 261
        },
        {
            "id": "8a769151-d829-4bb2-b243-35768222bd5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 263
        },
        {
            "id": "12cbf710-e4a8-4359-9c00-ede0016fa08e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 265
        },
        {
            "id": "c681b7a9-a14e-46fc-893c-fc09b5ff670f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 269
        },
        {
            "id": "8966b820-6cfe-4967-a01e-3ca7f39ce7eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 271
        },
        {
            "id": "c0bee7e6-8cff-4bdc-a1e5-2660f7ad9dc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 273
        },
        {
            "id": "2d14b1ef-3699-4491-a4da-5110d14570ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 275
        },
        {
            "id": "d7c5499e-6a70-4c66-b383-5e8c3b01a3a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 277
        },
        {
            "id": "6ab22f40-6862-4ff3-b0bf-dbbeb49691ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 279
        },
        {
            "id": "3fe477b5-4994-4154-933e-691d54125c95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 281
        },
        {
            "id": "be0ab3e7-906c-44ef-bb40-c7fe99fec00e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 283
        },
        {
            "id": "147399a7-d071-4ab4-91ec-6a926c1d1fbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 285
        },
        {
            "id": "fe42a5f8-1a11-4b83-9dd2-ab67acc8acb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 287
        },
        {
            "id": "9b7c2927-fa4f-4b78-894a-49633cd1058d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 289
        },
        {
            "id": "ed98fe60-8bac-4a45-85c9-3cebce4f6796",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 291
        },
        {
            "id": "0cca32f8-fd19-463e-8032-e4230bf1e2df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 297
        },
        {
            "id": "730a036c-3460-4d7d-a9ab-066e07fff4b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 299
        },
        {
            "id": "b5f86b0a-8945-4ee2-9924-842dc73b5f5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 301
        },
        {
            "id": "0096327e-da63-4ea1-92fd-3af5698ec675",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 333
        },
        {
            "id": "914e27e1-b189-4d3d-a8ed-ae014dd8637d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 335
        },
        {
            "id": "6f03bb47-ee0a-465b-b89b-a01ae09583ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 337
        },
        {
            "id": "92d9337d-4f88-425c-906c-2662bb330107",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 339
        },
        {
            "id": "4a599d35-8c1c-4d92-bb1f-7c6d3ea04d4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 506
        },
        {
            "id": "ca0495e8-7b46-4cc0-bac4-e89b453479cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 507
        },
        {
            "id": "3178798f-6dc0-4526-b976-17494ca6a9aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 508
        },
        {
            "id": "b76808c5-dbfd-4745-aee1-fa96fd95326f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 509
        },
        {
            "id": "ec24052c-6bc4-47ea-bc50-84a9cf120931",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 511
        },
        {
            "id": "59c4a6d1-1057-4d5e-8cdd-01ab6777024c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 8218
        },
        {
            "id": "4358fb1f-3147-49ba-91fe-b4983382227a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 8222
        },
        {
            "id": "6b76ef07-b0e6-4e21-92b2-b3bb2a320bab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 8230
        },
        {
            "id": "4c822fd4-2b89-4001-9cfb-acd769a71720",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8249
        },
        {
            "id": "5a30888c-713a-4466-8079-faf7535bbc7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 8250
        },
        {
            "id": "e0f827fe-f290-4a73-812b-a24fbe883b07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 44
        },
        {
            "id": "cc8bd6a0-9161-4a9b-ac0a-b799bbce1fbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 46
        },
        {
            "id": "16155e20-6b17-4c71-9930-750db90c9c65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 59
        },
        {
            "id": "16f99848-035e-4850-9315-5620fb53e425",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 74
        },
        {
            "id": "78ee09dd-f6bb-4c55-bb67-71110a6a2c5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 84
        },
        {
            "id": "3441a0d2-ca89-49d5-890a-fcb082b9a607",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 239
        },
        {
            "id": "01af595c-6948-47f0-b087-d3b92f204777",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 297
        },
        {
            "id": "eebac7e9-ad04-45b4-849c-031d9c7d2729",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 299
        },
        {
            "id": "1510c016-08fe-4094-98c4-561df6962cfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 301
        },
        {
            "id": "d9d463a2-97cd-4163-91a0-e9af6c380fff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 308
        },
        {
            "id": "a464633c-1fc0-4b45-afd6-a108bd914b72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 8218
        },
        {
            "id": "435cb105-28e1-4ba3-b4ce-fed755d59be7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 8222
        },
        {
            "id": "29d2d407-1bb5-44d0-8d69-5e1ba302ebd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 8230
        },
        {
            "id": "f88b6c30-169f-42bd-bf3c-dcb0ef0ca39f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 44
        },
        {
            "id": "eabc854b-828e-4054-82c7-0418eb563f31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 46
        },
        {
            "id": "0d895c6c-4b98-4964-96a6-ee5f80dcb63c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 65
        },
        {
            "id": "e4cf5a90-713a-4a4b-8f7e-95aef4acee42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 67
        },
        {
            "id": "a9a687e0-9b46-4d74-9272-54f59cd87d0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 71
        },
        {
            "id": "e3697126-7e50-42d4-a2ee-11ee5d704797",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 74
        },
        {
            "id": "3a27fd82-c7ef-4f65-8609-73853cf2890f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 79
        },
        {
            "id": "db4707e4-746b-409f-a16c-e087fbf0fef9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 81
        },
        {
            "id": "11404d42-b0ef-49ae-80c6-460a1808d212",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 84
        },
        {
            "id": "4a836e58-9691-4688-a0b6-4b191d3c9287",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 97
        },
        {
            "id": "c58153de-f9f7-4aba-8cb3-58305714764b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 99
        },
        {
            "id": "35b1ad1e-465b-4bbd-a321-f90010a0cb59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 100
        },
        {
            "id": "af855ed3-3867-4415-8a54-0c7a20517835",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 101
        },
        {
            "id": "479a20e3-345f-4ac8-8d22-0e7554d20a3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 103
        },
        {
            "id": "265cb0de-9c9a-458b-aa19-4dd5b497f4cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 109
        },
        {
            "id": "4f29550a-34ec-4110-a599-d201fe64b455",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 110
        },
        {
            "id": "b7b469fe-74ff-45a2-b5e0-f89a53d388d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 111
        },
        {
            "id": "438cbb2d-5443-4f47-9f63-d520456b09b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 112
        },
        {
            "id": "d2c46505-152c-4b67-8bf6-93a8301c3aa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 113
        },
        {
            "id": "f0c19d87-9684-4777-8461-b6e0b09ff7e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 114
        },
        {
            "id": "4f844223-a12e-403c-98a3-824f0dc635a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 115
        },
        {
            "id": "9e598e2a-1cc2-4a68-9b43-cb647e09bf78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 117
        },
        {
            "id": "366ec202-6010-4798-8cac-520e275ee1fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 171
        },
        {
            "id": "009f63f6-309d-4d07-88eb-b9db1d321eaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 191
        },
        {
            "id": "a121e96c-eaf2-4b91-b273-f35a3028aefd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 192
        },
        {
            "id": "101fb522-3e14-4c09-b1bc-f6bacd5fbd2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 193
        },
        {
            "id": "b4a2a4ab-521e-48e9-9fc1-060699acb9f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 194
        },
        {
            "id": "304c9ade-b213-41b0-b39d-7dc63fc8fb07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 195
        },
        {
            "id": "45070a1d-c4d1-42a1-8f55-1db4b17f7a75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 196
        },
        {
            "id": "b3aa4e35-9545-446e-9c44-3a9a908d5fae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 197
        },
        {
            "id": "bb483825-caf0-42d8-9a47-a8ad2b56a793",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 198
        },
        {
            "id": "b9be1abb-dec3-44f5-a000-baac5635aced",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 199
        },
        {
            "id": "62fea2d2-bc6d-41a9-8ca8-f76f2f12ad97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 210
        },
        {
            "id": "5c9073ed-505f-4d68-8e8c-854dddd902b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 211
        },
        {
            "id": "f8b68dbd-d0d2-4164-8ff7-c9f54d09b1b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 212
        },
        {
            "id": "3f7b6d66-311b-4d75-8aa8-09188f34a141",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 213
        },
        {
            "id": "8e6bba9c-9a3c-4765-86d7-6d249ba8859c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 214
        },
        {
            "id": "dd259ef1-2e71-4784-87d6-41e740f73e4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 216
        },
        {
            "id": "fe975e96-3230-49ce-8c34-b974e6a728b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 224
        },
        {
            "id": "45ca12b2-0220-48ed-bd4c-8b0aaa386e80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 225
        },
        {
            "id": "c4978b4b-3ecf-4714-bfae-db43b4676a0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 226
        },
        {
            "id": "646ee87c-d49c-4026-ac89-41c09cf7b1d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 227
        },
        {
            "id": "95461dc7-c062-4c2c-87b5-93afa4c3fb2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 228
        },
        {
            "id": "92901c89-a7b0-4157-8a15-e5f779793540",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 229
        },
        {
            "id": "b476ec8e-b589-4b15-aa77-dd1c32118ee3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 230
        },
        {
            "id": "f06fcbad-a9d9-4592-a7d0-19bb7ab7e8f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 231
        },
        {
            "id": "cfbe8991-4c83-4f30-94fd-85edf674b08b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 232
        },
        {
            "id": "63bbb385-fddb-46d1-8dd1-111ff5660114",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 233
        },
        {
            "id": "1d78ff37-bafa-4037-b2be-df00ea49b829",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 234
        },
        {
            "id": "74167611-c649-4b45-910a-106c89ad894d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 235
        },
        {
            "id": "42dbcbb4-2800-4b36-beec-5d307aeaba35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 237
        },
        {
            "id": "4e05f0a8-d674-4916-b48c-985399641c6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 89,
            "second": 239
        },
        {
            "id": "9a61a156-0e5e-4d69-88d4-ca22fdee3809",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 241
        },
        {
            "id": "4f0009c4-0b71-4f23-b1eb-23e5e1895ea5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 242
        },
        {
            "id": "87c04092-69fb-4a08-a626-45cdd81b7994",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 243
        },
        {
            "id": "07bbbf01-aa4f-4a5d-a6be-92c8368ab9b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 244
        },
        {
            "id": "e02c1628-589f-4f26-84c0-752345ef138e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 245
        },
        {
            "id": "42a08d48-6c3d-4ec3-bf61-3ff8ea4cef76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 246
        },
        {
            "id": "5d4732d3-d6d9-4e7a-8ec3-345e2dfa06aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 249
        },
        {
            "id": "31e670f4-b482-457a-a6dc-5f88e0e3c11a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 250
        },
        {
            "id": "b44f44cc-8c95-446b-ba47-00b560c9765f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 251
        },
        {
            "id": "29203a39-9b69-400c-ae6c-7ca5d8d77c99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 252
        },
        {
            "id": "8fdc4d44-1766-4862-8e7d-6314f928366f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 256
        },
        {
            "id": "ba61ba89-0e99-41d1-9e7e-2d458a918ce1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 257
        },
        {
            "id": "95f2d729-ee5e-4826-aacd-abfaf505e541",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 258
        },
        {
            "id": "32131df8-baac-42ef-b46e-f9af7c639cb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 259
        },
        {
            "id": "0633cf29-f3bc-4d3a-8f8c-3b11ff0e5e66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 260
        },
        {
            "id": "f6c262ae-9f73-4545-82b2-d701281f9a6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 261
        },
        {
            "id": "4f30354f-4466-4068-8b8e-8b8b1d4eb034",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 262
        },
        {
            "id": "385887d9-d55d-40e1-ab20-cc5bdbfd3d3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 263
        },
        {
            "id": "3d256e84-d093-4d2b-bda2-462021f46374",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 264
        },
        {
            "id": "34696730-6e5e-4b4b-86e9-ed3b371b8ada",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 265
        },
        {
            "id": "cf72b10b-5487-4723-ba25-7107e884842d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 268
        },
        {
            "id": "37782336-cab9-447d-b016-69b4bba1d6a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 269
        },
        {
            "id": "98fe2e18-40be-453c-9f8d-26d7baf743f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 271
        },
        {
            "id": "0a356b82-5ebe-418f-b631-e90192c0dc38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 273
        },
        {
            "id": "bb8dcf3a-9c81-4fc9-827a-b2af5081e108",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 275
        },
        {
            "id": "92ee48f3-4806-499a-bf97-d050294f919d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 277
        },
        {
            "id": "9addea85-0a8c-4238-8401-717f955cca99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 279
        },
        {
            "id": "7c84d01f-d8ce-417d-b0fe-150c58ba79af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 281
        },
        {
            "id": "bc045f4a-e02b-43c5-a057-eac7f035ed60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 283
        },
        {
            "id": "7da91b15-d636-4e4b-a5fa-dbddda11b63f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 284
        },
        {
            "id": "bcc08074-87bc-4d7d-9243-d4622e0cda49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 285
        },
        {
            "id": "bca716f2-374c-416b-a0de-9809973462e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 286
        },
        {
            "id": "b543de7e-0849-4b14-9a96-b8da8423df4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 287
        },
        {
            "id": "80dd5f11-e9c7-41cb-af7c-916b05bb6e17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 288
        },
        {
            "id": "6a583b55-231c-494d-b0dc-346ac7df6551",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 289
        },
        {
            "id": "658b7632-8e2b-4fdc-bff4-f47c4270d414",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 290
        },
        {
            "id": "27b679b2-f778-4ff8-be9c-988378b4aa5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 291
        },
        {
            "id": "48e9772b-4ed2-462e-a1a4-5ec438d5a98e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 297
        },
        {
            "id": "54faa96e-cb5c-4220-9a28-1a2d0e6f00e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 299
        },
        {
            "id": "8b50cbb4-a5f6-4d30-98a4-09dee3e85cee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 301
        },
        {
            "id": "475efb49-b9d4-44bd-8597-a6b9187ceede",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 308
        },
        {
            "id": "0008738c-a20e-4632-9d0e-21128609fd5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 324
        },
        {
            "id": "1a797340-1be4-42a9-a7ec-19d085c4ca53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 328
        },
        {
            "id": "bf7db871-0efb-4419-ad09-8e24672580b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 331
        },
        {
            "id": "4132803c-6387-4f18-8b33-ff62692d3cc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 332
        },
        {
            "id": "7e8b686a-0099-423f-8c27-37202ab668db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 333
        },
        {
            "id": "82c81bf9-de48-4639-a292-41a63d181a24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 334
        },
        {
            "id": "4b1448a9-4787-4eb0-a4a4-c32c1c24dbf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 335
        },
        {
            "id": "623fda6e-b260-41eb-b69d-ebf228022c17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 336
        },
        {
            "id": "0a8f3de8-9251-458c-95de-23cf139a8a8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 337
        },
        {
            "id": "51c61395-fdde-4738-85eb-57ec1e435c12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 338
        },
        {
            "id": "556bc6f9-7281-43e5-a4cf-e6ca978e900b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 339
        },
        {
            "id": "5e40de9a-7810-4a18-a95b-7b914ea97737",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 341
        },
        {
            "id": "d9195895-f352-4752-a33f-7aa3a407db84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 343
        },
        {
            "id": "85e4d04b-dc9d-4e92-8ac6-de2066b03a7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 345
        },
        {
            "id": "c88a918f-f0cc-474a-9d56-2485c1f7aef2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 347
        },
        {
            "id": "f1b2f11a-890d-44b8-839a-accdd14f6700",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 349
        },
        {
            "id": "6a398baa-2110-4392-9059-d876ff386e74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 351
        },
        {
            "id": "695ee2da-8020-4aac-80c2-8537de6abf4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 353
        },
        {
            "id": "da15acb3-3526-4b4d-9d04-ec3f6755b212",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 361
        },
        {
            "id": "f943aa6e-14f8-4f6d-aaec-30311abbc72c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 363
        },
        {
            "id": "c85d3b35-4877-4ffe-8dd6-153de92384e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 365
        },
        {
            "id": "a408b081-114d-4024-8518-c21c068ceb9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 367
        },
        {
            "id": "b4191737-da11-4517-a71b-aad91992fd9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 369
        },
        {
            "id": "9d895a43-2264-4173-b956-5d39ca218b6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 371
        },
        {
            "id": "c6b05ee7-b7cb-419f-acfc-2bd4319f9f25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 506
        },
        {
            "id": "886acb12-f24e-4271-8f7c-eab9dfe99dd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 507
        },
        {
            "id": "3db87845-5baf-4e5b-add4-fd0f338427b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 508
        },
        {
            "id": "74742de1-e675-4cc9-9809-c60f096c2572",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 509
        },
        {
            "id": "e18cc08a-1d36-415c-a36b-58268a590700",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 510
        },
        {
            "id": "e2aa8340-092f-42a1-81e1-42a269599103",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 511
        },
        {
            "id": "21530cbd-8400-4ab3-bfd4-b3c4ac6a6da9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 537
        },
        {
            "id": "f08b858c-eebd-4be1-828d-c8eaf88e85e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 8218
        },
        {
            "id": "1d9eab0a-db4b-4c89-b5b6-8174e7b5a0b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 8222
        },
        {
            "id": "c75dac3b-7b06-4dc1-8e80-117e4ae0daa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 8230
        },
        {
            "id": "bff95f21-0abd-489e-a666-ad1fd265001e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8249
        },
        {
            "id": "85ca0ec0-9111-4547-b0b0-18553dea3b57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 74
        },
        {
            "id": "6a65350a-f587-4071-9851-93223e0396da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 84
        },
        {
            "id": "05a1a336-09be-4731-ad78-a89e10f500ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 121
        },
        {
            "id": "3c1fe8b2-ca0e-4d3c-aa41-e4238159a120",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 187
        },
        {
            "id": "177f48e5-7adc-413e-9784-33c796625ff0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 239
        },
        {
            "id": "d1b2f22c-4e20-4071-854d-7c10059e1149",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 253
        },
        {
            "id": "c0ef7136-89a1-478e-829d-08c61edc3129",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 255
        },
        {
            "id": "0dd0571f-1794-48ef-b304-26ff19fab502",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 297
        },
        {
            "id": "ee2745dc-d6f7-418c-90ac-08eebcf7add8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 299
        },
        {
            "id": "6067c9f8-d1be-40ba-b440-40ccee427b3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 301
        },
        {
            "id": "bfa14be6-1ae0-43d7-a8c8-7bb0559d750b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 308
        },
        {
            "id": "8ae98ec3-8f64-426d-a149-1a198eb86bd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 375
        },
        {
            "id": "99ea8f09-e39e-4074-88d5-29ebb903cd28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 7923
        },
        {
            "id": "1342e038-3d97-426f-8a8c-89caac1d4557",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 8250
        },
        {
            "id": "41405b0c-7a07-42fe-a4c1-de49c92d4660",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 91,
            "second": 106
        },
        {
            "id": "57f84c7b-6afb-448d-88aa-493cb0e79c98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 91,
            "second": 309
        },
        {
            "id": "1626d9b1-aa5e-4adc-bf08-31ff3bb81829",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 120
        },
        {
            "id": "adec2c6a-c3db-473b-9af6-00144f2eeaeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 74
        },
        {
            "id": "19eb9762-8de0-4c5e-974f-3b55f681b5b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 84
        },
        {
            "id": "767925fc-a48f-4615-8ea6-9e1413b982a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 89
        },
        {
            "id": "e36dc404-8cce-4cd1-b64a-a07cb14aacf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 221
        },
        {
            "id": "5751ae05-1e32-4f71-89b9-3131eea3479a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 308
        },
        {
            "id": "4aecece3-1637-4fb2-81c1-bbbb06ccd08e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 354
        },
        {
            "id": "7e06571b-48bb-45b2-81fb-7d426972a1fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 356
        },
        {
            "id": "4acf7263-9233-43b1-a604-3f9f65a1fb76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 358
        },
        {
            "id": "178fdbf7-fc90-435b-9f74-7ca147ba2fa6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 374
        },
        {
            "id": "8bd82214-5352-461b-89bc-930602d35380",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 7922
        },
        {
            "id": "90bd4d1e-30b8-458a-9e38-ad1328f83c0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 34
        },
        {
            "id": "a1af7292-b531-4559-b65b-c448153bb99a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 101,
            "second": 39
        },
        {
            "id": "2d4c791a-2c10-4e0a-ae16-1cfce466de86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 41
        },
        {
            "id": "6571e7ac-ab7c-4e07-82a2-09f9c588eeed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 42
        },
        {
            "id": "81b5ff33-90eb-4e4b-962d-019403557781",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 44
        },
        {
            "id": "e990334a-fff2-4dd5-b0c9-33b7b50f3cfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 45
        },
        {
            "id": "f866cc47-06b7-4af1-97fd-68338c22aa05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 46
        },
        {
            "id": "dc64293e-3ed2-4dae-989f-c59b7f8a947c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 58
        },
        {
            "id": "d2443c03-64e4-487b-bb39-1b017d757500",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 59
        },
        {
            "id": "63f96fda-b4d6-4b53-9549-43b9ae0fe57f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 63
        },
        {
            "id": "801a9707-4625-4211-ac34-afccb714aa7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 93
        },
        {
            "id": "e490955e-3e97-49f9-81e9-f2e162ba0c21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 116
        },
        {
            "id": "04a7a060-b6ad-4fbf-8bc1-4474360b0080",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 118
        },
        {
            "id": "175c839b-73ab-48a2-819b-656c33eaf1f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 119
        },
        {
            "id": "937c3226-5c36-4025-af7c-2acc4f8fdbc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 121
        },
        {
            "id": "9817307a-c4e9-4b4b-a740-7346bb8cb91c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 125
        },
        {
            "id": "80be1c7a-abc3-46a3-ad97-ef423918cea0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 187
        },
        {
            "id": "0aa03a44-86a8-4b6c-a874-57882cc4f32e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 236
        },
        {
            "id": "bb4d0d5a-50d0-4b08-bd4a-91025e89d628",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 238
        },
        {
            "id": "f470d1fb-e4ce-41a3-9efd-8b37d560924e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 102,
            "second": 239
        },
        {
            "id": "a2e8f167-3078-401a-9692-2cae53bba308",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 102,
            "second": 297
        },
        {
            "id": "d9a4e4be-1434-4688-8859-d1ff627dff72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 102,
            "second": 299
        },
        {
            "id": "b567928a-b788-42aa-824a-af104c7f34e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 102,
            "second": 301
        },
        {
            "id": "1a3f59e9-9232-441b-8ea5-274d803c4175",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 8208
        },
        {
            "id": "662c527d-f877-4028-828c-d96bc2a403a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8216
        },
        {
            "id": "09a2b48a-d861-48dd-b3ba-36f21a367945",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "160e20e1-5ea3-4b66-9b1d-72f92da74967",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 8218
        },
        {
            "id": "2ce92b4d-8001-46af-9ca8-bdd595fce347",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8220
        },
        {
            "id": "fdecb8ab-dde7-4a13-85c9-3adb857a5e93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8221
        },
        {
            "id": "4f76e078-8b07-4a46-830f-9412ac63cbdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 8222
        },
        {
            "id": "ce748e50-b3cd-47d4-800f-292adbc3f5ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 8230
        },
        {
            "id": "0881727d-282f-4fe6-bd11-130e5ad88976",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8250
        },
        {
            "id": "4ec8dd4b-9b33-464a-b2ba-ec6d27b73ff5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 102,
            "second": 8482
        },
        {
            "id": "27c2e097-3d82-4ae0-bb2b-36bf85cb1e9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 64831
        },
        {
            "id": "5bc2247e-b0b7-487e-9bd5-8fcbaa2ccd71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 309
        },
        {
            "id": "352faca1-31bf-4ce2-8e30-d7b7e9ad6882",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 44
        },
        {
            "id": "6de8e5d7-ca63-4c37-9d01-1c6ea6eb49aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 45
        },
        {
            "id": "e9709c81-132e-41c7-9b16-708017ec38fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 46
        },
        {
            "id": "9502702a-1561-4b3d-914d-5733e6e9ff15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 58
        },
        {
            "id": "88befe3d-47c9-4180-8105-38cccb5a0c40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 59
        },
        {
            "id": "c4a35d18-f06a-4c47-a8b9-57a2a4c48c14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 263
        },
        {
            "id": "aec3a3c8-e179-4078-9fac-7a661b17ba3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 265
        },
        {
            "id": "c5cc04ea-90e1-4d2a-a2a5-09e227ee21b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 269
        },
        {
            "id": "255ecd0e-0563-4626-af51-af12af2cc94b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 275
        },
        {
            "id": "c0778317-acf5-4184-b329-512773712b2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 277
        },
        {
            "id": "0a7be706-4769-4e28-be76-e1d88d0362e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 279
        },
        {
            "id": "f72a5951-f32e-4085-97e9-94aaf5efedcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 281
        },
        {
            "id": "8b24c78e-651f-4fda-b339-3b5396ec680c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 283
        },
        {
            "id": "a9140fee-0ca8-48ff-9513-7b8c2d3531a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 285
        },
        {
            "id": "721e1ac7-f670-40d6-bb8c-56840087fb83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 287
        },
        {
            "id": "b30d81b8-1d07-4298-9fca-d8b0bf27a2fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 289
        },
        {
            "id": "180e8ba2-b267-41aa-b881-7f9af2673510",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 291
        },
        {
            "id": "8a8af449-e63d-4c9c-8571-fa851969b36f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 333
        },
        {
            "id": "7d84d8ce-7a0a-4151-bb16-1c256bd016db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 335
        },
        {
            "id": "6b1b7fc0-a734-471f-be8a-45245f031bf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 337
        },
        {
            "id": "00aa71e7-956d-42c1-b5c2-4466087e92df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 511
        },
        {
            "id": "24e43e41-5cfa-480e-86e0-3b4d1668ec64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 8208
        },
        {
            "id": "6eebf06c-8d96-471c-8fb2-322400b9c92d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 8230
        },
        {
            "id": "ab31f3a1-cd21-41f7-b1d2-dd02847d76bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 34
        },
        {
            "id": "b7922f99-e941-4bc1-a7e9-26baa822f1b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 110,
            "second": 39
        },
        {
            "id": "578cb847-c216-458a-bc29-833a0339b397",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 34
        },
        {
            "id": "be01ae94-5bd3-41bc-ab78-229a1c31d840",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 111,
            "second": 39
        },
        {
            "id": "01fc71c2-3878-4379-b187-39162c0a9dcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 120
        },
        {
            "id": "6121a027-b6da-4913-94e0-5c1d32c96fae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 8216
        },
        {
            "id": "122b5bf3-74b5-4ede-9359-8370d5fa99d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 111,
            "second": 8217
        },
        {
            "id": "bf62187e-2b9c-4354-8540-5e61f75d335f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 8220
        },
        {
            "id": "3e3b7fff-9e53-44ac-b6b8-d44ee3f61690",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 111,
            "second": 8221
        },
        {
            "id": "cadd833d-d473-4597-86f0-d34daee4828e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 120
        },
        {
            "id": "1f67052f-4a34-47f5-afbc-bfaea7522929",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 112,
            "second": 8216
        },
        {
            "id": "c1febd27-7140-40a1-a017-a083f79fe6f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 112,
            "second": 8217
        },
        {
            "id": "2e24a57a-6fa2-4967-8590-5bb42ba203d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 8220
        },
        {
            "id": "1d1ac454-4f30-4b41-be00-af35fba34ee7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 112,
            "second": 8221
        },
        {
            "id": "cbe875f5-bbdc-455d-a927-82182bbacc53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 106
        },
        {
            "id": "2f46d336-7b69-4a6d-bc06-0bcb69e6ac90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 309
        },
        {
            "id": "eaf96695-d61a-4f4a-82f3-3fda8c2d9235",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 44
        },
        {
            "id": "390717ff-7fe0-41d8-a411-268a7e56682f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 45
        },
        {
            "id": "9ce157bc-3f8a-484c-8794-8daab2afadf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 46
        },
        {
            "id": "8b8cd106-fec8-4fa1-8fe8-05cd3a23bc03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 58
        },
        {
            "id": "a11623fa-47d8-4c96-853e-60a3e28830e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 59
        },
        {
            "id": "25e14fa1-cb90-4c58-b48c-da3ce8a98d42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 102
        },
        {
            "id": "9a41b447-d175-4280-8909-205d62b40eec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 116
        },
        {
            "id": "c94e6fea-cf69-4765-b9cc-b1e557427427",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 118
        },
        {
            "id": "79ec9180-fd68-488e-97e0-9d887df60bb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 119
        },
        {
            "id": "0300014e-7b5e-4d62-ae74-c6b782ce2c21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 120
        },
        {
            "id": "5f7beeca-95ea-4d20-b285-a7fe05d07d7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 121
        },
        {
            "id": "d9fe64d8-599d-4eeb-a412-3710d9ee5d55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 122
        },
        {
            "id": "4c43953f-4ba1-45b2-84ae-f427f974d8b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 253
        },
        {
            "id": "ea0b9ee9-2c41-429d-b4fe-fae777df78f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 355
        },
        {
            "id": "4ddf5fa0-4431-4bd0-8d1a-fb264d4aaaa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 357
        },
        {
            "id": "63e6859f-4b9d-4425-90f4-c6b7559079a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 359
        },
        {
            "id": "7d873e91-2689-44e6-a20e-7ca2fa5743eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 373
        },
        {
            "id": "a3f136f6-147f-4282-8660-09e335a83f36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 375
        },
        {
            "id": "ac1949e8-dad1-4815-a64f-93c4adee520d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 7809
        },
        {
            "id": "a2b20204-3a6a-4e7b-a454-2c846e97138f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 7811
        },
        {
            "id": "ac4f7014-7043-4935-b707-f592c42d814c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 7813
        },
        {
            "id": "221442b8-4775-4a81-bf49-021a3b0da841",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 7923
        },
        {
            "id": "fcf05b21-bac9-436d-b921-6a3be38d958c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 8208
        },
        {
            "id": "b0129461-a75e-4373-a4aa-6fdb099be57b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 114,
            "second": 8216
        },
        {
            "id": "c216ab58-b33f-45dd-b757-49da909e6a63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 8217
        },
        {
            "id": "1f19d793-9014-42e0-9ca3-51207d2cdd39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 8218
        },
        {
            "id": "7ec89729-7dc3-4a03-a6fd-c631db230873",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 114,
            "second": 8220
        },
        {
            "id": "316d8772-414d-416c-9daf-e44936006984",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 8221
        },
        {
            "id": "9839b3f0-65f2-40c6-94ab-4ce21dccf7c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 8222
        },
        {
            "id": "e7ef8c75-ade6-437e-b18a-a902664daa2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 8230
        },
        {
            "id": "2a4a15df-3645-4bdd-96ed-30ace628d990",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 45
        },
        {
            "id": "9354c892-aa3b-4170-b0e0-2d1c4a2234c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 63
        },
        {
            "id": "d1e9222a-d5bd-4c00-953f-98a3ed9ab902",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 187
        },
        {
            "id": "6cb3d77f-4414-492d-84ed-e1f5b42c110e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 8208
        },
        {
            "id": "32050b0b-d1f9-4e56-b9dc-4c79b1be1634",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 8250
        },
        {
            "id": "fc9b2918-8e9a-4c8b-80c4-b50278879f69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 34
        },
        {
            "id": "576b5552-b56f-46bc-a25b-da2edde43818",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 39
        },
        {
            "id": "67d5f152-9ddf-45c7-bb6d-1d39cddf2f98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 44
        },
        {
            "id": "bcd06053-97f7-4623-861a-12866f307adc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 46
        },
        {
            "id": "e4063c4e-dc77-4e37-9d25-94b3be3d54e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 8218
        },
        {
            "id": "6c0b83cf-4bed-4435-bc39-f73ea148c336",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 8222
        },
        {
            "id": "cdd1e8ac-d637-44a4-bddf-f119d7278226",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 8230
        },
        {
            "id": "04fdf1cf-19d7-43e8-9645-4d182692a50b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "1f2fe61c-ece0-4244-a6e9-dccd30188390",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "267fde45-9aee-46c5-ad28-9f61f9483c50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 8218
        },
        {
            "id": "aeedf3fc-8cbe-4862-a324-014efb652dce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 8222
        },
        {
            "id": "685490c1-43ca-47f8-ad57-bce3b621c80d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 8230
        },
        {
            "id": "6dd9089e-34bb-4509-831e-e76b9f762053",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 99
        },
        {
            "id": "e34f7f5d-a889-43ca-af2f-512a10d8869f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 100
        },
        {
            "id": "e3e17c66-4e27-423c-8448-0c98dfb45b3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 101
        },
        {
            "id": "37c9faac-faa0-4e14-8c83-6bce67d0d8ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 103
        },
        {
            "id": "042bcb12-50c8-4b58-80b8-7699744ac1c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 111
        },
        {
            "id": "d8dbea97-1276-41cb-bfa5-c0119558414e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 113
        },
        {
            "id": "c1e0ebb3-c9eb-40d5-a399-0f4ee64dec7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 232
        },
        {
            "id": "9eb1ebdc-28f6-4ad0-88e8-0f5ca657c5dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 233
        },
        {
            "id": "c5041e6a-1aab-46e8-a677-e18bc046b972",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 234
        },
        {
            "id": "b6feed7d-85a2-46b4-8c3f-1ce5cde0fdaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 235
        },
        {
            "id": "6fde7922-d8cd-4a36-90b3-7a3162b0ac74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 242
        },
        {
            "id": "a1d6f1ec-77b3-487c-b0fe-e56a173781d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 243
        },
        {
            "id": "e8a0f339-f56a-4535-8590-53462c608be0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 244
        },
        {
            "id": "4f3c8970-1268-460a-b112-d030d6759059",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 245
        },
        {
            "id": "5d52e12f-ae31-47e2-8789-01699f95bf93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 246
        },
        {
            "id": "1940734d-fca1-49f1-92d2-2e2504e07550",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 339
        },
        {
            "id": "8805428d-fb49-4508-a601-d8c03ee1b20a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 39
        },
        {
            "id": "8d8e876d-b2bf-4ca1-8903-b94af59e47ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 44
        },
        {
            "id": "31de4cb9-d36c-48a2-956f-56bfe81ac221",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 46
        },
        {
            "id": "2c0536b6-4f0e-4e7f-87b1-132d32685e6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 8218
        },
        {
            "id": "4f2ea324-7252-4375-bbe9-d27331244314",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 8222
        },
        {
            "id": "b3e0d7d6-4a43-43e5-a891-eaa03cb6c155",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 8230
        },
        {
            "id": "ce469fad-68f7-473e-8490-fc8cb1fa0085",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 123,
            "second": 106
        },
        {
            "id": "060d1c33-e8e8-4d3c-afe4-aa7df1414754",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 123,
            "second": 309
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 24,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}