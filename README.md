# tennis_for_one
A Tennis for Two clone with simple AI that gets progressively more difficult

# Current Status
Currently the player controls work well and the simple AI is still able to provide a decent challenge (for myself at least). Goals at this point are primarily to improve the AI. 

Current simple AI Algorithm goes something like this:
* Game estimates ball's location in 5 frames
* CPU Paddle moves to the y value of that location

To make the movement look more natural and give it a slight delay an easing algorithm is used for CPU Paddle movement and the vertical velocity is capped at a max value. Additionally to remove jitter if the paddle is within its half-width of the ball's height it will stop moving. This max value increases with each five consecutive points scored by the player to increase difficulty.

# Controls
Move mouse up and down to move paddle along y axis
Left Mouse click to serve ball
ESC to pause

# Images
![live](https://i.imgur.com/Vz0Pqnd.png)

![pause](https://i.imgur.com/manYwYb.png)
