{
    "id": "509a3888-aefe-4b44-970e-bb7471093326",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEnemyPaddle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bbc85fb5-833d-4da6-a685-5761ecceaa85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "509a3888-aefe-4b44-970e-bb7471093326",
            "compositeImage": {
                "id": "131bf243-e4c7-4f31-bc20-7cdb03dfcead",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbc85fb5-833d-4da6-a685-5761ecceaa85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f97e4e3-0d4a-4f94-a86c-dc51c0208c39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbc85fb5-833d-4da6-a685-5761ecceaa85",
                    "LayerId": "d32ecefd-de61-4f84-9a87-149d8de13cea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "d32ecefd-de61-4f84-9a87-149d8de13cea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "509a3888-aefe-4b44-970e-bb7471093326",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 64
}