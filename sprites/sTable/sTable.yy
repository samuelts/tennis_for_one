{
    "id": "3679ec8e-b307-4c0a-a407-d7e28eab856b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTable",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eb65b7d0-06d2-4047-881d-63c39e1be072",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3679ec8e-b307-4c0a-a407-d7e28eab856b",
            "compositeImage": {
                "id": "b6f8cff1-7c83-453f-8801-b92608887dd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb65b7d0-06d2-4047-881d-63c39e1be072",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a09577ef-75eb-45cb-aba3-0a2bd590143e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb65b7d0-06d2-4047-881d-63c39e1be072",
                    "LayerId": "db29cd05-e56d-4a9f-9846-6b3e6f0578b4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "db29cd05-e56d-4a9f-9846-6b3e6f0578b4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3679ec8e-b307-4c0a-a407-d7e28eab856b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}