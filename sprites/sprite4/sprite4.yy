{
    "id": "98cda1e5-e0fe-456f-be28-143aba47bca7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2d270dd8-35f8-4724-994e-ddaf8389b6d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98cda1e5-e0fe-456f-be28-143aba47bca7",
            "compositeImage": {
                "id": "2b92637b-c144-4ba4-b16a-7c474452a741",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d270dd8-35f8-4724-994e-ddaf8389b6d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74154d66-06df-4146-9df3-039c718b4c0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d270dd8-35f8-4724-994e-ddaf8389b6d4",
                    "LayerId": "1aa7a1f2-40ab-48eb-9d7c-0b1067a4758f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1aa7a1f2-40ab-48eb-9d7c-0b1067a4758f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "98cda1e5-e0fe-456f-be28-143aba47bca7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}