{
    "id": "8fff29d4-1669-4de5-be07-55036345bd45",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sNet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "19f541a0-6d9b-4c1e-b094-175ae46bb94f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fff29d4-1669-4de5-be07-55036345bd45",
            "compositeImage": {
                "id": "10dfca46-4494-4193-a3e0-077fe488dacb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19f541a0-6d9b-4c1e-b094-175ae46bb94f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84a799ce-40ae-46a1-89c1-015570276bd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19f541a0-6d9b-4c1e-b094-175ae46bb94f",
                    "LayerId": "c81092ea-9aca-40fa-8282-eb472ad9a8de"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c81092ea-9aca-40fa-8282-eb472ad9a8de",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8fff29d4-1669-4de5-be07-55036345bd45",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}