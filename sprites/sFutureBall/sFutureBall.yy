{
    "id": "f4da7bec-ec79-421b-b530-dd01a20e70f2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFutureBall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0803b58b-3028-487a-bfc2-91496bd3c8dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4da7bec-ec79-421b-b530-dd01a20e70f2",
            "compositeImage": {
                "id": "7784a24b-bfaf-4441-b027-c51d2130f9a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0803b58b-3028-487a-bfc2-91496bd3c8dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cc78df4-b95b-4134-9704-c1e52c9f903a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0803b58b-3028-487a-bfc2-91496bd3c8dd",
                    "LayerId": "0b223c95-3db3-498c-86e5-8c86d3a94f2d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0b223c95-3db3-498c-86e5-8c86d3a94f2d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f4da7bec-ec79-421b-b530-dd01a20e70f2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}