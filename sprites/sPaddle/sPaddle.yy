{
    "id": "23b9dac9-5591-4234-881b-feea8e2e5823",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPaddle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4acc6fa9-facd-4475-9f96-54accdfd262e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23b9dac9-5591-4234-881b-feea8e2e5823",
            "compositeImage": {
                "id": "3b2ac55a-7c29-4422-be68-7eed48f338be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4acc6fa9-facd-4475-9f96-54accdfd262e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e1011d7-8fb6-4904-a55c-15792cd19d33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4acc6fa9-facd-4475-9f96-54accdfd262e",
                    "LayerId": "9446e8c0-ef53-4990-be58-b7d4c2bbabfe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "9446e8c0-ef53-4990-be58-b7d4c2bbabfe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "23b9dac9-5591-4234-881b-feea8e2e5823",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 64
}