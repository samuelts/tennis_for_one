{
    "id": "a289af38-fc79-4ce2-afdb-6fb2ab819cc0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "33d9674a-4cfe-408b-8504-93b782f34d8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a289af38-fc79-4ce2-afdb-6fb2ab819cc0",
            "compositeImage": {
                "id": "fcddc42a-a5e2-4e88-8023-a1aad969c15f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33d9674a-4cfe-408b-8504-93b782f34d8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37dcf9d5-b0fa-4c50-a730-5b42d6b63d19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33d9674a-4cfe-408b-8504-93b782f34d8e",
                    "LayerId": "9fee2a57-d9c6-4388-b9f2-e6c91cbba23d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9fee2a57-d9c6-4388-b9f2-e6c91cbba23d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a289af38-fc79-4ce2-afdb-6fb2ab819cc0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}