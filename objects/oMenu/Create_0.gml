/// @desc GUI/Vars/Menu setup
game_set_speed(60,gamespeed_fps);
window_set_fullscreen(false);
display_set_gui_size(1920,1080);
#macro SAVEFILE "autosave.sav"

gui_width = display_get_gui_width();
gui_height = display_get_gui_height();
gui_margin = tile_size;

menu_x = gui_width / 2;
menu_y = gui_height + 500;
menu_y_target = gui_height/2;
menu_speed = 25; //lower is faster
menu_font = fMenu;
menu_itemheight = font_get_size(fMenu);
menu_commit = -1;
menu_control = true;

menu[3] = "New Game";
menu[2] = "Continue";
menu[1] = "Options";
menu[0] = "Quit";

menu_items = array_length_1d(menu);
menu_cursor = 3;

title = "Tennis for One"

menu_top = menu_y - ((menu_itemheight * 1.5) * menu_items);