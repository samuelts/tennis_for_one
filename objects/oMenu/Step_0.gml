/// @desc Control Menu

///Item ease in
menu_y += (menu_y_target - menu_y) / menu_speed;

if (menu_control)
{
	if (keyboard_check_pressed(vk_up)) || (keyboard_check_pressed(ord("W"))) || (gamepad_button_check_pressed(0, gp_padu))
	{
		menu_cursor++;
		if (menu_cursor >= menu_items) menu_cursor = 0;
	}
	
	if (keyboard_check_pressed(vk_down)) || (keyboard_check_pressed(ord("S"))) || (gamepad_button_check_pressed(0, gp_padd))
	{
		menu_cursor--;
		if (menu_cursor < 0) menu_cursor = menu_items-1;
	}
	
	if (keyboard_check_pressed(vk_enter))  || (keyboard_check_pressed(ord("E")))  || (gamepad_button_check_pressed(0, gp_face1))
	{
		menu_y_target = -500;
		menu_commit = menu_cursor;
		menu_control = false;
	}
	
	var mouse_y_gui = device_mouse_y_to_gui(0);
	if (mouse_y_gui < menu_y) && (mouse_y_gui > menu_top)
	{
		menu_cursor = (menu_y - mouse_y_gui) div (menu_itemheight * 1.5);
		if (mouse_check_button_pressed(mb_left))
		{
			menu_y_target = -500;
			menu_commit = menu_cursor;
			menu_control = false;
		}
	}
}

if (menu_x < 0) && (menu_commit != -1)
{
	switch (menu_commit)
	{
		case 3: {
			room_goto(rGame);
			break;
		}
		case 2: 
		{
			with(oPaddleCPU) oPaddleCPU.vspMax = 10;
			room_goto(rGame);			
			break;
		}
		case 1:
		{
			main_menu = false;
			options_menu = true;		
			break;
		}
		case 0: game_end(); break;		
	}
	
}
