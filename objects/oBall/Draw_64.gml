/// @description Insert description here
// You can write your code in this editor

draw_set_font(fScore);
draw_set_color(c_white);
draw_set_halign(fa_left);
draw_set_valign(fa_top);

draw_text(100,100,"P1: " + string(oPaddleP1.scoreP1));

draw_set_halign(fa_right);

if (instance_exists(oPaddleP2)) {
	draw_text(room_width-100,100,"P2: " + string(oPaddleP2.scoreP2));
} else if (instance_exists(oPaddleCPU)) {
	draw_text(room_width-100,100,"CPU: " + string(oPaddleCPU.scoreCPU));
}

draw_set_halign(fa_center);
if (timer != 0) {
	if (timer % 60 == 0) timersec = timer / 60;
	draw_text(512,100,string(timersec));
}

draw_set_halign(fa_center);
if (noticeTimer != 0) {
	draw_text(room_width/2,100,"Difficulty Increased!");
	noticeTimer--;
}

draw_text(room_width/2,40,"Level: " + string(lvl));

