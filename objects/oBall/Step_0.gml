/// @desc Ball Step Event
#macro tile_size 32

if(global.pause) exit;

window_mouse_set(clamp(window_mouse_get_x(),0,window_get_width()-tile_size),clamp(window_mouse_get_y(),0,window_get_height()-tile_size))

//Start Fixed
if (fixed) {
	serve =  mouse_check_button_pressed(mb_left);//keyboard_check_pressed(vk_space); //gamepad_button_check_pressed(0, gp_face1);
	if (serveP1) {
		if (serve) {	
			hsp = hspMax;
			vsp = oPaddleP1.vsp/1.5;
			serveP1 = false;
			fixed = false;
		}
		with (oPaddleP1) {
			other.x = oPaddleP1.x+tile_size;
			other.y = oPaddleP1.y;
			instance_create_layer(other.x,other.y,"Ball",oFutureBall);
		}
	} else if (serveP2) {
		if (serve) { 
			hsp = -hspMax;
			vsp = oPaddleP2.vsp/2;
			serveP2 = false;
			fixed = false;
		}
		with (oPaddleP2) {
			other.x = oPaddleP2.x-tile_size;			
			other.y = oPaddleP2.y;
		}
	} else if (serveCPU) {
		if(timer > 0) {
			timer--;
			hsp = 0;
			vsp = 0;
		} else {
			hsp = -hspMax;
			vsp = random_range(-oPaddleCPU.vspMax,oPaddleCPU.vspMax);
			serveCPU = false;
			fixed = false;
		}
		with (oPaddleCPU) {
			other.x = oPaddleCPU.x-tile_size;			
			other.y = oPaddleCPU.y;
		}
	} 


} else {
	
	//Net Collision
	if (place_meeting(x+hsp,y,oNet)) {
	
		while(!place_meeting(x+sign(hsp),y,oNet)) {
			x = x + sign(hsp);
		}
	
		hsp = 0;
		
	}
	
	if (place_meeting(x,y+vsp,oNet)) {
	
		while(!place_meeting(x,y+sign(vsp),oNet)) {
			y = y + sign(vsp);
		}
	
		hsp = 0;
	}
	
	//Table Collision (roof is also oTable)
	if (place_meeting(x,y+vsp,oTable)) {
	
		while(!place_meeting(x,y+sign(vsp),oTable)) {
			y = y + sign(vsp);
		}
	
		vsp = -vsp;
		
		if (abs(vsp) < 10) {
			vsp = sign(vsp)*10;
		}
		
		if (abs(vsp) > 30) {
			vsp = sign(vsp)*30;
		}
		
	}
	
	//Paddle collision
	if (place_meeting(x+hsp,y,pPaddle)) {
	
		while(!place_meeting(x+sign(hsp),y,pPaddle)) {
			x = x + sign(hsp);
		}
	
		with (pPaddle) {
			other.vsp = other.vsp + vsp;	
		}
	
		hsp = -hsp;
		instance_create_layer(x,y,"Ball",oFutureBall);
	}

	if (place_meeting(x,y+vsp,pPaddle)) {
	
		while(!place_meeting(x,y+sign(vsp),pPaddle)) {
			y = y + sign(vsp);
		}
	
		vsp = -vsp;
		
		instance_create_layer(x,y,"Ball",oFutureBall);
	}	
	
	//Gravity
	vsp = vsp + grv;
	
	//Position Update
	x = x + hsp;

	y = y + vsp;
	y = clamp(y,tile_size,room_height-tile_size);
	
}

//Out of Bounds
if (x < 0) {
	if (instance_exists(oPaddleP2)) {
		with (oPaddleP2) {
			oPaddleP2.scoreP2++;
			other.x = oPaddleP2.x-tile_size;
			other.y = room_height / 2;
		}
		
		fixed = true;
		serveP2 = true;
		
	} else if (instance_exists(oPaddleCPU)) {
		with (oPaddleCPU) {
			oPaddleCPU.scoreCPU++;
			y = room_height / 2;
			other.x = oPaddleCPU.x-tile_size;
			other.y = room_height / 2;
		}
		
		fixed = true;
		serveCPU = true;
		timer = 180;
		
	}
} else if (x > room_width) {
	with (oPaddleP1) {
		oPaddleP1.scoreP1++;
		if (oPaddleP1.scoreP1 % 5 == 0) {
			oPaddleP1.diffUp = true;		
			other.noticeTimer = 180;
			other.lvl++;
		}
		other.x = oPaddleP1.x+tile_size;
		other.y = room_height / 2;
	}
	fixed = true;
	serveP1 = true;
	
}


	

	