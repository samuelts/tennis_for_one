/// @description Insert description here
// You can write your code in this editor
if(global.pause) exit;

with (oBall) {
	if (oBall.serveCPU) {
		other.vsp = 0;
		other.y = room_height / 2;
	}
}
with (oFutureBall) {
	if (oFutureBall.hsp > 0) {	
	//prevent jitter by not moving if within one fourth of a tile from center
	if (abs(oFutureBall.y - other.y) > 8) {
		other.vsp = (oFutureBall.y - other.y) * 0.2; //easing algorithm
	} else {
		other.vsp = 0;
	}
	} else {
		other.move = 0;
	}
}
t = 0;

t++;

vsp = clamp(vsp,-vspMax,vspMax);

y = y + vsp;

y = clamp(y,64,room_height-64);
if (instance_exists(oPaddleP1)) {
	if (oPaddleP1.diffUp) {
		oPaddleP1.diffUp = false;
		vspMax++;
	}
}
