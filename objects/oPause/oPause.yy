{
    "id": "b3b00e88-fedf-49e3-941c-76a58d9722bf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPause",
    "eventList": [
        {
            "id": "3c529ffb-6cf0-4bbe-acef-fcd564c1a258",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b3b00e88-fedf-49e3-941c-76a58d9722bf"
        },
        {
            "id": "1e5d389e-d16c-47b0-bf40-1c52df8cf460",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "b3b00e88-fedf-49e3-941c-76a58d9722bf"
        },
        {
            "id": "0191b60e-dd42-4646-9d2c-f4d6cdbaf430",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "b3b00e88-fedf-49e3-941c-76a58d9722bf"
        },
        {
            "id": "e1f5a96a-9813-4f2d-87f0-ff2f106ff60b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b3b00e88-fedf-49e3-941c-76a58d9722bf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}