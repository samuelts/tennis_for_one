/// @description Insert description here
if(global.pause) exit;

#macro tile_size 32

leadFrames = 1;

x = oBall.x + leadFrames*oBall.hsp;
hsp = oBall.hsp;

y = oBall.y + leadFrames*oBall.vsp;
vsp = oBall.vsp;

//Table Collision (roof is also oTable)
if (place_meeting(x,y+vsp,oTable)) {
	
	while(!place_meeting(x,y+sign(vsp),oTable)) {
		y = y + sign(vsp);
	}
	
	vsp = -vsp;
		
	if (abs(vsp) < 10) {
		vsp = sign(vsp)*10;
	}
		
	if (abs(vsp) > 30) {
		vsp = sign(vsp)*30;
	}
		
}
	
//Gravity
vsp = vsp + grv;
	
//Position Update
x = x + hsp;

y = y + vsp;
y = clamp(y,tile_size,room_height-tile_size);

//Out of Bounds
if (x < 0) {
	instance_destroy();
} else if (x > room_width) {
	instance_destroy();
} else if (place_meeting(x,y,oPaddleCPU)) {
	instance_destroy();
}